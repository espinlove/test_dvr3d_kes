!###########################################################
!##Written by S. Skokov for frequency calc. on            ##
!##HOCL PES, Jan. 9,1998                                  ##
!###########################################################
!** Compute energy in a.u. for points (x1,x2,x3) in      ***
!** jacobi coord. units are bohr and degree              ***
!**x1-OH, x2- HO-CL, x3 - zero for linear geometry       ***        
!** hocl.fits  Morse parameters for zmat coord.          ***
!***********************************************************
       subroutine potv(v,x1e,x2e,x3e) 
       implicit double precision  (a-h,o-z)
       parameter (bohr=0.529177249d0,hkcal=627.5096d0)
       parameter (hcm=219474.63067d0)
!**Energy minimum for HOCL PES************
        emin=-0.0079777259  !kcal
        pi = 4.0 * atan (1.0)
        x4e = dacos(x3e)
        x4e = (180.0d0*x4e)/pi
        call jzmat(x1e,x2e,x4e,r1,r2,theta)
        ladj=1  !make adjustments
!       ladj=0  !no adjustments    
        call pes1(r1,r2,theta,es,ladj)
        V=(es-emin)
        v = v/hkcal
    print *, "PES", x1e, x2e, x3e, v*219474.63d0
!       write(6,*)V,'kcal',es
!       write(6,*)V/hkcal,'a.u.',(V/hkcal)*hcm
!**Test results on RISC should be**
!   0.151475661874789225E-09 kcal
!   0.241391784085517143E-12 a.u.
!**********************************   
        end 
     
!###########################################################
!## Written by S. Skokov, December 8, 1997                ##
!###########################################################
!** On input r1 - R(OH) in Bohr 
!**          r2 - R(OCl) in Bohr 
!**          theta - angle in degree 
!** On out efit - energy in kcal/mol above HOCL ground state
!** All calculaions are in bohr, kcal, degree
!***********************************************************
      subroutine pes1(r1,r2,theta,efit,ladj) 
      implicit double precision  (a-h,o-z)
      PARAMETER(np=6)
      DIMENSION p(np)
      pi = 4.0 * atan (1.0)

      if(ladj.eq.1) then !make adjustment
!**ADJUSTMENTS*************************************
!**Shift ground state to experimental one**********
      r1exp=1.822395 !exper. ground state
      r2exp=3.191692
      thexp=102.965
      r1e=1.81872 !minimum of current PES
      r2e=3.19201
      the=103.0416
      dr1=(r1exp-r1e)
      dr2=r2exp-r2e
      dth=thexp-the
      r1=r1-dr1
      r2=r2-dr2
      theta=theta-dth
!**Scaling of coordinates****************
      sc1=0.996448-0.000408*exp(-1.5*abs(r1-r1e))
      sc2=1.00069
      sc3=0.99606
      r1=sc1*(r1-r1exp)+r1exp
      r2=sc2*(r2-r2exp)+r2exp
      theta=sc3*(theta-thexp)+thexp
      endif
!**END OF ADJUSTMENT*****************************
       
      thmin=40.0 !last computed angle 
      r2min=2.4  !smallest computed R(OCL) 
      r2max=10.0 !lagest used  R(OCL) 
      r1min=1.2  !smallest used  R(OH) 
      if(r1.lt.r1min) r1=r1min
      if(r2.lt.r2min) r2=r2min
      if(r2.gt.r2max) r2=r2max
      alp=1.0
!*******Compute parameters for point (x2,x3)***
      if(theta.lt.thmin) then ! polinomial extrapolation
      dtheta=1.0
      call param(r2,thmin,p)
      d=p(1)
      v=p(2)
      r0=p(3)
      c=-p(4)
      b=p(5)
      a=-p(6)
!*******Extrapolate for large OH  ****
      r1max=r0+1.5 
      if (r1.gt.r1max) then
        em=emax(r2)
        dinf=em-v
        d=(d-dinf)*exp(-alp*(r1-r1max)**2)+dinf
      endif
      efit1=esurf(r1,d,v,r0,c,b,a)
      call param(r2,thmin+dtheta,p)
      d=p(1)
      v=p(2)
      r0=p(3)
      c=-p(4)
      b=p(5)
      a=-p(6)
!*******Extrapolate for large OH  ****
      r1max=r0+1.5 
      if (r1.gt.r1max) then
        em=emax(r2)
        dinf=em-v
        d=(d-dinf)*exp(-alp*(r1-r1max)**2)+dinf
      endif
      efit2=esurf(r1,d,v,r0,c,b,a)
      t=(efit2-efit1)/dtheta  !derivative
      
!**Assign other param. at angle theta*******
      d2=106.39     !
      r02=1.8295    !
      c2=-1.2188    ! OH diss. curve
      b2=-0.0039    !
      a2=-0.0499    !
      v2=58.0575    !
      e1=esurf(r1,d2,v2,r02,c2,b2,a2)
      e2=emax(r2)
      dr=abs(r1-r2) 
      rep=100.0*exp(-2.5*(dr-2.5))
      if(rep.gt.999) rep=999
      efit2=e1+e2+rep-164.4475
      efit=sf1(theta,thmin,efit2-efit1,t)+efit1
        else
      call param(r2,theta,p)
      d=p(1)
      v=p(2)
      r0=p(3)
      c=-p(4)
      b=p(5)
      a=-p(6)
!*******Extrapolate for large OH  ****
      r1max=r0+1.5 
      if (r1.gt.r1max) then
        em=emax(r2)
        dinf=em-v
        d=(d-dinf)*exp(-alp*(r1-r1max)**2)+dinf
      endif
      efit=esurf(r1,d,v,r0,c,b,a)
!*******Check for spline errors  ****
!     if(d.lt.0.0) write(30,*)'SPLINE ERROR, D<0'
!     if(v.lt.0.0) write(30,*)'SPLINE ERROR, V<0'
!     if(c.gt.0.0) write(30,*)'SPLINE ERROR, c>0'
!     if(a.gt.0.0) write(30,*)'SPLINE ERROR, a>0'
        endif
!     write(30,99999)r1,r2,theta,efit
!     write(30,99999)d,v,r0,c,b,a
99999 format(8F9.3)     
!     write(6,*) efit  
      return
      end
!***Morse function for surface*********
       function esurf(x,d,v,r0,c,b,a)
       implicit double precision (a-h,o-z)
       dr=x-r0
       ar=a*dr**3+b*dr**2+c*dr
       s=1.0d0-exp(ar)
       esurf=D*s**2+v
       end
!***Smooth switch function (t+2)*x^3-(t+3)*x^2+1 *********
!** with derivative t at x=1  and 0 at x=0  **************
       function sf1(x,dx,dy,t)
       implicit double precision (a-h,o-z)
       a=(t*dx+2*dy)/dx**3
       b=-(3*dy+t*dx)/dx**2
       d=dy
       sf1=a*x**3+b*x**2+d
       end

!***Morse function for OCL diss.*********
       function emax(x)
       implicit double precision (a-h,o-z)
       edis=101.66 !MRCI/CBS value for H+OCL diss.
       d=62.7875   !adjusted
       R0=2.9737   !
       a=-0.0486   !Parameters for OCL diss, Peterson
       b=-0.1080   !
       c=-1.2398   !
       dr=x-r0
       ar=a*dr**3+b*dr**2+c*dr
       s=1.0d0-exp(ar)
       emax=D*s**2+edis
       end

      subroutine jzmat(x1,x2,x3,r1,r2,theta)
!****Transformation from jacobian (x1,x2,x3) for HOCL**
!****to z-matrix (r1,r2,theta) for HOCL              **
!*   x1 - R(OH), x2 - R(HO-CL), x3 - jacobian angle   *
!*   r1 - R(OH), r2 - R(OCL), theta - HOCL angle      *
!*   distance in bohr, angle in degree                *
!******************************************************
       implicit double precision (a-h,o-z)
       amh=1.007825035
       amo=15.99491463
       pi = 4.0 * atan (1.0)
       x3=x3*pi/180.0d0
       dx1=x1*amh/(amh+amo)
       r1=x1
       r2=sqrt(dx1**2+x2**2-2.0*dx1*x2*cos(x3))
       ar=(dx1**2+r2**2-x2**2)/(2*dx1*r2)
	if (ar.gt.1.0) ar=1.0
        if (ar.lt.-1.0) ar=-1.0
       theta=180.0d0*acos(ar)/pi
       x3=x3*180.0d0/pi
       return
       end

!#########################################################
!## Written by S. Skokov, November 7, 1997              ##
!#########################################################
!**Return parameters p(np) for point x1=r2,x2=th**********
!** On input:                                          ***
!** x1 - R(OH-CL) distance in Bohr                     ***
!** x2 - angle in degree                               ***
!** fort(nin) - file with 1-D Morse fits               ***
!** m - number of distances                            ***
!** n - number of angles                               ***
!** np -number of parameters                           ***
!*********************************************************
      subroutine param(x1,x2,p)  
      implicit double precision  (a-h,o-z)
      PARAMETER(MM=14,NN=16,NP=6)
      DIMENSION p(np),ya(mm,nn),y2a(mm,nn)
!     common/potpar/x1a(mm),x2a(nn),&
!    & par(mm,nn,np),der(mm,nn,np)
      dimension  x1a(mm),x2a(nn),par(mm,nn,np),der(mm,nn,np)
      save x1a,x2a,par,der,m,n
      data ncall/1/
      nin=14 
      if(ncall.eq.1) then
      open(nin,file='hocl.fits')
!****Read parameters from fort(nin) at first time*********** 
      read(nin,*)m, (x1a(i),i=1,m)  !R(OH-CL) distances
      read(nin,*)n, (x2a(j),j=1,n)  !jacobi angles
      if(m.ne.mm.or.n.ne.nn) then
      write(6,*)"!!! WARNING, ADJUST MM,NN !!!"
      STOP
      endif
      read(nin,*)
      read(nin,*)
      do j=1,nn
        do i=1,mm
        read(nin,*)r,th,(par(i,j,k),k=1,np)
        enddo
      enddo
      ijk = mm*nn*np
!***Compute second derivative matrix at first time*******
      do k=1,np  !loop over parameters
        do i=1,mm
        do j=1,nn
        ya(i,j)=par(i,j,k)
        enddo
        enddo
!*****Compute Morse parameters for point x1,x2********
      call splie2(x1a,x2a,ya,m,n,y2a)
       do i=1,mm
       do j=1,nn
       der(i,j,k)=y2a(i,j)
       enddo
       enddo
      call splin2(x1a,x2a,ya,y2a,m,n,x1,x2,y)
      p(k)=y
      enddo !loop over  parameters
      
      endif
!****for ncall larger then 1**************************
      do k=1,np  !loop over parameters
        do i=1,mm
        do j=1,nn
        ya(i,j)=par(i,j,k)
        y2a(i,j)=der(i,j,k)
        enddo
        enddo
!*****Compute Morse parameters for point x1,x2********
      call splin2(x1a,x2a,ya,y2a,m,n,x1,x2,y)
      p(k)=y
      enddo !loop over k parameters
      ncall=ncall+1
      return
      END
!##################################################################
!# SPLINE ROUTINES
!#            Numerical recipes in fortran
!#            Cambrige University Press
!#            York, 2nd edition, 1992.
!##################################################################
      SUBROUTINE splint(xa,ya,y2a,n,x,y)
      implicit double precision  (a-h,o-z)
      DIMENSION xa(n),y2a(n),ya(n)
      klo=1
      khi=n
 1    if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.0d0) write(6,*) 'bad xa input in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**&
     &2)/6.0d0
      return
      END
!##############################################################################
      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      implicit double precision  (a-h,o-z)
      DIMENSION x(n),y(n),y2(n)
      PARAMETER (NMAX=100)
      DIMENSION u(NMAX)
     
      if (yp1.gt..99d30) then
        y2(1)=0.0d0
        u(1)=0.0d0
      else
        y2(1)=-0.5d0
        u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.0d0
        y2(i)=(sig-1.0d0)/p
        u(i)=(6.0d0*((y(i+1)-y(i))/(x(i+&
     &1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*&
     &u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
        qn=0.0d0
        un=0.0d0
      else
        qn=0.5d0
        un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      END
!##############################################################################
      SUBROUTINE splin2(x1a,x2a,ya,y2a,m,n,x1,x2,y)
      implicit double precision  (a-h,o-z)
      DIMENSION x1a(m),x2a(n),y2a(m,n),ya(m,n)
      PARAMETER (NN=100)
      DIMENSION y2tmp(NN),ytmp(NN),yytmp(NN)
!U    USES spline,splint
      do 12 j=1,m
        do 11 k=1,n
          ytmp(k)=ya(j,k)
          y2tmp(k)=y2a(j,k)
11      continue
        call splint(x2a,ytmp,y2tmp,n,x2,yytmp(j))
!       write(6,*)'in splin2,yytmp(j),j',j,yytmp(j)
12    continue
      call spline(x1a,yytmp,m,1.d30,1.d30,y2tmp)
!     call spline(x1a,yytmp,m,0.0d0,0.0d0,y2tmp)
      call splint(x1a,yytmp,y2tmp,m,x1,y)
      return
      END
!##############################################################################
      SUBROUTINE splie2(x1a,x2a,y,m,n,y2)
      implicit double precision  (a-h,o-z)
      DIMENSION x1a(m),x2a(n),y2(m,n),y(m,n)
      PARAMETER (NN=100)
      DIMENSION y2tmp(NN),ytmp(NN)
!U    USES spline
      do 13 j=1,m
        do 11 k=1,n
!       write(6,*)'in splie2,y',y(j,k),j,k
          ytmp(k)=y(j,k)
!       write(6,*)'in splie2,ytmp',ytmp(k)
11      continue
       call spline(x2a,ytmp,n,1.d30,1.d30,y2tmp)
!      call spline(x2a,ytmp,n,0.0d0,0.0d0,y2tmp)
        do 12 k=1,n
          y2(j,k)=y2tmp(k)
12      continue
13    continue
      return
      END
