      SUBROUTINE potv(V,R1,R2,xcos)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! This subroutine provides a potential energy surface (PES) for the H3+,
! H2D+, D2H+ and D3+ molecules.  
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! SHORT SUMMARY OF THE INPUT/OUTPUT PARAMETERS
!
! INPUT PARAMETERS: R1,R2,xcos                
!    R1 and R2 are the values of the stretching-type coordinates, which
!    should be given in bohrs
!    xcos is the cosine of the angle-type coordinate 
! COMMON VARIABLE INPUT PARAMETERS: XMASS(1),XMASS(2),XMASS(3),G1,G2
!    XMASS(1),XMASS(2),XMASS(3) store the masses of the nuclei, and
!    should be in unified atomic mass units
!    G1 and G2 are constants defining the geometrical meaning of R1,R2
!    and xcos, and are unit independent 
! OUTPUT PARAMETERS: V            
!    V is given in hartrees
!
!
! IMPORTANT DETAILS ON THE INPUT PARAMETERS:
!
! This subroutine uses generalized Sutcliffe-Tennyson triatomic
! coordinates 
! (for details see Figure 1. of B. Sutcliffe and J. Tennyson, Int. J.
! Quantum Chem., Vol.XXXIX, 183-196 (1991) ), 
! thus the geometrical meaning of the input R1, R2 and xcos values
! depend on the values of the G1 and G2 
! COMMON variables, which should be provided by the user.
!
! For the case of mixed isotopolouges, the values of XMASS(i) i=1,2,3
! require special attention!
!  For H2D+ : XMASS(1)= mass of D, XMASS(2)=XMASS(3)= mass of H. 
!  For D2H+ : XMASS(1)= mass of H, XMASS(2)=XMASS(3)= mass of D.
!
!
! SOME CLARIFYING EXAMPLES:
!
! A) Input variables for H2D+ in valence coordinates:
!   XMASS(1)=2.013810d0  XMASS(2)=1.007537d0  XMASS(3)=1.007537d0
!   G1=0.0d0
!   G2=0.0d0
!   In this case R1 and R2 are simple bond lengths, while xcos is the
!   cosine of the bond angle defined by the two bonds corresponding to
!   R1 and R2.
! 
! B) Input variables for D2H+ in Jacobi (scattering) coordinates:
!   XMASS(1)=1.007537d0  XMASS(2)=2.013810d0  XMASS(3)=2.013810d0
!   G1=0.5d0
!   G2=0.0d0
!   In this case R1 is the D-D distance, R2 is the distance between the
!   H atom and the center of mass of the D-D diatom and xcos is the 
!   cosine of the angle defined by the D-D bond and the vector pointing
!   to the H atom from the center of mass of the D-D diatom. 
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      IMPLICIT DOUBLE PRECISION (A-H,O-Y),logical(z)
      COMMON /MASS/ XMASS(3),G1,G2,xmassr(3)
      dimension :: rm(3)
      DATA TOANG/0.5291772/, CMTOAU/219474.631/
      DATA A/2.22600/
      DATA X1/1.0D0/,X0/0.0D0/,TINY/9.0D-15/,X2/2.0D0/,x3/3.0d0/

         pi=dacos(-1.d0)
!     Equilibrium

      IF (G1 .EQ. X0) THEN
!        BONDLENGTH BONDANGLE COORDINATES: ATOM 1 = ATOM 2
         Q1 = R1
         Q2 = R2
         THETA = ACOS(XCOS)
! KES added line below
         COST = XCOS
      ELSE IF (G2 .EQ. X0) THEN
!        SCATTERING COORDINATES: ATOM 2 = ATOM 3
         XX = R1 * G1
         YY = R1 * (X1 - G1)
         IF (R2 .EQ. X0 .OR. XCOS .GE. (X1 - TINY)) THEN
            Q1 = ABS(XX - R2)
            Q2 = (YY + R2)
            COST = -X1
         ELSE IF (XCOS .LE. (TINY - X1)) THEN
            Q1 = (XX + R2)
            Q2 = ABS(YY + R2)
            COST = X1
         ELSE
            Q1 = SQRT(XX*XX + R2*R2 - X2*XX*R2*XCOS)
            Q2 = SQRT(YY*YY + R2*R2 + X2*YY*R2*XCOS)
            COST = (Q1**2 + Q2**2 - R1**2) / (X2 * Q1 * Q2)
         ENDIF
         THETA = ACOS(COST)
      ELSE
!        GENERAL COORDINATES (INCLUDING RADAU): ATOM 1 = ATOM 2
         F1= X1/G1
         F2= X1/G2
         F12= X1 - F1*F2
         P1= R1*(X1-F1)/(G2*F12)
         P2= R2*(X1-F2)/(G1*F12)
         S1= R1-P1
         S2= R2-P2
         Q1= SQRT(P1*P1 + S2*S2 + X2*P1*S2*XCOS)/(X1-G1)
         Q2= SQRT(P2*P2 + S1*S1 + X2*P2*S1*XCOS)/(X1-G2)
         Q3= SQRT(P1*P1 + P2*P2 - X2*P1*P2*XCOS)
         COST = (Q1*Q1 + Q2*Q2 - Q3*Q3)/(X2*Q1*Q2)
         THETA = ACOS(COST)
      ENDIF

!  s1,s2,s3 - distances between atoms in Bohrs
      s1 = Q1
      s2 = Q2
      s3=SQRT(s1*s1+s2*s2-2.0d0*s1*s2*COST)

! BO: fit260I
      call newpot(s1,s2,s3,pot)
! Making the bottom of the well = zero in energy
       v=pot + 1.3438355735473d0
1      format(4f10.2)    

! AC symmetric part     
      call potvAC(ac,s1,s2,s3)

! Asymptotic "cut" of the DA correction
      rm(1) = s1
      rm(2) = s2
      rm(3) = s3
          
      jmin = -1
      rmin = rm(1)
          
      do 100 j = 1, 3
        if(rm(j) .lt. rmin) then
          rmin = rm(j)
          jmin = j
        end if
100   continue

      if(jmin .gt. 1) then
        rmin = rm(1)
        rm(1) = rm(jmin)
        rm(jmin) = rmin
      end if
              
      R0 = dsqrt(dabs(0.5d0*rm(2)**2+0.5d0*rm(3)**2-0.25d0*rm(1)**2))
          
      ac0 = -114.5d0
      delta = 0.1d0
      R00 = 10.d0
      ba = 1.d0
      alphaa = dlog(100.d0 / delta - 1.d0) / ba                                                    
          
      ffupa = 1.d0 / (1.d0 + dexp(2.d0 * alphaa * (R0 - R00)))
      fflowa = 1.d0 / (1.d0 + dexp(-2.d0 * alphaa * (R0 - R00)))
                                                                                                                                                      
      ac = ac * ffupa + ac0 * fflowa

      if(ac.lt.-200.0d0) ac=-200.0d0
      if(ac.gt.0.0d0) ac=0.0d0
      ac = -ac*(x1/xmass(1)+x1/xmass(2)+x1/xmass(3))/x3/cmtoau*
     $     1836.15/1822.89
          
! Asymetric part of AC
!        call potvACasym(Vas, s1,s2,s3)
!        Vas = Vas*(x1/xmass(1)-x1/xmass(2))/x3/cmtoau*1836.15/1822.89

! Relativistic correction
        call potvRCb(vr,s1,s2,s3)

! Asymptotic "cut" of the rel correction
        bu = 3.d0                        
        vr0u = 6.d0
        alphau = dlog(100.d0 / delta - 1.d0) / bu                                                    
        ffup = 1.d0 / (1.d0 + dexp(2.d0 * alphau * (abs(vr) - vr0u)))
                                                                                                              
        vr = vr * ffup
        if(abs(vr).gt.10.0d0) vr=0.0d0
        
        vr = vr / cmtoau
        
! BO+AC+Rel
        v = v + ac + vr

      return      
      end

      subroutine potvRCb(Vrel,P1,P2,P3)

c  Relativistic correction, fit of Barchorz 2009 data
C     UNITS: HARTREE & BOHR
C     symmetric part

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=98)
      dimension cv(ncf), ft(ncf), cv_qed(ncf)

      DATA C0/0.0D0/,RE/1.65000/,BET/1.300D0/
      
      data nv/ ncf/,nfmax/10/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      save icall, cv
      save icall_qed, cv_qed

      icall = icall + 1
      if(icall.eq.1) then
        open(unit=32,status='old',file='f.31.rel')
      do i=1,ncf
        read(32,2) cv(i)
      end do
      close(32)
      end if

      icall_qed = icall_qed + 1
      if(icall_qed.eq.1) then
        open(unit=33,status='old',file='f.31.qed')
      do i=1,ncf
        read(33,2) cv_qed(i)
      end do
      close(33)
      end if

      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
c      FACTOR=BET/RE
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR2+DR2-DR1-DR3)/(SQ2*SQ3)
      SY1=(DR1-DR3)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else
       phi=acos(sx1/se1)
      endif

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      if(mod(k,3).ne.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      V=ZERO
      DO 40 I=1,NV
   40 V=V+(CV(I)+cv_qed(i))*FT(I)
      Vrel = V
      
2     format(f25.13)
      RETURN
      END

c  Potential from Ludwik's calculations of BO and AC
c  Calculates AC
c  input bond lengths in Bohr
      subroutine potvAC(V,p1,p2,p3)     

      implicit real*8(A-H,O-Z)
      double precision Ves, p1,p2,p3
      parameter (ncf=98)
      dimension ft(ncf), cv(ncf)
      data RE/1.65d0/,BET/1.300d0/
      data nfmax/12/
      save icall, cv

      icall = icall + 1
      if(icall.eq.1) then
        open(unit=31,status='old',file='f.31.symAC')
      do i=1,ncf
        read(31,2) cv(i)
2       format(f20.8)
      end do
      close(31)
      end if
      SQ3=SQRT(3.0d0)
      SQ2=SQRT(2.0d0)
      FACTOR=BET/RE
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)
       Y1=(1.0d0-EXP(-FACTOR*DR1))/BET
       Y2=(1.0d0-EXP(-FACTOR*DR2))/BET
       Y3=(1.0d0-EXP(-FACTOR*DR3))/BET
      SA=(Y1+Y2+Y3)/SQ3
      SX=(Y3+Y3-Y1-Y2)/(SQ2*SQ3)
      SY=(Y2-Y1)/SQ2
      SX1=(DR3+DR3-DR1-DR2)/(SQ2*SQ3)
      SY1=(DR2-DR1)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
        phi=acos(0.0d0)
      else
        phi=acos(sx1/se1)
      end if

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) go to 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      Vad=0.0d0

      do i=1,ncf
       Vad=Vad + cv(i)*ft(i)
      end do
      V = Vad 
   
      return
      end

c  Fit of asymmetric part of AC of Ludwik

      subroutine potvACasym(V,P1,P2,P3)   

c   Calculation of asymAC
C     H3+ asymAC IN 7TH ORDER Morse fit to experimental data 
C     based on the ab initio potential of Ludwik
C     asymmetric part

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=98)
      dimension ft(ncf), cv(ncf)
      DATA C0/0.0D0/,RE/1.65000/,BET/1.300D0/,SCALE/1.0D-6/
      data nv/ ncf/,nfmax/9/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      save icall, cv

      icall = icall + 1
      if(icall.eq.1) then
       open(unit=31, status='old', file='f.31.asymAC')
      do i=1,ncf
       read(31,2) cv(i)
2      format(f20.5)
      end do
       close(31)
      end if
      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR3+DR3-DR1-DR2)/(SQ2*SQ3)
      SY1=(DR1-DR2)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else 
       phi=acos(sx1/se1)
      endif 

! Polynomial for asymmetric AC
      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=1,norder-n
      if(mod(k,3).eq.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      V=ZERO
      DO 40 I=1,NV
   40 V=V+CV(I)*FT(I)
      RETURN
      END

