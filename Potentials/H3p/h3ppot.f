      SUBROUTINE potv(V,R1,R2,xcos)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! This subroutine provides a potential energy surface (PES) for the H3+,
! H2D+, D2H+ and D3+ molecules.  
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! SHORT SUMMARY OF THE INPUT/OUTPUT PARAMETERS
!
! INPUT PARAMETERS: R1,R2,xcos                
!    R1 and R2 are the values of the stretching-type coordinates, which
!    should be given in bohrs
!    xcos is the cosine of the angle-type coordinate 
! COMMON VARIABLE INPUT PARAMETERS: XMASS(1),XMASS(2),XMASS(3),G1,G2
!    XMASS(1),XMASS(2),XMASS(3) store the masses of the nuclei, and
!    should be in unified atomic mass units
!    G1 and G2 are constants defining the geometrical meaning of R1,R2
!    and xcos, and are unit independent 
! OUTPUT PARAMETERS: V            
!    V is given in hartrees
!
!
! IMPORTANT DETAILS ON THE INPUT PARAMETERS:
!
! This subroutine uses generalized Sutcliffe-Tennyson triatomic
! coordinates 
! (for details see Figure 1. of B. Sutcliffe and J. Tennyson, Int. J.
! Quantum Chem., Vol.XXXIX, 183-196 (1991) ), 
! thus the geometrical meaning of the input R1, R2 and xcos values
! depend on the values of the G1 and G2 
! COMMON variables, which should be provided by the user.
!
! For the case of mixed isotopolouges, the values of XMASS(i) i=1,2,3
! require special attention!
!  For H2D+ : XMASS(1)= mass of D, XMASS(2)=XMASS(3)= mass of H. 
!  For D2H+ : XMASS(1)= mass of H, XMASS(2)=XMASS(3)= mass of D.
!
!
! SOME CLARIFYING EXAMPLES:
!
! A) Input variables for H2D+ in valence coordinates:
!   XMASS(1)=2.013810d0  XMASS(2)=1.007537d0  XMASS(3)=1.007537d0
!   G1=0.0d0
!   G2=0.0d0
!   In this case R1 and R2 are simple bond lengths, while xcos is the
!   cosine of the bond angle defined by the two bonds corresponding to
!   R1 and R2.
! 
! B) Input variables for D2H+ in Jacobi (scattering) coordinates:
!   XMASS(1)=1.007537d0  XMASS(2)=2.013810d0  XMASS(3)=2.013810d0
!   G1=0.5d0
!   G2=0.0d0
!   In this case R1 is the D-D distance, R2 is the distance between the
!   H atom and the center of mass of the D-D diatom and xcos is the 
!   cosine of the angle defined by the D-D bond and the vector pointing
!   to the H atom from the center of mass of the D-D diatom. 
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      IMPLICIT DOUBLE PRECISION (A-H,O-Y),logical(z)
      COMMON /MASS/ XMASS(3),G1,G2,xmassr(3)
      dimension :: rm(3)
      DATA TOANG/0.5291772/, CMTOAU/219474.631/
      DATA A/2.22600/
      DATA X1/1.0D0/,X0/0.0D0/,TINY/9.0D-15/,X2/2.0D0/,x3/3.0d0/

         pi=dacos(-1.d0)
!     Equilibrium

      IF (G1 .EQ. X0) THEN
!        BONDLENGTH BONDANGLE COORDINATES: ATOM 1 = ATOM 2
         Q1 = R1
         Q2 = R2
         THETA = ACOS(XCOS)
! KES added line below
         COST = XCOS
      ELSE IF (G2 .EQ. X0) THEN
!        SCATTERING COORDINATES: ATOM 2 = ATOM 3
         XX = R1 * G1
         YY = R1 * (X1 - G1)
         IF (R2 .EQ. X0 .OR. XCOS .GE. (X1 - TINY)) THEN
            Q1 = ABS(XX - R2)
            Q2 = (YY + R2)
            COST = -X1
         ELSE IF (XCOS .LE. (TINY - X1)) THEN
            Q1 = (XX + R2)
            Q2 = ABS(YY + R2)
            COST = X1
         ELSE
            Q1 = SQRT(XX*XX + R2*R2 - X2*XX*R2*XCOS)
            Q2 = SQRT(YY*YY + R2*R2 + X2*YY*R2*XCOS)
            COST = (Q1**2 + Q2**2 - R1**2) / (X2 * Q1 * Q2)
         ENDIF
         THETA = ACOS(COST)
      ELSE
!        GENERAL COORDINATES (INCLUDING RADAU): ATOM 1 = ATOM 2
         F1= X1/G1
         F2= X1/G2
         F12= X1 - F1*F2
         P1= R1*(X1-F1)/(G2*F12)
         P2= R2*(X1-F2)/(G1*F12)
         S1= R1-P1
         S2= R2-P2
         Q1= SQRT(P1*P1 + S2*S2 + X2*P1*S2*XCOS)/(X1-G1)
         Q2= SQRT(P2*P2 + S1*S1 + X2*P2*S1*XCOS)/(X1-G2)
         Q3= SQRT(P1*P1 + P2*P2 - X2*P1*P2*XCOS)
         COST = (Q1*Q1 + Q2*Q2 - Q3*Q3)/(X2*Q1*Q2)
         THETA = ACOS(COST)
      ENDIF

!  s1,s2,s3 - distances between atoms in Bohrs
      s1 = Q1
      s2 = Q2
      s3=SQRT(s1*s1+s2*s2-2.0d0*s1*s2*COST)

! BO: fit260I
      call newpot(s1,s2,s3,pot)
! Making the bottom of the well = zero in energy
       v=pot + 1.3438355735473d0
1      format(4f10.2)    

! AC symmetric part     
      call potvAC(ac,s1,s2,s3)

! Asymptotic "cut" of the DA correction
      rm(1) = s1
      rm(2) = s2
      rm(3) = s3
          
      jmin = -1
      rmin = rm(1)
          
      do 100 j = 1, 3
        if(rm(j) .lt. rmin) then
          rmin = rm(j)
          jmin = j
        end if
100   continue

      if(jmin .gt. 1) then
        rmin = rm(1)
        rm(1) = rm(jmin)
        rm(jmin) = rmin
      end if
              
      R0 = dsqrt(dabs(0.5d0*rm(2)**2+0.5d0*rm(3)**2-0.25d0*rm(1)**2))
          
      ac0 = -114.5d0
      delta = 0.1d0
      R00 = 10.d0
      ba = 1.d0
      alphaa = dlog(100.d0 / delta - 1.d0) / ba                                                    
          
      ffupa = 1.d0 / (1.d0 + dexp(2.d0 * alphaa * (R0 - R00)))
      fflowa = 1.d0 / (1.d0 + dexp(-2.d0 * alphaa * (R0 - R00)))
                                                                                                                                                      
      ac = ac * ffupa + ac0 * fflowa

      if(ac.lt.-200.0d0) ac=-200.0d0
      if(ac.gt.0.0d0) ac=0.0d0
      ac = -ac*(x1/xmass(1)+x1/xmass(2)+x1/xmass(3))/x3/cmtoau*
     $     1836.15/1822.89
          
! Asymetric part of AC
!        call potvACasym(Vas, s1,s2,s3)
!        Vas = Vas*(x1/xmass(1)-x1/xmass(2))/x3/cmtoau*1836.15/1822.89

! Relativistic correction
        call potvRCb(vr,s1,s2,s3)

! Asymptotic "cut" of the rel correction
        bu = 3.d0                        
        vr0u = 6.d0
        alphau = dlog(100.d0 / delta - 1.d0) / bu                                                    
        ffup = 1.d0 / (1.d0 + dexp(2.d0 * alphau * (abs(vr) - vr0u)))
                                                                                                              
        vr = vr * ffup
        if(abs(vr).gt.10.0d0) vr=0.0d0
        
        vr = vr / cmtoau
        
! BO+AC+Rel
        v = v + ac + vr

      return      
      end

      subroutine potvRCb(Vrel,P1,P2,P3)

c  Relativistic correction, fit of Barchorz 2009 data
C     UNITS: HARTREE & BOHR
C     symmetric part

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=98)
      dimension cv(ncf), ft(ncf), cv_qed(ncf)

      DATA C0/0.0D0/,RE/1.65000/,BET/1.300D0/
      
      data nv/ ncf/,nfmax/10/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      save icall, cv
      save icall_qed, cv_qed

      icall = icall + 1
      if(icall.eq.1) then
        open(unit=32,status='old',file='f.31.rel')
      do i=1,ncf
        read(32,2) cv(i)
      end do
      close(32)
      end if

      icall_qed = icall_qed + 1
      if(icall_qed.eq.1) then
        open(unit=33,status='old',file='f.31.qed')
      do i=1,ncf
        read(33,2) cv_qed(i)
      end do
      close(33)
      end if

      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
c      FACTOR=BET/RE
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR2+DR2-DR1-DR3)/(SQ2*SQ3)
      SY1=(DR1-DR3)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else
       phi=acos(sx1/se1)
      endif

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      if(mod(k,3).ne.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      V=ZERO
      DO 40 I=1,NV
   40 V=V+(CV(I)+cv_qed(i))*FT(I)
      Vrel = V
      
2     format(f25.13)
      RETURN
      END

c  Potential from Ludwik's calculations of BO and AC
c  Calculates AC
c  input bond lengths in Bohr
      subroutine potvAC(V,p1,p2,p3)     

      implicit real*8(A-H,O-Z)
      double precision Ves, p1,p2,p3
      parameter (ncf=98)
      dimension ft(ncf), cv(ncf)
      data RE/1.65d0/,BET/1.300d0/
      data nfmax/12/
      save icall, cv

      icall = icall + 1
      if(icall.eq.1) then
        open(unit=31,status='old',file='f.31.symAC')
      do i=1,ncf
        read(31,2) cv(i)
2       format(f20.8)
      end do
      close(31)
      end if
      SQ3=SQRT(3.0d0)
      SQ2=SQRT(2.0d0)
      FACTOR=BET/RE
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)
       Y1=(1.0d0-EXP(-FACTOR*DR1))/BET
       Y2=(1.0d0-EXP(-FACTOR*DR2))/BET
       Y3=(1.0d0-EXP(-FACTOR*DR3))/BET
      SA=(Y1+Y2+Y3)/SQ3
      SX=(Y3+Y3-Y1-Y2)/(SQ2*SQ3)
      SY=(Y2-Y1)/SQ2
      SX1=(DR3+DR3-DR1-DR2)/(SQ2*SQ3)
      SY1=(DR2-DR1)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
        phi=acos(0.0d0)
      else
        phi=acos(sx1/se1)
      end if

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) go to 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      Vad=0.0d0

      do i=1,ncf
       Vad=Vad + cv(i)*ft(i)
      end do
      V = Vad 
   
      return
      end

c  Fit of asymmetric part of AC of Ludwik

      subroutine potvACasym(V,P1,P2,P3)   

c   Calculation of asymAC
C     H3+ asymAC IN 7TH ORDER Morse fit to experimental data 
C     based on the ab initio potential of Ludwik
C     asymmetric part

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=98)
      dimension ft(ncf), cv(ncf)
      DATA C0/0.0D0/,RE/1.65000/,BET/1.300D0/,SCALE/1.0D-6/
      data nv/ ncf/,nfmax/9/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      save icall, cv

      icall = icall + 1
      if(icall.eq.1) then
       open(unit=31, status='old', file='f.31.asymAC')
      do i=1,ncf
       read(31,2) cv(i)
2      format(f20.5)
      end do
       close(31)
      end if
      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR3+DR3-DR1-DR2)/(SQ2*SQ3)
      SY1=(DR1-DR2)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else 
       phi=acos(sx1/se1)
      endif 

! Polynomial for asymmetric AC
      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=1,norder-n
      if(mod(k,3).eq.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
      V=ZERO
      DO 40 I=1,NV
   40 V=V+CV(I)*FT(I)
      RETURN
      END



      subroutine newpot(r1, r2, r3, Vai)
  
      implicit none
      double precision r1, r2, r3, V, fac, min, Vai, Vlow, 
     *Vaugcm, Vaf, Vafcm, rmin, R, Vair, Vaie
      double precision E0l, E0u, E02, de2, ga02, ga12, ga2, 
     *ffup, fflow, ffup2, fflow2, delta, alphal, alphau, bl, bu
      double precision R0, br, alphar, ffupr, fflowr
      integer i, imin
      double precision, dimension(3) :: Vaug, V3bt, rm
      
      min = -1.3438355735473d0
      fac = 219474.631d0
      V = 0.d0
      Vai = 0.d0
      
c     Accuracy of reaching asymptotic values in b cm-1 range in %
      delta = 0.1d0 
      bl = 100.d0
      bu = 300.d0
      
      E0l = 37000.d0
      E0u = 48000.d0
      
      alphal = dlog(100.d0 / delta - 1.d0) / bl
      alphau = dlog(100.d0 / delta - 1.d0) / bu
      
      call singlet(r1, r2, r3, V3bt, 0)
      call DIMH3P(r1, r2, r3, Vaug(1), Vaug(2), Vaug(3))
      call newpot_V(r1, r2, r3, Vaf)
      
      Vaugcm = (Vaug(1) - min) * fac
      Vafcm = (Vaf - min) * fac
  
      fflow = 1.d0 / (1.d0 + dexp(-2.d0 * alphal * (Vaugcm - E0l)))
      ffup = 1.d0 / (1.d0 + dexp(2.d0 * alphau * (Vaugcm - E0u)))

      call dim_ai_diff_surf(V, r1, r2, r3)
      
      V = V * fflow * ffup
      
      Vlow = (V3bt(1) - min) * fac  - V
      
      E02 = 42000.d0
      de2 = Vafcm - E02
       
      ga02 = 1.71d0 / fac / fac / 100.d0
      ga12 = 2000.0d0 / fac / fac / 100.d0
      ga2 = ga02 + ga12 * (de2**2)
      
      ffup2 = 0.5d0 * (1.d0 + dtanh(ga2*de2))
      fflow2 = 0.5d0 * (1.d0 + dtanh(-ga2*de2))
      
      Vaie = fflow2 * Vlow + ffup2 * Vafcm                                                                                

      rm(1) = r1
      rm(2) = r2
      rm(3) = r3
      
      imin = -1
      rmin = rm(1)
      
      do 100 i = 1, 3
        if (rm(i) .lt. rmin) then 
          rmin = rm(i)
          imin = i
        end if
100   continue
      
      if (imin .gt. 1) then 
        rmin = rm(1)
        rm(1) = rm(imin)
        rm(imin) = rmin
      end if
      
      R = dsqrt(dabs(0.5d0*rm(2)**2 + 0.5d0*rm(3)**2 - 0.25d0*rm(1)**2))
      
      R0 = 18.d0
      br = 1.d0
    
      alphar = dlog(100.d0 / delta - 1.d0) / br
      
      fflowr = 1.d0 / (1.d0 + dexp(-2.d0 * alphar * (R - R0)))
      ffupr = 1.d0 / (1.d0 + dexp(2.d0 * alphar * (R - R0)))

      Vair = ffupr * Vaie + fflowr * Vaugcm

      Vai = Vair / fac + min      

      return
      end

      subroutine newpot_V (r1, r2, r3, Vai)
  
      implicit none
      double precision r1, r2, r3, V, Vacm, fac, min, 
     *Vai, Vlow
      double precision E0l, E0u, ffup, fflow, delta, 
     *alphal, alphau, bl, bu
      double precision, dimension(3) :: Va
      
      min = -1.3438355735473d0
      fac = 219474.631d0
      V = 0.d0
      Vai = 0.d0
      
c     Accuracy of reaching asymptotic values in b cm-1 range in %
      delta = 0.1d0 
      bl = 80.d0
      bu = 1400.d0
      
      E0l = 40000.d0
      E0u = 55000.d0
      
      alphal = dlog(100.d0 / delta - 1.d0) / bl
      alphau = dlog(100.d0 / delta - 1.d0) / bu
      
      call DIMH3P(r1, r2, r3, Va(1), Va(2), Va(3))
      Vacm = (Va(1) - min) * fac

      fflow = 1.d0 / (1.d0 + dexp(-2.d0 * alphal * (Vacm - E0l)))
      ffup = 1.d0 / (1.d0 + dexp(2.d0 * alphau * (Vacm - E0u)))

      call V_ai_diff_surf(V, r1, r2, r3)
      
      V = V * fflow * ffup
      
      Vai = (Va(1) - min) * fac  - V
      
      Vai = Vai / fac + min      
      
      return
      end

      subroutine dim_ai_diff_surf(V, P1, P2, P3)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=31)
      dimension cv(ncf), ft(ncf)

      DATA RE/1.65000/
      DATA nfmax/7/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      DATA cv/ -0.75143071, -1.76440106, -0.09859390, -0.99200748,  
     *1.48567895, -4.46772673, -3.90622308, -0.37926323, 1.53719611,  
     *2.48855132, 1.37657184, -0.01544350, 0.07169542, -0.37054284, 
     *-0.68993265, -0.20471568, 0.00037056, 0.00167004, 0.00764265, 
     *-0.00024218, 0.02173168, 0.04686634, -0.00943583, 0.00077371, 
     *-0.00681003, -0.00285356, 0.01863322, 0.01297023, -0.01472313,  
     *0.00114263, -0.01094351/

      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR2+DR2-DR1-DR3)/(SQ2*SQ3)
      SY1=(DR1-DR3)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else
       phi=acos(sx1/se1)
      endif

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      if(mod(k,3).ne.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
  
      V=ZERO
      DO 40 i=1,ncf
   40 V=V+cv(i)*ft(i)
      RETURN
      END

      subroutine V_ai_diff_surf(V, P1, P2, P3)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      parameter (ncf=30)
      dimension cv(ncf), ft(ncf)

      DATA RE/1.65000/
      DATA nfmax/7/
      DATA ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,THREE/3.0D0/
      DATA cv/28.89617715, -10.58157805, 2.87604608, 2.08628333, 
     *-0.62636049, -0.51256565, -1.15962111, 0.13476748, -0.08342574, 
     *0.29409500, 0.10614684, -0.02831679, 0.08313826, -0.01860622, 
     *-0.10208867, -0.03815698, 0.00295364, -0.01213435, 0.00056655, 
     *0.01681600, 0.00053141, -0.00595500, -0.00116108, -0.00010578, 
     *0.00050616, -0.00000479, -0.00079326, 0.00002889, 0.00040228, 
     *0.00004892/

      SQ3=SQRT(THREE)
      SQ2=SQRT(TWO)
      DR1= (P1-RE)
      DR2= (P2-RE)
      DR3= (P3-RE)

c Displacement coordinates
       Y1=DR1
       Y2=DR2
       Y3=DR3
      SA=(Y1+Y2+Y3)/SQ3
      SX1=(DR2+DR2-DR1-DR3)/(SQ2*SQ3)
      SY1=(DR1-DR3)/SQ2
      QUAD1=SX1**2+SY1**2
      SE1=SQRT(QUAD1)
      if(abs(se1).lt.1.0e-10) then
       phi=acos(0.0d0)
      else
       phi=acos(sx1/se1)
      endif

      npot=1
      ft(1)=1.0d0
      do 100 norder=1,nfmax
      do 100 n=norder,0,-1
      do 100 k=0,norder-n,3
      if(mod(k,3).ne.0) goto 100
      m=norder-k-n
      if (mod(m,2) .ne. 0) goto 100
      npot=npot+1
      if(npot.gt.ncf) goto 100
      ft(npot)=sa**n * se1**(m+k) * cos(dble(k)*phi)
  100 continue
  
      V=ZERO
      DO 40 i=1,ncf
   40 V=V+cv(i)*ft(i)
      RETURN
      END

c #####################################################
c POTENTIAL

c r1,r2,r3 --> interparticle distances
c pot      --> 3 dimension vector ( 3 adiabatic sheets )

c Call the potential with:

c call singlet(r1,r2,r3,pot)
c ######################################################
      
      subroutine singlet(r1,r2,r3,pot,idimp)
      implicit none
      integer nparam,npol,order
      integer np,inst,nst,nn,idimp,idimp2
      parameter(nn=297)
      double precision r1,r2,r3,pot(3)
      double precision h(3,3),tb(5),dim(3,3),xdiag,xodiag
      double precision gam1,x0,r0,beta,x
      double precision minad,sumad,rmax
      common/polorder/npol(5),order(5,5)
      common/switch/gam1(5,5),x0(5,5),r0(5,5),beta(5,5)
      common/param/np(5)
      common/coef3/x(nn)
c    
c     if idimp=0  the three-body terms are used
c     else        pure DIM potential

c  isolate points with two large distances > rmax
      rmax=9.0d0
      minad=min(r1,r2,r3)
      sumad=r1+r2+r3
      if (sumad-minad.gt.2.0d0*rmax.or.minad.gt.3.5d0) then
c  pure DIM
         idimp2=1
      else
c  if idimp=0 potential with three-body terms 
c  if idimp=1 pure DIM
         idimp2=idimp
      end if

c
c  parameters of polorder and switch are set in block data co3bdnl
c  parameters of coef3 are set in block data co3bdl
c  attention: the parameter "nn" should be set as in co3bdl
c
c  "co3bdl" may be created running the code in mblockd.f
c
c  determine number of parameters
      np(1)=nparam(order(1,1))+nparam(order(2,1))
      np(2)=nparam(order(1,2))
      np(3)=nparam(order(1,3)) 
      np(4)=nparam(order(1,4)) 
      np(5)=nparam(order(1,5)) 

      tb(1)=0.0d0
      tb(2)=0.0d0
      tb(3)=0.0d0
      tb(4)=0.0d0
      tb(5)=0.0d0

      if (idimp2.eq.0) then
      inst=1

      do nst=1,5
         call thrbody(x,r1,r2,r3,tb(nst),nst,inst)
         inst=inst+np(nst)
      enddo
      end if

      xdiag=tb(1)
      xodiag=tb(2)

      call dimpot(r1,r2,r3,xdiag,xodiag,h,dim,pot)

        pot(1)=pot(1) + tb(3)
        pot(2)=pot(2) + tb(4)
        pot(3)=pot(3) + tb(5)

      return
      end

      block data co3bdnl
c  sets non-linear parameters of three-body term
      implicit none
      integer npol,order
      double precision gam1,x0,r0,beta
      common/polorder/npol(5),order(5,5)
      common/switch/gam1(5,5),x0(5,5),r0(5,5),beta(5,5)
               
c  orders of polynomials:
c    if order=-1, no polynomial
c    if order= 0, only constant term
c    if order= 1, terms of order 0 and 1
c    if order= 2, terms of order 0, 1 and 2
c    etc.
c
c number of polynomials of the diagonal
      data npol(1)    /2/
c first polynomial of the diagonal
      data gam1(1,1)  /0.3d0/
      data r0(1,1)    /1.65d0/
      data beta(1,1)  /1.3d0/
      data x0(1,1)    /12.0d0/
      data order(1,1) /-1/
c second polynomial of the diagonal
      data gam1(2,1)  /0.3d0/
      data r0(2,1)    /2.5d0/
      data beta(2,1)  /1.0d0/
      data x0(2,1)    /14.0d0/
      data order(2,1) /15/

c polynomial of the off-diagonal
      data npol(2)    /1/
      data gam1(1,2)  /0.3d0/
      data r0(1,2)    /2.5d0/
      data beta(1,2)  /1.0d0/
      data x0(1,2)    /14.0d0/
      data order(1,2) /13/
      
c polynomial outside DIM matrix for ground state
      data npol(3)    /1/
      data gam1(1,3)  /0.3d0/
      data r0(1,3)    /2.5d0/
      data beta(1,3)  /1.0d0/
      data x0(1,3)    /10.0d0/
      data order(1,3) /-1/
      
c polynomial outside DIM matrix for first excited state
      data npol(4)    /1/
      data gam1(1,4)  /0.3d0/
      data r0(1,4)    /2.5d0/
      data beta(1,4)  /1.0d0/
      data x0(1,4)    /12.0d0/
      data order(1,4) /-1/
      
c polynomial outside DIM matrix for second excited state
      data npol(5)    /1/
      data gam1(1,5)  /0.3d0/
      data r0(1,5)    /2.5d0/
      data beta(1,5)  /1.0d0/
      data x0(1,5)    /12.0d0/
      data order(1,5) /-1/
      
      end

      
      block data co3bdl 
      double precision x
      common /coef3/ x( 297)
      data x / 0.2038135752082E-01 , 
     &        -0.4502653609961E-02 , 
     &        -0.2049733139575E-01 , 
     &         0.1388978585601E-01 , 
     &         0.2624774724245E-01 , 
     &        -0.5493988282979E-03 , 
     &         0.2584048919380E-01 , 
     &         0.3755598887801E-01 , 
     &         0.1371347345412E-01 , 
     &        -0.1439545862377E-01 , 
     &        -0.2458681911230E-01 , 
     &        -0.8049705065787E-02 , 
     &         0.3551620244980E-01 , 
     &         0.5522616580129E-01 , 
     &        -0.1107459589839E+00 , 
     &        -0.2481598639861E-02 , 
     &        -0.7615165784955E-02 , 
     &        -0.1556981354952E-02 , 
     &        -0.2044748142362E-01 , 
     &         0.3450882650213E-04 , 
     &        -0.2876006998122E-01 , 
     &         0.3585752798244E-02 , 
     &         0.5190435051918E-01 , 
     &        -0.1708707539365E-02 , 
     &         0.2274964749813E-01 , 
     &         0.1964472758118E-03 , 
     &         0.2397969597951E-02 , 
     &         0.2068273315672E-03 , 
     &        -0.5980014428496E-01 , 
     &        -0.8253587409854E-02 , 
     &        -0.4225731641054E-01 , 
     &        -0.1474103238434E-01 , 
     &        -0.9330268949270E-02 , 
     &        -0.6015709415078E-01 , 
     &         0.1426827609539E+00 , 
     &         0.1501764813838E-05 , 
     &        -0.7821730524302E-01 , 
     &        -0.9919969737530E-01 , 
     &         0.8972355723381E-01 , 
     &        -0.1051881723106E-01 , 
     &        -0.1506378054619E+00 , 
     &        -0.1030388195068E-01 , 
     &        -0.1700490526855E-01 , 
     &        -0.2058380693197E+00 , 
     &        -0.8895791321993E-01 , 
     &        -0.1030693203211E+00 , 
     &         0.1231466904283E+00 , 
     &         0.2777793817222E-01 , 
     &         0.1696047693258E-03 , 
     &         0.2040153890848E+00 , 
     &         0.3578749597073E+00 , 
     &        -0.5582627654076E-01 , 
     &        -0.4295857623219E-01 , 
     &         0.1307931524934E-03 , 
     &        -0.3089929744601E-01 , 
     &        -0.3914486244321E-01 , 
     &        -0.2266425266862E-01 , 
     &        -0.3675921559334E+00 , 
     &         0.1150405555964E+00 , 
     &         0.8040013313293E+00 , 
     &         0.8343981951475E-01 , 
     &         0.9832771420479E+00 , 
     &         0.1498659372330E+01 , 
     &         0.1561497449875E+01 , 
     &         0.4981927871704E+00 , 
     &         0.3854436874390E+00 , 
     &         0.5157361552119E-01 , 
     &        -0.5597009789199E-02 , 
     &        -0.8208463899791E-02 , 
     &         0.2799553871155E+00 , 
     &         0.1264895796776E+00 , 
     &        -0.8795589674264E-03 , 
     &         0.4569451212883E+00 , 
     &         0.3874383270741E+00 , 
     &         0.2551791965961E+00 , 
     &         0.1009184122086E+01 , 
     &         0.2745246887207E+00 , 
     &         0.1018496394157E+01 , 
     &         0.1291464686394E+01 , 
     &         0.1277048707008E+01 , 
     &         0.5290365219116E-01 , 
     &         0.9185030460358E+00 , 
     &         0.3099000081420E-01 , 
     &         0.1510376925580E-02 , 
     &         0.9264567052014E-04 , 
     &         0.4045693203807E-01 , 
     &         0.3528299555182E-01 , 
     &         0.1624061316252E+00 , 
     &         0.7760862112045E+00 , 
     &         0.1307412516326E-01 , 
     &         0.1503127068281E+00 , 
     &        -0.1806402653456E+00 , 
     &         0.1454018354416E+00 , 
     &        -0.8989291787148E+00 , 
     &        -0.2468922138214E+01 , 
     &        -0.2391477674246E+00 , 
     &        -0.2874656677246E+01 , 
     &        -0.3005650281906E+01 , 
     &        -0.2573872089386E+01 , 
     &        -0.3544934093952E+00 , 
     &         0.1113087534904E+00 , 
     &        -0.1227685366757E-02 , 
     &         0.1945724524558E-01 , 
     &         0.2700309082866E-01 , 
     &        -0.1965133659542E-01 , 
     &        -0.2602054774761E+00 , 
     &        -0.1566321551800E+00 , 
     &         0.1117114797235E+00 , 
     &        -0.3906232118607E+00 , 
     &        -0.4097225964069E+00 , 
     &        -0.4356949925423E+00 , 
     &         0.4296246469021E+00 , 
     &        -0.2132443428040E+01 , 
     &        -0.1110988020897E+01 , 
     &        -0.1931788206100E+01 , 
     &        -0.3352178335190E+01 , 
     &        -0.2033104300499E+00 , 
     &        -0.3341197252274E+01 , 
     &        -0.2821970701218E+01 , 
     &        -0.2959844112396E+01 , 
     &        -0.3598988354206E+00 , 
     &        -0.6201191544533E+00 , 
     &         0.8133341680150E-06 , 
     &         0.1234306255355E-02 , 
     &         0.1268017664552E-01 , 
     &        -0.5187008995563E-02 , 
     &        -0.1626813709736E+00 , 
     &        -0.3367761671543E+00 , 
     &         0.1109622512013E-01 , 
     &        -0.8243380188942E+00 , 
     &        -0.4655699804425E-01 , 
     &        -0.3448127508163E+00 , 
     &        -0.6386389732361E+00 , 
     &        -0.7093213200569E+00 , 
     &        -0.3560272976756E-02 , 
     &         0.2475702613592E+00 , 
     &         0.3179576396942E+00 , 
     &         0.7398064136505E+00 , 
     &         0.1610691308975E+01 , 
     &         0.4166177272797E+01 , 
     &         0.9598708748817E+00 , 
     &         0.3224317550659E+01 , 
     &         0.2147500514984E+01 , 
     &         0.7551590204239E+00 , 
     &        -0.2166554182768E+00 , 
     &        -0.2505114376545E+00 , 
     &         0.9207403287292E-02 , 
     &         0.1126132156060E-04 , 
     &        -0.2862761961296E-02 , 
     &        -0.1534178009024E-03 , 
     &         0.9977171197534E-02 , 
     &        -0.1740714460611E+00 , 
     &         0.1246334798634E-01 , 
     &        -0.2196481227875E+00 , 
     &        -0.1066186577082E+00 , 
     &        -0.6484547257423E-01 , 
     &        -0.2513209283352E+00 , 
     &         0.3015027567744E-01 , 
     &        -0.3334693908691E+00 , 
     &         0.5404766201973E+00 , 
     &         0.8669694662094E+00 , 
     &         0.5779470801353E+00 , 
     &         0.1180241554976E+00 , 
     &         0.2760182857513E+01 , 
     &         0.1963732719421E+01 , 
     &         0.2369036197662E+01 , 
     &         0.4611462593079E+01 , 
     &         0.8045859336853E+00 , 
     &         0.3192297935486E+01 , 
     &         0.2113111972809E+01 , 
     &         0.1158948063850E+01 , 
     &        -0.2899852395058E-01 , 
     &         0.3210568428040E-01 , 
     &         0.4643308464438E-02 , 
     &         0.5794972926378E-01 , 
     &         0.2163177281618E+00 , 
     &         0.3456968755700E-07 , 
     &        -0.7665733825490E-08 , 
     &         0.3650718182325E-01 , 
     &        -0.6945794820786E-01 , 
     &        -0.5244998261333E-01 , 
     &        -0.1063322369009E-01 , 
     &         0.1378841549158E+00 , 
     &        -0.3282044827938E+00 , 
     &        -0.5219735205173E-01 , 
     &        -0.1209799572825E+00 , 
     &        -0.4702143371105E-02 , 
     &        -0.6626180559397E-01 , 
     &         0.2407051852060E-05 , 
     &         0.1468833833933E+00 , 
     &        -0.9982571646105E-04 , 
     &         0.2347189188004E-01 , 
     &         0.4199467948638E-03 , 
     &         0.1984704583883E+00 , 
     &        -0.1109232529998E+00 , 
     &        -0.1109396368265E+00 , 
     &        -0.2261913381517E-01 , 
     &         0.2691740883165E-04 , 
     &         0.2081657201052E+00 , 
     &         0.1839612275362E+00 , 
     &         0.5080552101135E+00 , 
     &        -0.1613639108837E-01 , 
     &         0.3844989538193E+00 , 
     &         0.1302360445261E+00 , 
     &        -0.3850560188293E+00 , 
     &        -0.1574946641922E+00 , 
     &        -0.1519335508347E+00 , 
     &        -0.2564406394958E+00 , 
     &         0.4450597465038E+00 , 
     &        -0.2165480405092E+00 , 
     &        -0.3114369213581E+00 , 
     &         0.2186658978462E+00 , 
     &         0.6476427316666E+00 , 
     &         0.3730938732624E+00 , 
     &        -0.1092024073005E+00 , 
     &        -0.9590282291174E-01 , 
     &        -0.1192676718347E-03 , 
     &        -0.6356548666954E+00 , 
     &         0.1947915554047E+00 , 
     &         0.6025955080986E+00 , 
     &         0.2409428507090E+00 , 
     &         0.8307718038559E+00 , 
     &         0.7525328397751E+00 , 
     &         0.1109323501587E+01 , 
     &         0.6152945756912E+00 , 
     &         0.1256046380149E-04 , 
     &         0.1692902892828E+00 , 
     &         0.6652852892876E-01 , 
     &        -0.2153564430773E-01 , 
     &        -0.3678647056222E-01 , 
     &         0.8088537305593E-01 , 
     &         0.5307267885655E-02 , 
     &         0.6883329153061E+00 , 
     &         0.2174600362778E+01 , 
     &         0.6245110630989E+00 , 
     &         0.2579672574997E+01 , 
     &         0.4459482669830E+01 , 
     &         0.2777974843979E+01 , 
     &         0.3691339790821E+00 , 
     &         0.5397902727127E+00 , 
     &         0.1189048052765E-02 , 
     &        -0.2816260792315E-01 , 
     &        -0.3072485029697E+00 , 
     &         0.7174132466316E+00 , 
     &        -0.5216906666756E+00 , 
     &        -0.4354291260242E+00 , 
     &        -0.3040901422501E+00 , 
     &        -0.3592750523239E-02 , 
     &        -0.9823914766312E+00 , 
     &        -0.6712844371796E+00 , 
     &        -0.1427285722457E-02 , 
     &         0.6134739518166E+00 , 
     &         0.6469817757607E+00 , 
     &         0.2023749053478E+00 , 
     &         0.2512786984444E+00 , 
     &         0.8785905838013E+00 , 
     &        -0.1337261050940E+00 , 
     &        -0.6628892384470E-02 , 
     &        -0.3364108800888E+00 , 
     &        -0.5055920407176E-01 , 
     &         0.1961876749992E+00 , 
     &         0.1384985866025E-02 , 
     &        -0.7081563025713E-01 , 
     &        -0.7799867987633E+00 , 
     &         0.7183402776718E-01 , 
     &        -0.1918944001198E+01 , 
     &        -0.1496339321136E+01 , 
     &        -0.2778274774551E+01 , 
     &        -0.5837337970734E+01 , 
     &        -0.1115664243698E+01 , 
     &        -0.3906371831894E+01 , 
     &        -0.4546409130096E+01 , 
     &        -0.3241823196411E+01 , 
     &        -0.6220843270421E-01 , 
     &         0.2666761279106E+00 , 
     &        -0.6738465279341E-01 , 
     &        -0.6620140373707E-01 , 
     &        -0.5357830226421E-01 , 
     &         0.2084175050259E+00 , 
     &        -0.3408109247684E+00 , 
     &         0.2126413285732E+00 , 
     &         0.4915786087513E+00 , 
     &        -0.5661582350731E+00 , 
     &         0.2188280783594E-01 , 
     &         0.5355895336834E-04 , 
     &         0.3853269517422E+00 , 
     &        -0.1088703632355E+01 , 
     &        -0.7743591070175E+00 , 
     &        -0.2267065197229E+00 , 
     &        -0.2393134385347E+00 , 
     &        -0.3650400936604E+00 , 
     &         0.1053227926604E-02 , 
     &        -0.1342126488686E+01 , 
     &        -0.1051973462105E+01 , 
     &        -0.1317051351070E+00 , 
     &         0.1048775576055E-01 , 
     &        -0.6959872320294E-02 / 
      end
      
      subroutine thrbody(x,d1,d2,d3,v,nst,inst)
      implicit none
      double precision x,d1,d2,d3,v
      integer nst,inst
      double precision A,Q,q1,q2,q3,r
      double precision g1p,g2p,g3p,rho,damp,pow
      double precision gam1,x0,r0,beta
      integer i,j,k,l,num,nr
      integer np,order,npol,idimp
      dimension R(3),A(3,3),Q(3),x(*)
      common/polorder/npol(5),order(5,5)
      common/switch/gam1(5,5),x0(5,5),r0(5,5),beta(5,5)
      common/param/np(5)
      R(1)=d1
      R(2)=d2
      R(3)=d3
      
      rho=dsqrt((R(1)**2+R(2)**2+R(3)**2)/dsqrt(3.0d0))
      A(1,1)=dsqrt(1.d0/3.d0)
      A(1,2)=A(1,1)
      A(1,3)=A(1,1)
      A(2,1)=0.d0
      A(2,2)=dsqrt(1.d0/2.d0)
      A(2,3)=-A(2,2)
      A(3,1)=dsqrt(2.d0/3.d0)
      A(3,2)=-dsqrt(1.d0/6.d0)
      A(3,3)=A(3,2)
      
      num=inst-1
      
      V=0.0d0
      
      do nr=1,npol(nst)
        
        do 100 i=1,3
          Q(i)=0.d0
          do 100 j=1,3
            Q(i)=Q(i)+A(i,j)*
     &        (1.d0-dexp(-beta(nr,nst)*(R(j)/R0(nr,nst)-
     &        1.d0)))/beta(nr,nst)
 100      continue
          
        q1=Q(1)
        q2=Q(2)**2+Q(3)**2
        q3=Q(3)**3-3.d0*Q(3)*Q(2)**2
          
        do 101 l=0,order(nr,nst)
          do 101 i=0,l
            g1p=pow(q1,i)
            do 101 j=0,(l-i),2
              g2p=pow(q2,j/2)
              k=l-i-j
              if(mod(k,3).EQ.0) then
                g3p=pow(q3,k/3)
                num=num+1
                  V=V+x(num)*g1p*g2p*g3p
     &               *damp(gam1(nr,nst),d1,x0(nr,nst))
     &               *damp(gam1(nr,nst),d2,x0(nr,nst))
     &               *damp(gam1(nr,nst),d3,x0(nr,nst))
     &               *damp(gam1(nr,nst),q1,x0(nr,nst))     ! 16/1/2011
     &               *damp(gam1(nr,nst),q2,x0(nr,nst))     ! 16/1/2011
     &               *damp(gam1(nr,nst),q3,x0(nr,nst))     ! 16/1/2011
!     &               *damp(gam1(nr,nst),rho,x0(nr,nst))
                endif

 101          continue

            enddo  ! end of npol loop
c
c  avoid instabilities and small distances
c
            V=V*(1.0d0-damp(10.0d0,d1,0.8d0))
     &         *(1.0d0-damp(10.0d0,d2,0.8d0))
     &         *(1.0d0-damp(10.0d0,d3,0.8d0))

c
c   the three-body term may misbehave, exclude it if the conditions
c   below are satified. The potential will then be pure DIM, which is
c   good at large distances.
c
!      if(d1+d2+d3.gt.13.0d0.and.min(d1,d2,d3).gt.2.5d0) v=0.0d0   !  not used


      return
      end

      double precision function pow(x,i)
      integer i
      double precision x
c calculates x**i, with 0**0=1
      if (i.eq.0) then
        pow=1.0d0
      else
        pow=x**i
      end if
      return
      end
          
      real*8 function damp(gam,q,qq)
      real*8 q,gam,qq
      damp=1.d0/(1.d0+dexp(gam*(q-qq)))
      return
      end
        
      subroutine dimpot(r1,r2,r3,xdiag,xodiag,h,dim,pot)
      implicit none
      dimension H(3,3),pot(3),z(3,3)
      double precision h,r1,r2,r3,xh2pd,ah2pd,pothhx,pot,z
      double precision xdiag,xodiag,dim(3,3)
      integer nrot,i,j

c  a. alijah 17/02/2010
c  xdiag:   three-body term to be added to the diagonals
c  xodiag:  three-body term to be added to the off-diagonals
c  simplification to get rid of tb(1-7)
c    e --> tb(1)
c    x --> tb(2)
c    a --> tb(3)
c    b --> tb(4)
c    c --> tb(5)
c    d --> tb(6)
c   xx --> tb(7)

      H(1,1)=xh2pd(r2)+ah2pd(r2)+xh2pd(r3)+ah2pd(r3)
      H(1,1)=0.5d0*H(1,1)+pothhx(r1)+xdiag-1.0d0

      H(2,2)=xh2pd(r1)+ah2pd(r1)+xh2pd(r3)+ah2pd(r3)
      H(2,2)=0.5d0*H(2,2)+pothhx(r2)+xdiag-1.0d0

      H(3,3)=xh2pd(r1)+ah2pd(r1)+xh2pd(r2)+ah2pd(r2)
      H(3,3)=0.5d0*H(3,3)+pothhx(r3)+xdiag-1.0d0

      H(1,2)=0.5d0*(xh2pd(r3)-ah2pd(r3)-xodiag**2)
      H(2,1)=H(1,2)

      H(1,3)=0.5d0*(xh2pd(r2)-ah2pd(r2)-xodiag**2)
      H(3,1)=H(1,3)

      H(2,3)=0.5d0*(xh2pd(r1)-ah2pd(r1)-xodiag**2)
      H(3,2)=H(2,3)

      do i=1,3
        do j=1,3
          dim(i,j)=h(i,j)
        enddo
      enddo

      call jacobi2(h,3,3,pot,z,nrot)
      call piksrt(3,pot)
      return
      end
        
      integer function nparam (order)
      implicit none
c     calculates number of parameters up to order "order"
      integer i,j,k,l,order
      nparam=0
      do l=0,order
        do i=0,l
          do j=0,(l-i),2
            k=l-i-j
            if(mod(k,3).eq.0) then
              nparam=nparam+1
            endif
            end do
          end do
        end do
        return
      end
      


      FUNCTION POTHHX(R)
C===============================================================
C  NEWEST FIT FOR H2 GROUND STATE POTENTIAL CURVE ##2006-06-22##
C  USING AB INITIO POINTS FROM:
C  L. WOLNIEWICZ JCP 99(3),1851 (1993) ---> FIRST POINTS
C  L. WOLNIEWICZ JCP 103(5),1792 (1995) ---> CORRECTIONS    
C  RANGE of R: 0.6 to 12 a0
C
C  Initial fit  
C  RMS(m= 52 )= 35.2987773353410503  cm-1
C  Final fit
C  RMS(m= 52 )= 0.967169906620670844E-01  cm-1
C===============================================================
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION DD(20)
      COMMON/POTEN/D1(20),D2(20),C(20),GAMMA,AGP,AI(9),R0,RM
      COMMON/LIM/NLOW,NUPP
      COMMON/TH/G0,G1,G2
      COMMON/EXPOE/RE,IEXP
C      COMMON/OUT/VHF,ASEXC,DISP,DD(20)
      COMMON/ASYEXC/CATILD,ATILD(2),ALPHT,GTILD
      NLOW=6
      NUPP=16
      DO II=NLOW,NUPP
        D1(II)=AN(II)
        D2(II)=BN(II)
      ENDDO
      CATILD=-0.8205D0
      ATILD(1)=0.0D0
      ATILD(2)=0.0D0
      ALPHT=2.5D0
      GTILD=2.0D0
      IEXP=1
      RE=0.14010000D+01
      C(6)=0.64990000D+01
      C(7)=0.0D0
      C(8)=0.12440000D+03
      C(9)=0.0D0
      C(10)=0.32858000D+04
      C(11)=-0.34750000D+04
      C(12)=0.12150000D+06
      C(13)=-0.29140000D+06
      C(14)=0.60610000D+07
      C(15)=-0.23050000D+08
      C(16)=0.39380000D+09
      GAMMA=2.5D0
      AGP=0.229794389784158d0
      AI(1)=1.74651398886700093d0
      AI(2)=0.631036031819560028d0
      AI(3)=0.747363488024733624d0
      AI(4)=0.956724297662875783E-01
      AI(5)=0.131320504483065703d0
      AI(6)=-0.812200084994067194E-07
      AI(7)=0.119803887928360935E-01
      AI(8)=-0.212584227748381302E-02
      AI(9)=0.509125901134908042E-03
      G0=1.02072511539524680d0
      G1=1.82599688484061118d0
      G2=0.269916332495592104d0
      R0=0.69282032D+01
      RM=0.11000000D+02
      X=R-RE

       pol1=ai(9)
       do i=8,1,-1              
         pol1=pol1*x + ai(i)
       end do
       pol1=pol1*x + 1.0d0 

      GAM=G0*(1.0D0+G1*TANH(G2*X))
      VHF=-AGP/(R**IEXP)*POL1*EXP(-GAM*X)
    
      ASEXC=1.0D0
      DO I=1,2
        ASEXC=ASEXC+ATILD(I)*R**I
      ENDDO
      ASEXC=ASEXC*CATILD*R**ALPHT*EXP(-GTILD*R)
    
      RHH=0.5D0*(RM+GAMMA*R0)
      X=R/RHH
      DEXC=(1.0D0-EXP(-D1(NLOW)*X-D2(NLOW)*X**2))**NLOW
    
      ASEXC=ASEXC*DEXC
      VHF=VHF+ASEXC
    
      DISP=0.0D0
      DO 1 I=NLOW,NUPP
        DAMPI=(1.0D0-EXP(-D1(I)*X-D2(I)*X**2))**I
        DD(I)=DAMPI
        DISP=DISP-C(I)*DAMPI*R**(-I)
 1    CONTINUE
      POTHHX=VHF+DISP
      RETURN
      END


      FUNCTION AN(N)
      IMPLICIT REAL*8(A-H,O-Z)
      ALPH0=16.36606D0
      ALPH1=0.70172D0
      AN=ALPH0/(FLOAT(N))**ALPH1
      RETURN
      END

      FUNCTION BN(N)
      IMPLICIT REAL*8(A-H,O-Z)
      BET0=17.19338D0
      BET1=0.09574D0
      BN=BET0*EXP(-BET1*FLOAT(N))
      RETURN
      END
           
      real*8 function ah2pd(r)
C===============================================================
C POTENTIAL CURVE FOR H2+ ( A 2^sigma^(+)_(u) )
C ## 2006-06-22 ##
C USING AB INITIO POINTS FROM:
C J. M. PEEK, JCP 43(9), 3004 (1965)
C RANGE of R: 3.5 to 15 a0 
C
C  Final fit  
C  RMS(m= 24 )= 0.125196743261995869  cm-1
C===============================================================
      implicit none
      integer i
      real*8 r,coef(0:7),xh2pd,v
      coef( 0 )=  1.11773285795729826d0
      coef( 1 )= -1.27592697554394174d0
      coef( 2 )=  0.235612064424508216d0
      coef( 3 )= -0.500203729467869895E-01
      coef( 4 )=  0.568627052480373801E-02
      coef( 5 )= -0.382978465312642114E-03
      coef( 6 )=  0.149149267032670154E-04
      coef( 7 )= -0.267518847221239873E-06
c
      v=coef(7)
      do i=6,0,-1
        v=v*r+coef(i)
      enddo
      ah2pd=dexp(v)+xh2pd(r)
      return
      end



      function XH2PD(R)
C===============================================================
C EHFACE2U POTENTIAL CURVE FOR H2+ ( X 2^sigma^(+)_(g) )
C ## 2006-06-22 ##
C USING AB INITIO POINTS FROM:
C D. M. BISHOP AND R. W. WETMORE, MOL. PHYS. 26(1),145 (1972)
C RANGE of R: 0.6 to 10 a0
C
C  Initial fit  
C  RMS(m= 95 )= 67.6787429191265630  cm-1
C  Final fit  
C  RMS(m= 95 )= 0.675527097585787786E-02  cm-1
C===============================================================     
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION DD(20)
      COMMON/POTEN1/D1(20),D2(20),C(20),GAMMA,AGP,AI(11),R0,RM
      COMMON/LIM1/NLOW,NUPP
      COMMON/TH1/G0,G1,G2
      COMMON/EXPOE1/RE,IEXP
      COMMON/ASYEXC1/CATILD,ATILD(2),ALPHT,GTILD
      NLOW=4
      NUPP=11
      DO II=NLOW,NUPP
        D1(II)=AN(II)
        D2(II)=BN(II)
      ENDDO
      IEXP=1
      RE=2.0D0
      C(4)=2.250d0
      C(5)=0.0d0
      C(6)=7.5000d0
      C(7)=53.25d0
      C(8)=65.625d0
      C(9)=886.5d0
      C(10)=1063.125d0
      C(11)=21217.5d0
      GAMMA=2.5D0
      AGP=-0.180395506614312d0
      AI(1)=-0.897676265677028185d0
      AI(2)=-0.771599228853606545d0
      AI(3)=-0.245766669963638718d0
      AI(4)=-0.788889284685244524E-01
      AI(5)=-0.252032558464952844E-01
      AI(6)=0.681894227468654839E-02
      AI(7)=0.655940943163255976E-03
      AI(8)=-0.531288172311992135E-03
      AI(9)=0.890418306898401330E-04
      AI(10)=-0.666314834544138477E-05
      AI(11)=0.194182431833699709E-06
      G0=0.960151039243191562d0
      G1=-0.353946173857859037d0
      G2=-0.496213155382123572d0
      R0=3.4641d0
      RM=0.11000000D+02
      X=R-RE

      pol1=ai(11)
      do i=10,1,-1
         pol1=pol1*x + ai(i)
      end do
      pol1=pol1*x + 1.0d0

      GAM=G0*(1.0D0+G1*TANH(G2*X))
      VHF=-AGP/(R**IEXP)*POL1*EXP(-GAM*X)
C
      RHH=0.5D0*(RM+GAMMA*R0)
      X=R/RHH    
C
      DISP=0.0D0
      DO 1 I=NLOW,NUPP
      DAMPI=(1.0D0-EXP(-D1(I)*X-D2(I)*X**2))**I
      DD(I)=DAMPI
      DISP=DISP-C(I)*DAMPI*R**(-I)
    1 CONTINUE
      XH2PD=VHF+DISP
      RETURN
      END


      SUBROUTINE jacobi2(a,n,np,d,v,nrot)
      INTEGER n,np,nrot,NMAX
      DOUBLE PRECISION a(np,np),d(np),v(np,np)
      PARAMETER (NMAX=500)
      INTEGER i,ip,iq,j
      DOUBLE PRECISION c,g,h,s,sm,t,tau,theta,tresh,b(NMAX),z(NMAX)
      do 12 ip=1,n
        do 11 iq=1,n
          v(ip,iq)=0.d0
11      continue
        v(ip,ip)=1.d0
12    continue
      do 13 ip=1,n
        b(ip)=a(ip,ip)
        d(ip)=b(ip)
        z(ip)=0.d0
13    continue
      nrot=0
      do 24 i=1,50
        sm=0.d0
        do 15 ip=1,n-1
          do 14 iq=ip+1,n
            sm=sm+abs(a(ip,iq))
14        continue
15      continue
        if(sm.eq.0.d0)return
        if(i.lt.4)then
          tresh=0.2d0*sm/n**2
        else
          tresh=0.d0
        endif
        do 22 ip=1,n-1
          do 21 iq=ip+1,n
            g=100.d0*abs(a(ip,iq))
            if((i.gt.4).and.(abs(d(ip))+
     *g.eq.abs(d(ip))).and.(abs(d(iq))+g.eq.abs(d(iq))))then
              a(ip,iq)=0.d0
            else if(abs(a(ip,iq)).gt.tresh)then
              h=d(iq)-d(ip)
              if(abs(h)+g.eq.abs(h))then
                t=a(ip,iq)/h
              else
                theta=0.5d0*h/a(ip,iq)
                t=1.d0/(abs(theta)+sqrt(1.d0+theta**2))
                if(theta.lt.0.d0)t=-t
              endif
              c=1.d0/sqrt(1+t**2)
              s=t*c
              tau=s/(1.d0+c)
              h=t*a(ip,iq)
              z(ip)=z(ip)-h
              z(iq)=z(iq)+h
              d(ip)=d(ip)-h
              d(iq)=d(iq)+h
              a(ip,iq)=0.d0
              do 16 j=1,ip-1
                g=a(j,ip)
                h=a(j,iq)
                a(j,ip)=g-s*(h+g*tau)
                a(j,iq)=h+s*(g-h*tau)
16            continue
              do 17 j=ip+1,iq-1
                g=a(ip,j)
                h=a(j,iq)
                a(ip,j)=g-s*(h+g*tau)
                a(j,iq)=h+s*(g-h*tau)
17            continue
              do 18 j=iq+1,n
                g=a(ip,j)
                h=a(iq,j)
                a(ip,j)=g-s*(h+g*tau)
                a(iq,j)=h+s*(g-h*tau)
18            continue
              do 19 j=1,n
                g=v(j,ip)
                h=v(j,iq)
                v(j,ip)=g-s*(h+g*tau)
                v(j,iq)=h+s*(g-h*tau)
19            continue
              nrot=nrot+1
            endif
21        continue
22      continue
        do 23 ip=1,n
          b(ip)=b(ip)+z(ip)
          d(ip)=b(ip)
          z(ip)=0.d0
23      continue
24    continue
      stop 'too many iterations in jacobi'    ! AA
c      pause 'too many iterations in jacobi'   ! original
c      return                                  ! original
      END
C  (C) Copr. 1986-92 Numerical Recipes Software <%8=$j,)]#1_%.


      SUBROUTINE piksrt(n,arr)
      INTEGER n
      DOUBLE PRECISION arr(n)
      INTEGER i,j
      DOUBLE PRECISION a
      do 12 j=2,n
        a=arr(j)
        do 11 i=j-1,1,-1
          if(arr(i).le.a)goto 10
          arr(i+1)=arr(i)
11      continue
        i=0
10      arr(i+1)=a
12    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software <%8=$j,)]#1_%.

************************************************************************
*                                                                      *
*     H3+ PES Fit to 7610 ab initio aug-cc-pV6Z FCI points             *
*     Please cite:                                                     *
*     L. Velilla, B. Lepetit, A. Aguado, J. Alberto Beswick, and       *
*     M. Paniagua, Journal of Chemical Physics XXX, XXXX (2008)        *
*                                                                      *
************************************************************************
*
* Special Instructions:   
* The fortran code uses the dsyev.f diagonalization 
* subroutine of the LAPACK library
*
************************************************************************
*                                                                      *
*     H3+ DIM plus three body terms for ground state                   *
*     In this version the long-range electrostatic interactions        *
*     of H2 -- H^+ and H2+ -- H are included analytically              *
*     r1,r2,r3  Distances in a.u.                                      *
*     e1,e2,e3  Energies in a.u.                                       *
*                                                                      *
************************************************************************
      subroutine DIMH3P(r1,r2,r3,e1,e2,e3)
      implicit real*8 (a-h,o-z)
      parameter (ndim=3,nwork=ndim*(3+ndim/2))
      dimension en2(ndim),iord(ndim)
      dimension Bdiagon(ndim,ndim),ediagon(ndim)
      dimension work(nwork)
C KES added start
      integer info

      info=0
C KES added end
      h2mgr1=h2masg(r1)
      h2mgr2=h2masg(r2)
      h2mgr3=h2masg(r3)
      h2mur1=h2masu(r1)
      h2mur2=h2masu(r2)
      h2mur3=h2masu(r3)
C     Hii
      aux = .5d0*(h2mgr2+h2mur2+h2mgr3+h2mur3)
      h11= h2(r1)+aux 
      aux = .5d0*(h2mgr1+h2mur1+h2mgr3+h2mur3)
      h22= h2(r2)+aux 
      aux = .5d0*(h2mgr1+h2mur1+h2mgr2+h2mur2)
      h33= h2(r3)+aux 
C     Long range three body terms:
C     Jacobi coordinates in the three DIM wavefunctions
      RLIM=20.d0
C     h11 term 
      Rg=dsqrt(dabs(0.50d0*r2**2+0.50d0*r3**2-0.25d0*r1**2))
      Rg1=Rg
      xcos1=(r2**2-r3**2)/(2.d0*r1*Rg)
      vlong1=0.d0
C     h22 term 
      Rg=dsqrt(dabs(0.50d0*r1**2+0.50d0*r3**2-0.25d0*r2**2))
      Rg2=Rg
      xcos2=(r1**2-r3**2)/(2.d0*r2*Rg)
      vlong2=0.d0
C     h33 term 
      Rg=dsqrt(dabs(0.50d0*r1**2+0.50d0*r2**2-0.25d0*r3**2))
      Rg3=Rg
      xcos3=(r1**2-r2**2)/(2.d0*r3*Rg)
      vlong3=0.d0
CC    Long range H2 -- H+ terms
      if(Rg1.ge.Rg2.and.Rg1.ge.Rg3) then
         Rgg=Rg1+RLIM*dexp(-(Rg1-1.4d0))
         vlong1=h2hmlong(r1,Rgg,xcos1)
      elseif(Rg2.ge.Rg1.and.Rg2.ge.Rg3) then
         Rgg=Rg2+RLIM*dexp(-(Rg2-1.4d0))
         vlong2=h2hmlong(r2,Rgg,xcos2)
      elseif(Rg3.ge.Rg1.and.Rg3.ge.Rg2) then
         Rgg=Rg3+RLIM*dexp(-(Rg3-1.4d0))
         vlong3=h2hmlong(r3,Rgg,xcos3)
      endif
C     h11 term 
      h11=h11+vlong1
C     h22 term 
      h22=h22+vlong2
C     h33 term 
      h33=h33+vlong3
CC    Long range H2+ -- H terms
      a1=4.5d0
      a2=15.d0
      vlim1=0.d0
      vlim2=0.d0
      vlim3=0.d0
      if(Rg1.ge.Rg2.and.Rg1.ge.Rg3) then
         Rgg=Rg1+RLIM*dexp(-(Rg1-1.4d0))
         vlim1=-a1/Rgg**4-a2/Rgg**6
      elseif(Rg2.ge.Rg1.and.Rg2.ge.Rg3) then
         Rgg=Rg2+RLIM*dexp(-(Rg2-1.4d0))
         vlim2=-a1/Rgg**4-a2/Rgg**6
      elseif(Rg3.ge.Rg1.and.Rg3.ge.Rg2) then
         Rgg=Rg3+RLIM*dexp(-(Rg3-1.4d0))
         vlim3=-a1/Rgg**4-a2/Rgg**6
      endif
C
      h11=h11-vlim1
      h22=h22-vlim2
      h33=h33-vlim3
C     Hij
      h12 = .5d0*(h2mgr3-h2mur3)
      h13 = .5d0*(h2mgr2-h2mur2)
      h23 = .5d0*(h2mgr1-h2mur1)

      traza1=h11+h22+h33
      Bdiagon(1,1)=h11
      Bdiagon(2,2)=h22
      Bdiagon(3,3)=h33

      Bdiagon(1,2)=h12
      Bdiagon(1,3)=h13
      Bdiagon(2,3)=h23

      Bdiagon(2,1)=h12
      Bdiagon(3,1)=h13
      Bdiagon(3,2)=h23
      call dsyev('V','U',ndim,Bdiagon,ndim,ediagon,work,nwork,info)
      if (info.ne.0) then
         write (6,'('' Error in dsyev '',i10)') info
      stop
      endif
      do i=1,ndim
         iord(i)=i  
      enddo
 1234 continue
        nchan=0
        do i=1,ndim-1
           j=iord(i)
           j1=iord(i+1)
           if(ediagon(j).gt.ediagon(j1))then
              iord(i)=j1
              iord(i+1)=j
              nchan=nchan+1
           endif
        enddo
      if(nchan.gt.0)go to 1234

      en2(1)=ediagon(iord(1))
      en2(2)=ediagon(iord(2))
      en2(3)=ediagon(iord(3))

      traza2=en2(1)+en2(2)+en2(3)
      if(dabs(traza1-traza2).ge.1.d-6)then
        print*,' Warning in dsyev.'
        print*,' The matrix trace is not the same after diagonalization'
        print*,' at the point: ',r1,r2,r3
      endif
      ener=0.d0
      call H3P12(r1,r2,r3,ener)
      e1=en2(1) + ener
      e2=en2(2)
      e3=en2(3)
c     print*,r1,r2,r3,e1,e2,e3
      return
      end

************************************************************************
*                                                                      *
*     H2 (X1Sigma_g) + H+                                              *
*                                                                      *
*     aV6Z basis set                                                   *
*                                                                      *
************************************************************************
      function h2(X)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CF(  1: 24)
      DATA CF(  1)/0.108019307865263796D+01/
      DATA CF(  2)/0.885267132149937322D-03/
      DATA CF(  3)/-.150815862455622664D+01/
      DATA CF(  4)/-.793005902617790339D+02/
      DATA CF(  5)/0.105210297438485791D+04/
      DATA CF(  6)/-.357143673525664204D+04/
      DATA CF(  7)/-.368651364948682676D+06/
      DATA CF(  8)/0.164539693651234294D+08/
      DATA CF(  9)/-.412026161818069241D+09/
      DATA CF( 10)/0.724072715247103767D+10/
      DATA CF( 11)/-.959109981026094166D+11/
      DATA CF( 12)/0.989254462141214596D+12/
      DATA CF( 13)/-.807959988585112696D+13/
      DATA CF( 14)/0.527216468466357482D+14/
      DATA CF( 15)/-.275960789564009324D+15/
      DATA CF( 16)/0.115877241549258731D+16/
      DATA CF( 17)/-.388851291068040721D+16/
      DATA CF( 18)/0.103418212958519838D+17/
      DATA CF( 19)/-.214936518830420785D+17/
      DATA CF( 20)/0.341332510784677854D+17/
      DATA CF( 21)/-.399645002610553535D+17/
      DATA CF( 22)/0.324886197308079159D+17/
      DATA CF( 23)/-.163660520444291559D+17/
      DATA CF( 24)/0.384535813976511595D+16/
      ENER = 0.d0
      ENER=ENER+CF(  1)*DEXP(-0.236747300313316874D+01*X)/X
      DO 1 I=  2, 24
      ENER=ENER+CF(I)*X**(I-1)*DEXP(-0.106794010126025855D+01*X*(I-1))
    1 CONTINUE
      elong=elargo(x)
      ener=ener+elong
      h2=ENER
      RETURN
      END

      function elargo(x)
C     Long range H2 terms
      implicit real*8 (a-h,o-z)
      ef(x)=-6.499027d0*(1.d0+0.002723d0)/x**6-1.0d0
      eg(x)=-124.4d0*(1.d0+0.0038d0)/x**8
      eh(x)=-(1135.d0+2150.d0)*(1.d0+0.0049d0)/x**10
      ei(x)=-3986.d0*(1.d0+0.0054d0)/x**11
      ej(x)=-0.818d0*x**2.5d0*exp(-2.d0*x)
      elong(x)=ef(x)+eg(x)+eh(x)+ei(x)+ej(x)
C     New smoothing function
      elargo=elong(x+10.d0*dexp(-(x-1.4d0)))
      return
      end

************************************************************************
*                                                                      *
*     H2+ (X2Sigma_g+) + H                                             *
*                                                                      *
*     aV6Z basis set                                                   *
*                                                                      *
************************************************************************
      function h2masg(X)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CF(  1: 24)
      DATA CF(  1)/0.101890674541634368D+01/
      DATA CF(  2)/-.733533393194135500D+00/
      DATA CF(  3)/0.467069677723301975D+01/
      DATA CF(  4)/-.763480845016540428D+02/
      DATA CF(  5)/0.102025122621604111D+04/
      DATA CF(  6)/-.109622701633704962D+05/
      DATA CF(  7)/0.101809575753566751D+06/
      DATA CF(  8)/-.132484386487018557D+07/
      DATA CF(  9)/0.342712779058499769D+08/
      DATA CF( 10)/-.816538384749199313D+09/
      DATA CF( 11)/0.138084650733859766D+11/
      DATA CF( 12)/-.168937495084819981D+12/
      DATA CF( 13)/0.155265237370518038D+13/
      DATA CF( 14)/-.110034981711709734D+14/
      DATA CF( 15)/0.610745433165009416D+14/
      DATA CF( 16)/-.267537941717448694D+15/
      DATA CF( 17)/0.926029021022715639D+15/
      DATA CF( 18)/-.252024462766648397D+16/
      DATA CF( 19)/0.533008082596107679D+16/
      DATA CF( 20)/-.857995868101385281D+16/
      DATA CF( 21)/0.101558177479714493D+17/
      DATA CF( 22)/-.833241746069547676D+16/
      DATA CF( 23)/0.423236266007317995D+16/
      DATA CF( 24)/-.100242878347186489D+16/
      ENER = 0.0d0
      ENER=ENER+CF(  1)*DEXP(-0.164362574634482773D+01*X)/X
      DO 1 I=  2, 24
      ENER=ENER+CF(I)*X**(I-1)*DEXP(-0.996420886779922249D+00*X*(I-1))
    1 CONTINUE
      elong=elargom(x)
      ener=ener+elong
      h2masg = ENER
      RETURN
      END

      function elargom(x)
C     Long range H2+ terms
      implicit real*8 (a-h,o-z)
      elong(x)=-((9.d0/4.d0)/(x**4) + (15.d0/2.d0)/(x**6))
C     New smoothing function
      elargom=elong(x+10.d0*dexp(-(x-2.0d0)))
      return
      end

************************************************************************
*                                                                      *
*     H2+ ( 2Sigma_u+) + H                                             *
*                                                                      *
*     aV6Z basis set                                                   *
*                                                                      *
************************************************************************
      function h2masu(X)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CF(  1: 24)
      DATA CF(  1)/0.137108915731241061D+02/
      DATA CF(  2)/-.493718295281191559D+01/
      DATA CF(  3)/-.692623935598942444D+02/
      DATA CF(  4)/0.267018618909305215D+04/
      DATA CF(  5)/-.116176668163612864D+06/
      DATA CF(  6)/0.381371780628981505D+07/
      DATA CF(  7)/-.925133424514428771D+08/
      DATA CF(  8)/0.132214008993844046D+10/
      DATA CF(  9)/0.713541294481019089D+10/
      DATA CF( 10)/-.105638006330163701D+13/
      DATA CF( 11)/0.346534998935417323D+14/
      DATA CF( 12)/-.732042141163199778D+15/
      DATA CF( 13)/0.113728443003987435D+17/
      DATA CF( 14)/-.135879046313983325D+18/
      DATA CF( 15)/0.127274315931759288D+19/
      DATA CF( 16)/-.941967149040189864D+19/
      DATA CF( 17)/0.551203326946491633D+20/
      DATA CF( 18)/-.253613805480582665D+21/
      DATA CF( 19)/0.906370083707574737D+21/
      DATA CF( 20)/-.246347733315726969D+22/
      DATA CF( 21)/0.491835479035664877D+22/
      DATA CF( 22)/-.679835708957862177D+22/
      DATA CF( 23)/0.581022806005956867D+22/
      DATA CF( 24)/-.231245084520742244D+22/
      ENER = 0.d0
      ENER=ENER+CF(  1)*DEXP(-0.823768724331077401D+00*X)/X
      DO 1 I=  2, 24
      ENER=ENER+CF(I)*X**(I-1)*DEXP(-0.144122652170645560D+01*X*(I-1))
    1 CONTINUE
      elong=elargom(x)
      ener=ener+elong
      h2masu = ENER
      RETURN
      END

************************************************************************
*                                                                      *
*     H2 -- H Long range terms                                         *
*                                                                      *
*     aV6Z basis set                                                   *
*                                                                      *
************************************************************************
      function h2hmlong(rpeq,Rgran,xcos)
      implicit real*8 (a-h,o-z)
      call quadz(rpeq,h2quad)
      pol2=1.5d0*xcos*xcos-0.5d0
C     aV6Z quadrupole moment
      vqz=h2quad*pol2/rgran**3

C     aV6Z polarizabilities
      call polx(rpeq,axx) !! aV6Z
      call polz(rpeq,azz) !! aV6Z
      a0aV6Z=(azz+2.d0*axx)/3.d0
      a0aV6Z=a0aV6Z/2.d0
      a2aV6Z=(azz-axx)*2.d0/3.d0
      a2aV6Z=a2aV6Z/2.d0
      vpol=-(a0aV6Z+a2aV6Z*pol2)/Rgran**4
      h2hmlong=vqz+vpol
      return
      end
C     aV6Z quadrupole moment fit
      subroutine quadz(x,y)
      implicit real*8 (a-h,o-z)
      dimension cf( 15)
      dimension vex(  2)
      data cf(  1)/0.8166648734890371D-04/
      data cf(  2)/-.1881855960082434D+01/
      data cf(  3)/0.2421277875344904D+00/
      data cf(  4)/0.6085863990932506D+02/
      data cf(  5)/-.2757874922103535D+03/
      data cf(  6)/0.1512576449669221D+04/
      data cf(  7)/-.1090442521871086D+05/
      data cf(  8)/0.1905266714338982D+05/
      data cf(  9)/0.1730115053828064D+06/
      data cf( 10)/-.1000012086016814D+07/
      data cf( 11)/0.1762147199545549D+07/
      data cf( 12)/0.9342476802465859D+06/
      data cf( 13)/-.8105382035645692D+07/
      data cf( 14)/0.1151121162307790D+08/
      data cf( 15)/-.5602134540904779D+07/
      data vex(  1)/0.1340130881463356D+01/
      data vex(  2)/0.8322962713014708D+00/
      y=0.d0
      do 1 i=  1, 15
         y = y + cf(i)*funcion(vex,i,x)
    1 continue
      return
      end
C     aV6Z polarizabilities fits
      subroutine polx(x,y)
      implicit real*8 (a-h,o-z)
      dimension cf( 15)
      dimension vex(  2)
      data cf(  1)/0.8974583249592978D+01/
      data cf(  2)/-.1290835863481878D+02/
      data cf(  3)/-.5379421065843753D+01/
      data cf(  4)/0.1765706276501767D+03/
      data cf(  5)/-.3385803556921954D+04/
      data cf(  6)/0.4493699029122313D+05/
      data cf(  7)/-.3652369817756390D+06/
      data cf(  8)/0.1852753697133512D+07/
      data cf(  9)/-.5809312606038393D+07/
      data cf( 10)/0.9721353024158247D+07/
      data cf( 11)/-.7092806937216849D+06/
      data cf( 12)/-.3327729880967365D+08/
      data cf( 13)/0.6983684025829618D+08/
      data cf( 14)/-.6381343977493817D+08/
      data cf( 15)/0.2300922761580030D+08/
      data vex(  1)/0.1187879729111910D+01/
      data vex(  2)/0.7554684825669591D+00/
      y=0.d0
      do 1 i=  1, 15
         y = y + cf(i)*funcion(vex,i,x)
    1 continue
      return
      end
      subroutine polz(x,y)
      implicit real*8 (a-h,o-z)
      dimension cf( 15)
      dimension vex(  2)
      data cf(  1)/0.9005313464224852D+01/
      data cf(  2)/-.4087039493711530D+02/
      data cf(  3)/0.1169970996784607D+02/
      data cf(  4)/-.2570966837001311D+03/
      data cf(  5)/0.1115822309638538D+05/
      data cf(  6)/-.1611895201468293D+06/
      data cf(  7)/0.1476263886597057D+07/
      data cf(  8)/-.8725387879754866D+07/
      data cf(  9)/0.3255107028148777D+08/
      data cf( 10)/-.7292647566134660D+08/
      data cf( 11)/0.7756856658321184D+08/
      data cf( 12)/0.3625591185348335D+08/
      data cf( 13)/-.2179510419880131D+09/
      data cf( 14)/0.2531262189573988D+09/
      data cf( 15)/-.1047041830161959D+09/
      data vex(  1)/0.1826054733720348D+01/
      data vex(  2)/0.7429159069146456D+00/
      y=0.d0
      do 1 i=  1, 15
         y = y + cf(i)*funcion(vex,i,x)
    1 continue
      return
      end
****************************************************************
*
*     Fitted functions
*     In vex(1:nlinear) are the non-linear parameters
*     In sm(1:mt) are the linear parameters
*
****************************************************************
      function funcion(vex,k,x)
      implicit real*8 (a-h,o-z)
      parameter (nexp=100)
      dimension vex(nexp)

      if(k.eq.1) funcion = 1.d0
      if(k.eq.2) funcion = dexp(-vex(1)*x)
      if(k.ge.3) funcion = (x*dexp(-vex(2)*x))**(k-2)
      return
      end

************************************************************************
*                                                                      *
*     Three body terms                                                 *
*     See:                                                             *
*     A. Aguado and M. Paniagua. JCP 96, 1265 (1992)                   *
*     A. Aguado, C. Tablero, and M. Paniagua. CPC 108, 259 (1998)      *
*                                                                      *
************************************************************************
      subroutine H3P12(x,y,z,ener)
      implicit  real * 8 (a-h,o-z)
      dimension i( 836), j( 836), k( 836), cf( 836)
      dimension f12(0:  11),f13(0:  11),f23(0:  11)
      dimension g12(0:  11),g13(0:  11),g23(0:  11)
      data cf(   1)/-.63104793828257965071577103646761D+00/
      data i(   1)/  1/,j(   1)/  1/,k(   1)/  0/
      data cf(   2)/-.63104793828257965071577103646761D+00/
      data i(   2)/  1/,j(   2)/  0/,k(   2)/  1/
      data cf(   3)/-.63104793828257965071577103646761D+00/
      data i(   3)/  0/,j(   3)/  1/,k(   3)/  1/
      data cf(   4)/-.22482574666967662062284211058154D+02/
      data i(   4)/  1/,j(   4)/  1/,k(   4)/  1/
      data cf(   5)/0.17956956929044981961027969046102D+02/
      data i(   5)/  2/,j(   5)/  1/,k(   5)/  0/
      data cf(   6)/0.17956956929044981961027969046102D+02/
      data i(   6)/  2/,j(   6)/  0/,k(   6)/  1/
      data cf(   7)/0.17956956929044981961027969046102D+02/
      data i(   7)/  1/,j(   7)/  2/,k(   7)/  0/
      data cf(   8)/0.17956956929044981961027969046102D+02/
      data i(   8)/  1/,j(   8)/  0/,k(   8)/  2/
      data cf(   9)/0.17956956929044981961027969046102D+02/
      data i(   9)/  0/,j(   9)/  2/,k(   9)/  1/
      data cf(  10)/0.17956956929044981961027969046102D+02/
      data i(  10)/  0/,j(  10)/  1/,k(  10)/  2/
      data cf(  11)/0.40512899807664621695693369254202D+03/
      data i(  11)/  2/,j(  11)/  1/,k(  11)/  1/
      data cf(  12)/0.40512899807664621695693369254202D+03/
      data i(  12)/  1/,j(  12)/  2/,k(  12)/  1/
      data cf(  13)/0.40512899807664621695693369254202D+03/
      data i(  13)/  1/,j(  13)/  1/,k(  13)/  2/
      data cf(  14)/-.27479824007591655092546826200749D+03/
      data i(  14)/  2/,j(  14)/  2/,k(  14)/  0/
      data cf(  15)/-.27479824007591655092546826200749D+03/
      data i(  15)/  2/,j(  15)/  0/,k(  15)/  2/
      data cf(  16)/-.27479824007591655092546826200749D+03/
      data i(  16)/  0/,j(  16)/  2/,k(  16)/  2/
      data cf(  17)/-.22405204878223057592568350558109D+03/
      data i(  17)/  3/,j(  17)/  1/,k(  17)/  0/
      data cf(  18)/-.22405204878223057592568350558109D+03/
      data i(  18)/  3/,j(  18)/  0/,k(  18)/  1/
      data cf(  19)/-.22405204878223057592568350558109D+03/
      data i(  19)/  1/,j(  19)/  3/,k(  19)/  0/
      data cf(  20)/-.22405204878223057592568350558109D+03/
      data i(  20)/  1/,j(  20)/  0/,k(  20)/  3/
      data cf(  21)/-.22405204878223057592568350558109D+03/
      data i(  21)/  0/,j(  21)/  3/,k(  21)/  1/
      data cf(  22)/-.22405204878223057592568350558109D+03/
      data i(  22)/  0/,j(  22)/  1/,k(  22)/  3/
      data cf(  23)/-.12340018448082090604996618786125D+04/
      data i(  23)/  2/,j(  23)/  2/,k(  23)/  1/
      data cf(  24)/-.12340018448082090604996618786125D+04/
      data i(  24)/  2/,j(  24)/  1/,k(  24)/  2/
      data cf(  25)/-.12340018448082090604996618786125D+04/
      data i(  25)/  1/,j(  25)/  2/,k(  25)/  2/
      data cf(  26)/-.64854146453357408448505601300150D+04/
      data i(  26)/  3/,j(  26)/  1/,k(  26)/  1/
      data cf(  27)/-.64854146453357408448505601300150D+04/
      data i(  27)/  1/,j(  27)/  3/,k(  27)/  1/
      data cf(  28)/-.64854146453357408448505601300150D+04/
      data i(  28)/  1/,j(  28)/  1/,k(  28)/  3/
      data cf(  29)/0.68491013275559507606161134064425D+04/
      data i(  29)/  3/,j(  29)/  2/,k(  29)/  0/
      data cf(  30)/0.68491013275559507606161134064425D+04/
      data i(  30)/  3/,j(  30)/  0/,k(  30)/  2/
      data cf(  31)/0.68491013275559507606161134064425D+04/
      data i(  31)/  2/,j(  31)/  3/,k(  31)/  0/
      data cf(  32)/0.68491013275559507606161134064425D+04/
      data i(  32)/  2/,j(  32)/  0/,k(  32)/  3/
      data cf(  33)/0.68491013275559507606161134064425D+04/
      data i(  33)/  0/,j(  33)/  3/,k(  33)/  2/
      data cf(  34)/0.68491013275559507606161134064425D+04/
      data i(  34)/  0/,j(  34)/  2/,k(  34)/  3/
      data cf(  35)/0.27235640677554075007480048659808D+04/
      data i(  35)/  4/,j(  35)/  1/,k(  35)/  0/
      data cf(  36)/0.27235640677554075007480048659808D+04/
      data i(  36)/  4/,j(  36)/  0/,k(  36)/  1/
      data cf(  37)/0.27235640677554075007480048659808D+04/
      data i(  37)/  1/,j(  37)/  4/,k(  37)/  0/
      data cf(  38)/0.27235640677554075007480048659808D+04/
      data i(  38)/  1/,j(  38)/  0/,k(  38)/  4/
      data cf(  39)/0.27235640677554075007480048659808D+04/
      data i(  39)/  0/,j(  39)/  4/,k(  39)/  1/
      data cf(  40)/0.27235640677554075007480048659808D+04/
      data i(  40)/  0/,j(  40)/  1/,k(  40)/  4/
      data cf(  41)/0.30567696859479218427703492077954D+05/
      data i(  41)/  2/,j(  41)/  2/,k(  41)/  2/
      data cf(  42)/0.92375059169535807039532131752390D+04/
      data i(  42)/  3/,j(  42)/  2/,k(  42)/  1/
      data cf(  43)/0.92375059169535807039532131752390D+04/
      data i(  43)/  3/,j(  43)/  1/,k(  43)/  2/
      data cf(  44)/0.92375059169535807039532131752390D+04/
      data i(  44)/  2/,j(  44)/  3/,k(  44)/  1/
      data cf(  45)/0.92375059169535807039532131752390D+04/
      data i(  45)/  2/,j(  45)/  1/,k(  45)/  3/
      data cf(  46)/0.92375059169535807039532131752390D+04/
      data i(  46)/  1/,j(  46)/  3/,k(  46)/  2/
      data cf(  47)/0.92375059169535807039532131752390D+04/
      data i(  47)/  1/,j(  47)/  2/,k(  47)/  3/
      data cf(  48)/-.74551824412548897852950708621481D+05/
      data i(  48)/  3/,j(  48)/  3/,k(  48)/  0/
      data cf(  49)/-.74551824412548897852950708621481D+05/
      data i(  49)/  3/,j(  49)/  0/,k(  49)/  3/
      data cf(  50)/-.74551824412548897852950708621481D+05/
      data i(  50)/  0/,j(  50)/  3/,k(  50)/  3/
      data cf(  51)/0.63222244179959883416530226159837D+05/
      data i(  51)/  4/,j(  51)/  1/,k(  51)/  1/
      data cf(  52)/0.63222244179959883416530226159837D+05/
      data i(  52)/  1/,j(  52)/  4/,k(  52)/  1/
      data cf(  53)/0.63222244179959883416530226159837D+05/
      data i(  53)/  1/,j(  53)/  1/,k(  53)/  4/
      data cf(  54)/-.62267164653611312164245670941721D+05/
      data i(  54)/  4/,j(  54)/  2/,k(  54)/  0/
      data cf(  55)/-.62267164653611312164245670941721D+05/
      data i(  55)/  4/,j(  55)/  0/,k(  55)/  2/
      data cf(  56)/-.62267164653611312164245670941721D+05/
      data i(  56)/  2/,j(  56)/  4/,k(  56)/  0/
      data cf(  57)/-.62267164653611312164245670941721D+05/
      data i(  57)/  2/,j(  57)/  0/,k(  57)/  4/
      data cf(  58)/-.62267164653611312164245670941721D+05/
      data i(  58)/  0/,j(  58)/  4/,k(  58)/  2/
      data cf(  59)/-.62267164653611312164245670941721D+05/
      data i(  59)/  0/,j(  59)/  2/,k(  59)/  4/
      data cf(  60)/-.27273902688470212803630815596029D+05/
      data i(  60)/  5/,j(  60)/  1/,k(  60)/  0/
      data cf(  61)/-.27273902688470212803630815596029D+05/
      data i(  61)/  5/,j(  61)/  0/,k(  61)/  1/
      data cf(  62)/-.27273902688470212803630815596029D+05/
      data i(  62)/  1/,j(  62)/  5/,k(  62)/  0/
      data cf(  63)/-.27273902688470212803630815596029D+05/
      data i(  63)/  1/,j(  63)/  0/,k(  63)/  5/
      data cf(  64)/-.27273902688470212803630815596029D+05/
      data i(  64)/  0/,j(  64)/  5/,k(  64)/  1/
      data cf(  65)/-.27273902688470212803630815596029D+05/
      data i(  65)/  0/,j(  65)/  1/,k(  65)/  5/
      data cf(  66)/-.50120717279831024863361464870147D+05/
      data i(  66)/  3/,j(  66)/  2/,k(  66)/  2/
      data cf(  67)/-.50120717279831024863361464870147D+05/
      data i(  67)/  2/,j(  67)/  3/,k(  67)/  2/
      data cf(  68)/-.50120717279831024863361464870147D+05/
      data i(  68)/  2/,j(  68)/  2/,k(  68)/  3/
      data cf(  69)/0.36465365103056512914733615880218D+05/
      data i(  69)/  3/,j(  69)/  3/,k(  69)/  1/
      data cf(  70)/0.36465365103056512914733615880218D+05/
      data i(  70)/  3/,j(  70)/  1/,k(  70)/  3/
      data cf(  71)/0.36465365103056512914733615880218D+05/
      data i(  71)/  1/,j(  71)/  3/,k(  71)/  3/
      data cf(  72)/-.11126785787131699976427936357722D+06/
      data i(  72)/  4/,j(  72)/  2/,k(  72)/  1/
      data cf(  73)/-.11126785787131699976427936357722D+06/
      data i(  73)/  4/,j(  73)/  1/,k(  73)/  2/
      data cf(  74)/-.11126785787131699976427936357722D+06/
      data i(  74)/  2/,j(  74)/  4/,k(  74)/  1/
      data cf(  75)/-.11126785787131699976427936357722D+06/
      data i(  75)/  2/,j(  75)/  1/,k(  75)/  4/
      data cf(  76)/-.11126785787131699976427936357722D+06/
      data i(  76)/  1/,j(  76)/  4/,k(  76)/  2/
      data cf(  77)/-.11126785787131699976427936357722D+06/
      data i(  77)/  1/,j(  77)/  2/,k(  77)/  4/
      data cf(  78)/0.42434254449085369120078254242766D+06/
      data i(  78)/  4/,j(  78)/  3/,k(  78)/  0/
      data cf(  79)/0.42434254449085369120078254242766D+06/
      data i(  79)/  4/,j(  79)/  0/,k(  79)/  3/
      data cf(  80)/0.42434254449085369120078254242766D+06/
      data i(  80)/  3/,j(  80)/  4/,k(  80)/  0/
      data cf(  81)/0.42434254449085369120078254242766D+06/
      data i(  81)/  3/,j(  81)/  0/,k(  81)/  4/
      data cf(  82)/0.42434254449085369120078254242766D+06/
      data i(  82)/  0/,j(  82)/  4/,k(  82)/  3/
      data cf(  83)/0.42434254449085369120078254242766D+06/
      data i(  83)/  0/,j(  83)/  3/,k(  83)/  4/
      data cf(  84)/-.36594542445531802395037124496261D+06/
      data i(  84)/  5/,j(  84)/  1/,k(  84)/  1/
      data cf(  85)/-.36594542445531802395037124496261D+06/
      data i(  85)/  1/,j(  85)/  5/,k(  85)/  1/
      data cf(  86)/-.36594542445531802395037124496261D+06/
      data i(  86)/  1/,j(  86)/  1/,k(  86)/  5/
      data cf(  87)/0.29576100006699253517214761206037D+06/
      data i(  87)/  5/,j(  87)/  2/,k(  87)/  0/
      data cf(  88)/0.29576100006699253517214761206037D+06/
      data i(  88)/  5/,j(  88)/  0/,k(  88)/  2/
      data cf(  89)/0.29576100006699253517214761206037D+06/
      data i(  89)/  2/,j(  89)/  5/,k(  89)/  0/
      data cf(  90)/0.29576100006699253517214761206037D+06/
      data i(  90)/  2/,j(  90)/  0/,k(  90)/  5/
      data cf(  91)/0.29576100006699253517214761206037D+06/
      data i(  91)/  0/,j(  91)/  5/,k(  91)/  2/
      data cf(  92)/0.29576100006699253517214761206037D+06/
      data i(  92)/  0/,j(  92)/  2/,k(  92)/  5/
      data cf(  93)/0.17675158983289466563765581157228D+06/
      data i(  93)/  6/,j(  93)/  1/,k(  93)/  0/
      data cf(  94)/0.17675158983289466563765581157228D+06/
      data i(  94)/  6/,j(  94)/  0/,k(  94)/  1/
      data cf(  95)/0.17675158983289466563765581157228D+06/
      data i(  95)/  1/,j(  95)/  6/,k(  95)/  0/
      data cf(  96)/0.17675158983289466563765581157228D+06/
      data i(  96)/  1/,j(  96)/  0/,k(  96)/  6/
      data cf(  97)/0.17675158983289466563765581157228D+06/
      data i(  97)/  0/,j(  97)/  6/,k(  97)/  1/
      data cf(  98)/0.17675158983289466563765581157228D+06/
      data i(  98)/  0/,j(  98)/  1/,k(  98)/  6/
      data cf(  99)/0.63710471239613359672307453696188D+05/
      data i(  99)/  3/,j(  99)/  3/,k(  99)/  2/
      data cf( 100)/0.63710471239613359672307453696188D+05/
      data i( 100)/  3/,j( 100)/  2/,k( 100)/  3/
      data cf( 101)/0.63710471239613359672307453696188D+05/
      data i( 101)/  2/,j( 101)/  3/,k( 101)/  3/
      data cf( 102)/0.22561848308893266990156578760259D+06/
      data i( 102)/  4/,j( 102)/  2/,k( 102)/  2/
      data cf( 103)/0.22561848308893266990156578760259D+06/
      data i( 103)/  2/,j( 103)/  4/,k( 103)/  2/
      data cf( 104)/0.22561848308893266990156578760259D+06/
      data i( 104)/  2/,j( 104)/  2/,k( 104)/  4/
      data cf( 105)/-.12160074797649862744818579556370D+06/
      data i( 105)/  4/,j( 105)/  3/,k( 105)/  1/
      data cf( 106)/-.12160074797649862744818579556370D+06/
      data i( 106)/  4/,j( 106)/  1/,k( 106)/  3/
      data cf( 107)/-.12160074797649862744818579556370D+06/
      data i( 107)/  3/,j( 107)/  4/,k( 107)/  1/
      data cf( 108)/-.12160074797649862744818579556370D+06/
      data i( 108)/  3/,j( 108)/  1/,k( 108)/  4/
      data cf( 109)/-.12160074797649862744818579556370D+06/
      data i( 109)/  1/,j( 109)/  4/,k( 109)/  3/
      data cf( 110)/-.12160074797649862744818579556370D+06/
      data i( 110)/  1/,j( 110)/  3/,k( 110)/  4/
      data cf( 111)/-.18310291372104683929947265288590D+07/
      data i( 111)/  4/,j( 111)/  4/,k( 111)/  0/
      data cf( 112)/-.18310291372104683929947265288590D+07/
      data i( 112)/  4/,j( 112)/  0/,k( 112)/  4/
      data cf( 113)/-.18310291372104683929947265288590D+07/
      data i( 113)/  0/,j( 113)/  4/,k( 113)/  4/
      data cf( 114)/0.67461255884090270266063803227325D+06/
      data i( 114)/  5/,j( 114)/  2/,k( 114)/  1/
      data cf( 115)/0.67461255884090270266063803227325D+06/
      data i( 115)/  5/,j( 115)/  1/,k( 115)/  2/
      data cf( 116)/0.67461255884090270266063803227325D+06/
      data i( 116)/  2/,j( 116)/  5/,k( 116)/  1/
      data cf( 117)/0.67461255884090270266063803227325D+06/
      data i( 117)/  2/,j( 117)/  1/,k( 117)/  5/
      data cf( 118)/0.67461255884090270266063803227325D+06/
      data i( 118)/  1/,j( 118)/  5/,k( 118)/  2/
      data cf( 119)/0.67461255884090270266063803227325D+06/
      data i( 119)/  1/,j( 119)/  2/,k( 119)/  5/
      data cf( 120)/-.16676290837290931247914756932571D+07/
      data i( 120)/  5/,j( 120)/  3/,k( 120)/  0/
      data cf( 121)/-.16676290837290931247914756932571D+07/
      data i( 121)/  5/,j( 121)/  0/,k( 121)/  3/
      data cf( 122)/-.16676290837290931247914756932571D+07/
      data i( 122)/  3/,j( 122)/  5/,k( 122)/  0/
      data cf( 123)/-.16676290837290931247914756932571D+07/
      data i( 123)/  3/,j( 123)/  0/,k( 123)/  5/
      data cf( 124)/-.16676290837290931247914756932571D+07/
      data i( 124)/  0/,j( 124)/  5/,k( 124)/  3/
      data cf( 125)/-.16676290837290931247914756932571D+07/
      data i( 125)/  0/,j( 125)/  3/,k( 125)/  5/
      data cf( 126)/0.13077339229715985584946764357885D+07/
      data i( 126)/  6/,j( 126)/  1/,k( 126)/  1/
      data cf( 127)/0.13077339229715985584946764357885D+07/
      data i( 127)/  1/,j( 127)/  6/,k( 127)/  1/
      data cf( 128)/0.13077339229715985584946764357885D+07/
      data i( 128)/  1/,j( 128)/  1/,k( 128)/  6/
      data cf( 129)/-.11834110014828085759222610232594D+07/
      data i( 129)/  6/,j( 129)/  2/,k( 129)/  0/
      data cf( 130)/-.11834110014828085759222610232594D+07/
      data i( 130)/  6/,j( 130)/  0/,k( 130)/  2/
      data cf( 131)/-.11834110014828085759222610232594D+07/
      data i( 131)/  2/,j( 131)/  6/,k( 131)/  0/
      data cf( 132)/-.11834110014828085759222610232594D+07/
      data i( 132)/  2/,j( 132)/  0/,k( 132)/  6/
      data cf( 133)/-.11834110014828085759222610232594D+07/
      data i( 133)/  0/,j( 133)/  6/,k( 133)/  2/
      data cf( 134)/-.11834110014828085759222610232594D+07/
      data i( 134)/  0/,j( 134)/  2/,k( 134)/  6/
      data cf( 135)/-.73075349725927687583109159158472D+06/
      data i( 135)/  7/,j( 135)/  1/,k( 135)/  0/
      data cf( 136)/-.73075349725927687583109159158472D+06/
      data i( 136)/  7/,j( 136)/  0/,k( 136)/  1/
      data cf( 137)/-.73075349725927687583109159158472D+06/
      data i( 137)/  1/,j( 137)/  7/,k( 137)/  0/
      data cf( 138)/-.73075349725927687583109159158472D+06/
      data i( 138)/  1/,j( 138)/  0/,k( 138)/  7/
      data cf( 139)/-.73075349725927687583109159158472D+06/
      data i( 139)/  0/,j( 139)/  7/,k( 139)/  1/
      data cf( 140)/-.73075349725927687583109159158472D+06/
      data i( 140)/  0/,j( 140)/  1/,k( 140)/  7/
      data cf( 141)/0.32795985039751821202396594383694D+06/
      data i( 141)/  3/,j( 141)/  3/,k( 141)/  3/
      data cf( 142)/-.18788151209987344723109339694879D+06/
      data i( 142)/  4/,j( 142)/  3/,k( 142)/  2/
      data cf( 143)/-.18788151209987344723109339694879D+06/
      data i( 143)/  4/,j( 143)/  2/,k( 143)/  3/
      data cf( 144)/-.18788151209987344723109339694879D+06/
      data i( 144)/  3/,j( 144)/  4/,k( 144)/  2/
      data cf( 145)/-.18788151209987344723109339694879D+06/
      data i( 145)/  3/,j( 145)/  2/,k( 145)/  4/
      data cf( 146)/-.18788151209987344723109339694879D+06/
      data i( 146)/  2/,j( 146)/  4/,k( 146)/  3/
      data cf( 147)/-.18788151209987344723109339694879D+06/
      data i( 147)/  2/,j( 147)/  3/,k( 147)/  4/
      data cf( 148)/0.10705633692865825924934438519498D+07/
      data i( 148)/  4/,j( 148)/  4/,k( 148)/  1/
      data cf( 149)/0.10705633692865825924934438519498D+07/
      data i( 149)/  4/,j( 149)/  1/,k( 149)/  4/
      data cf( 150)/0.10705633692865825924934438519498D+07/
      data i( 150)/  1/,j( 150)/  4/,k( 150)/  4/
      data cf( 151)/-.16600988246610158818526053843780D+07/
      data i( 151)/  5/,j( 151)/  2/,k( 151)/  2/
      data cf( 152)/-.16600988246610158818526053843780D+07/
      data i( 152)/  2/,j( 152)/  5/,k( 152)/  2/
      data cf( 153)/-.16600988246610158818526053843780D+07/
      data i( 153)/  2/,j( 153)/  2/,k( 153)/  5/
      data cf( 154)/-.37315071413016770453700727232252D+06/
      data i( 154)/  5/,j( 154)/  3/,k( 154)/  1/
      data cf( 155)/-.37315071413016770453700727232252D+06/
      data i( 155)/  5/,j( 155)/  1/,k( 155)/  3/
      data cf( 156)/-.37315071413016770453700727232252D+06/
      data i( 156)/  3/,j( 156)/  5/,k( 156)/  1/
      data cf( 157)/-.37315071413016770453700727232252D+06/
      data i( 157)/  3/,j( 157)/  1/,k( 157)/  5/
      data cf( 158)/-.37315071413016770453700727232252D+06/
      data i( 158)/  1/,j( 158)/  5/,k( 158)/  3/
      data cf( 159)/-.37315071413016770453700727232252D+06/
      data i( 159)/  1/,j( 159)/  3/,k( 159)/  5/
      data cf( 160)/0.51196481550703075231781233493241D+07/
      data i( 160)/  5/,j( 160)/  4/,k( 160)/  0/
      data cf( 161)/0.51196481550703075231781233493241D+07/
      data i( 161)/  5/,j( 161)/  0/,k( 161)/  4/
      data cf( 162)/0.51196481550703075231781233493241D+07/
      data i( 162)/  4/,j( 162)/  5/,k( 162)/  0/
      data cf( 163)/0.51196481550703075231781233493241D+07/
      data i( 163)/  4/,j( 163)/  0/,k( 163)/  5/
      data cf( 164)/0.51196481550703075231781233493241D+07/
      data i( 164)/  0/,j( 164)/  5/,k( 164)/  4/
      data cf( 165)/0.51196481550703075231781233493241D+07/
      data i( 165)/  0/,j( 165)/  4/,k( 165)/  5/
      data cf( 166)/-.18960552551177684950025623353966D+07/
      data i( 166)/  6/,j( 166)/  2/,k( 166)/  1/
      data cf( 167)/-.18960552551177684950025623353966D+07/
      data i( 167)/  6/,j( 167)/  1/,k( 167)/  2/
      data cf( 168)/-.18960552551177684950025623353966D+07/
      data i( 168)/  2/,j( 168)/  6/,k( 168)/  1/
      data cf( 169)/-.18960552551177684950025623353966D+07/
      data i( 169)/  2/,j( 169)/  1/,k( 169)/  6/
      data cf( 170)/-.18960552551177684950025623353966D+07/
      data i( 170)/  1/,j( 170)/  6/,k( 170)/  2/
      data cf( 171)/-.18960552551177684950025623353966D+07/
      data i( 171)/  1/,j( 171)/  2/,k( 171)/  6/
      data cf( 172)/0.50996072814955640924015736979790D+07/
      data i( 172)/  6/,j( 172)/  3/,k( 172)/  0/
      data cf( 173)/0.50996072814955640924015736979790D+07/
      data i( 173)/  6/,j( 173)/  0/,k( 173)/  3/
      data cf( 174)/0.50996072814955640924015736979790D+07/
      data i( 174)/  3/,j( 174)/  6/,k( 174)/  0/
      data cf( 175)/0.50996072814955640924015736979790D+07/
      data i( 175)/  3/,j( 175)/  0/,k( 175)/  6/
      data cf( 176)/0.50996072814955640924015736979790D+07/
      data i( 176)/  0/,j( 176)/  6/,k( 176)/  3/
      data cf( 177)/0.50996072814955640924015736979790D+07/
      data i( 177)/  0/,j( 177)/  3/,k( 177)/  6/
      data cf( 178)/-.30493049318289205811262357392871D+07/
      data i( 178)/  7/,j( 178)/  1/,k( 178)/  1/
      data cf( 179)/-.30493049318289205811262357392871D+07/
      data i( 179)/  1/,j( 179)/  7/,k( 179)/  1/
      data cf( 180)/-.30493049318289205811262357392871D+07/
      data i( 180)/  1/,j( 180)/  1/,k( 180)/  7/
      data cf( 181)/0.39208749564265824332108490713723D+07/
      data i( 181)/  7/,j( 181)/  2/,k( 181)/  0/
      data cf( 182)/0.39208749564265824332108490713723D+07/
      data i( 182)/  7/,j( 182)/  0/,k( 182)/  2/
      data cf( 183)/0.39208749564265824332108490713723D+07/
      data i( 183)/  2/,j( 183)/  7/,k( 183)/  0/
      data cf( 184)/0.39208749564265824332108490713723D+07/
      data i( 184)/  2/,j( 184)/  0/,k( 184)/  7/
      data cf( 185)/0.39208749564265824332108490713723D+07/
      data i( 185)/  0/,j( 185)/  7/,k( 185)/  2/
      data cf( 186)/0.39208749564265824332108490713723D+07/
      data i( 186)/  0/,j( 186)/  2/,k( 186)/  7/
      data cf( 187)/0.19635709164764547090376469924092D+07/
      data i( 187)/  8/,j( 187)/  1/,k( 187)/  0/
      data cf( 188)/0.19635709164764547090376469924092D+07/
      data i( 188)/  8/,j( 188)/  0/,k( 188)/  1/
      data cf( 189)/0.19635709164764547090376469924092D+07/
      data i( 189)/  1/,j( 189)/  8/,k( 189)/  0/
      data cf( 190)/0.19635709164764547090376469924092D+07/
      data i( 190)/  1/,j( 190)/  0/,k( 190)/  8/
      data cf( 191)/0.19635709164764547090376469924092D+07/
      data i( 191)/  0/,j( 191)/  8/,k( 191)/  1/
      data cf( 192)/0.19635709164764547090376469924092D+07/
      data i( 192)/  0/,j( 192)/  1/,k( 192)/  8/
      data cf( 193)/-.10890575144690454553925489122544D+07/
      data i( 193)/  4/,j( 193)/  3/,k( 193)/  3/
      data cf( 194)/-.10890575144690454553925489122544D+07/
      data i( 194)/  3/,j( 194)/  4/,k( 194)/  3/
      data cf( 195)/-.10890575144690454553925489122544D+07/
      data i( 195)/  3/,j( 195)/  3/,k( 195)/  4/
      data cf( 196)/0.27691070129915523794190447471707D+07/
      data i( 196)/  4/,j( 196)/  4/,k( 196)/  2/
      data cf( 197)/0.27691070129915523794190447471707D+07/
      data i( 197)/  4/,j( 197)/  2/,k( 197)/  4/
      data cf( 198)/0.27691070129915523794190447471707D+07/
      data i( 198)/  2/,j( 198)/  4/,k( 198)/  4/
      data cf( 199)/0.81929948197342631768413424136805D+05/
      data i( 199)/  5/,j( 199)/  3/,k( 199)/  2/
      data cf( 200)/0.81929948197342631768413424136805D+05/
      data i( 200)/  5/,j( 200)/  2/,k( 200)/  3/
      data cf( 201)/0.81929948197342631768413424136805D+05/
      data i( 201)/  3/,j( 201)/  5/,k( 201)/  2/
      data cf( 202)/0.81929948197342631768413424136805D+05/
      data i( 202)/  3/,j( 202)/  2/,k( 202)/  5/
      data cf( 203)/0.81929948197342631768413424136805D+05/
      data i( 203)/  2/,j( 203)/  5/,k( 203)/  3/
      data cf( 204)/0.81929948197342631768413424136805D+05/
      data i( 204)/  2/,j( 204)/  3/,k( 204)/  5/
      data cf( 205)/-.20874734498708296871456137622246D+07/
      data i( 205)/  5/,j( 205)/  4/,k( 205)/  1/
      data cf( 206)/-.20874734498708296871456137622246D+07/
      data i( 206)/  5/,j( 206)/  1/,k( 206)/  4/
      data cf( 207)/-.20874734498708296871456137622246D+07/
      data i( 207)/  4/,j( 207)/  5/,k( 207)/  1/
      data cf( 208)/-.20874734498708296871456137622246D+07/
      data i( 208)/  4/,j( 208)/  1/,k( 208)/  5/
      data cf( 209)/-.20874734498708296871456137622246D+07/
      data i( 209)/  1/,j( 209)/  5/,k( 209)/  4/
      data cf( 210)/-.20874734498708296871456137622246D+07/
      data i( 210)/  1/,j( 210)/  4/,k( 210)/  5/
      data cf( 211)/-.10122511153450307128542462304012D+08/
      data i( 211)/  5/,j( 211)/  5/,k( 211)/  0/
      data cf( 212)/-.10122511153450307128542462304012D+08/
      data i( 212)/  5/,j( 212)/  0/,k( 212)/  5/
      data cf( 213)/-.10122511153450307128542462304012D+08/
      data i( 213)/  0/,j( 213)/  5/,k( 213)/  5/
      data cf( 214)/0.46576562341537797462992371506090D+07/
      data i( 214)/  6/,j( 214)/  2/,k( 214)/  2/
      data cf( 215)/0.46576562341537797462992371506090D+07/
      data i( 215)/  2/,j( 215)/  6/,k( 215)/  2/
      data cf( 216)/0.46576562341537797462992371506090D+07/
      data i( 216)/  2/,j( 216)/  2/,k( 216)/  6/
      data cf( 217)/0.24456479760932030190601210691056D+07/
      data i( 217)/  6/,j( 217)/  3/,k( 217)/  1/
      data cf( 218)/0.24456479760932030190601210691056D+07/
      data i( 218)/  6/,j( 218)/  1/,k( 218)/  3/
      data cf( 219)/0.24456479760932030190601210691056D+07/
      data i( 219)/  3/,j( 219)/  6/,k( 219)/  1/
      data cf( 220)/0.24456479760932030190601210691056D+07/
      data i( 220)/  3/,j( 220)/  1/,k( 220)/  6/
      data cf( 221)/0.24456479760932030190601210691056D+07/
      data i( 221)/  1/,j( 221)/  6/,k( 221)/  3/
      data cf( 222)/0.24456479760932030190601210691056D+07/
      data i( 222)/  1/,j( 222)/  3/,k( 222)/  6/
      data cf( 223)/-.93751472909523334657669312495297D+07/
      data i( 223)/  6/,j( 223)/  4/,k( 223)/  0/
      data cf( 224)/-.93751472909523334657669312495297D+07/
      data i( 224)/  6/,j( 224)/  0/,k( 224)/  4/
      data cf( 225)/-.93751472909523334657669312495297D+07/
      data i( 225)/  4/,j( 225)/  6/,k( 225)/  0/
      data cf( 226)/-.93751472909523334657669312495297D+07/
      data i( 226)/  4/,j( 226)/  0/,k( 226)/  6/
      data cf( 227)/-.93751472909523334657669312495297D+07/
      data i( 227)/  0/,j( 227)/  6/,k( 227)/  4/
      data cf( 228)/-.93751472909523334657669312495297D+07/
      data i( 228)/  0/,j( 228)/  4/,k( 228)/  6/
      data cf( 229)/0.22967697528876635259209243046406D+07/
      data i( 229)/  7/,j( 229)/  2/,k( 229)/  1/
      data cf( 230)/0.22967697528876635259209243046406D+07/
      data i( 230)/  7/,j( 230)/  1/,k( 230)/  2/
      data cf( 231)/0.22967697528876635259209243046406D+07/
      data i( 231)/  2/,j( 231)/  7/,k( 231)/  1/
      data cf( 232)/0.22967697528876635259209243046406D+07/
      data i( 232)/  2/,j( 232)/  1/,k( 232)/  7/
      data cf( 233)/0.22967697528876635259209243046406D+07/
      data i( 233)/  1/,j( 233)/  7/,k( 233)/  2/
      data cf( 234)/0.22967697528876635259209243046406D+07/
      data i( 234)/  1/,j( 234)/  2/,k( 234)/  7/
      data cf( 235)/-.11669029151180218939536530620478D+08/
      data i( 235)/  7/,j( 235)/  3/,k( 235)/  0/
      data cf( 236)/-.11669029151180218939536530620478D+08/
      data i( 236)/  7/,j( 236)/  0/,k( 236)/  3/
      data cf( 237)/-.11669029151180218939536530620478D+08/
      data i( 237)/  3/,j( 237)/  7/,k( 237)/  0/
      data cf( 238)/-.11669029151180218939536530620478D+08/
      data i( 238)/  3/,j( 238)/  0/,k( 238)/  7/
      data cf( 239)/-.11669029151180218939536530620478D+08/
      data i( 239)/  0/,j( 239)/  7/,k( 239)/  3/
      data cf( 240)/-.11669029151180218939536530620478D+08/
      data i( 240)/  0/,j( 240)/  3/,k( 240)/  7/
      data cf( 241)/0.47052608953720267680760291920169D+07/
      data i( 241)/  8/,j( 241)/  1/,k( 241)/  1/
      data cf( 242)/0.47052608953720267680760291920169D+07/
      data i( 242)/  1/,j( 242)/  8/,k( 242)/  1/
      data cf( 243)/0.47052608953720267680760291920169D+07/
      data i( 243)/  1/,j( 243)/  1/,k( 243)/  8/
      data cf( 244)/-.73453520065956312429934786547966D+07/
      data i( 244)/  8/,j( 244)/  2/,k( 244)/  0/
      data cf( 245)/-.73453520065956312429934786547966D+07/
      data i( 245)/  8/,j( 245)/  0/,k( 245)/  2/
      data cf( 246)/-.73453520065956312429934786547966D+07/
      data i( 246)/  2/,j( 246)/  8/,k( 246)/  0/
      data cf( 247)/-.73453520065956312429934786547966D+07/
      data i( 247)/  2/,j( 247)/  0/,k( 247)/  8/
      data cf( 248)/-.73453520065956312429934786547966D+07/
      data i( 248)/  0/,j( 248)/  8/,k( 248)/  2/
      data cf( 249)/-.73453520065956312429934786547966D+07/
      data i( 249)/  0/,j( 249)/  2/,k( 249)/  8/
      data cf( 250)/-.33628423853318544635245882637221D+07/
      data i( 250)/  9/,j( 250)/  1/,k( 250)/  0/
      data cf( 251)/-.33628423853318544635245882637221D+07/
      data i( 251)/  9/,j( 251)/  0/,k( 251)/  1/
      data cf( 252)/-.33628423853318544635245882637221D+07/
      data i( 252)/  1/,j( 252)/  9/,k( 252)/  0/
      data cf( 253)/-.33628423853318544635245882637221D+07/
      data i( 253)/  1/,j( 253)/  0/,k( 253)/  9/
      data cf( 254)/-.33628423853318544635245882637221D+07/
      data i( 254)/  0/,j( 254)/  9/,k( 254)/  1/
      data cf( 255)/-.33628423853318544635245882637221D+07/
      data i( 255)/  0/,j( 255)/  1/,k( 255)/  9/
      data cf( 256)/0.80236889767666885885680535249772D+06/
      data i( 256)/  4/,j( 256)/  4/,k( 256)/  3/
      data cf( 257)/0.80236889767666885885680535249772D+06/
      data i( 257)/  4/,j( 257)/  3/,k( 257)/  4/
      data cf( 258)/0.80236889767666885885680535249772D+06/
      data i( 258)/  3/,j( 258)/  4/,k( 258)/  4/
      data cf( 259)/0.12144479235403506175088199374682D+07/
      data i( 259)/  5/,j( 259)/  3/,k( 259)/  3/
      data cf( 260)/0.12144479235403506175088199374682D+07/
      data i( 260)/  3/,j( 260)/  5/,k( 260)/  3/
      data cf( 261)/0.12144479235403506175088199374682D+07/
      data i( 261)/  3/,j( 261)/  3/,k( 261)/  5/
      data cf( 262)/-.26776605946896459065440765461895D+07/
      data i( 262)/  5/,j( 262)/  4/,k( 262)/  2/
      data cf( 263)/-.26776605946896459065440765461895D+07/
      data i( 263)/  5/,j( 263)/  2/,k( 263)/  4/
      data cf( 264)/-.26776605946896459065440765461895D+07/
      data i( 264)/  4/,j( 264)/  5/,k( 264)/  2/
      data cf( 265)/-.26776605946896459065440765461895D+07/
      data i( 265)/  4/,j( 265)/  2/,k( 265)/  5/
      data cf( 266)/-.26776605946896459065440765461895D+07/
      data i( 266)/  2/,j( 266)/  5/,k( 266)/  4/
      data cf( 267)/-.26776605946896459065440765461895D+07/
      data i( 267)/  2/,j( 267)/  4/,k( 267)/  5/
      data cf( 268)/0.34534719091322114909134319779459D+07/
      data i( 268)/  5/,j( 268)/  5/,k( 268)/  1/
      data cf( 269)/0.34534719091322114909134319779459D+07/
      data i( 269)/  5/,j( 269)/  1/,k( 269)/  5/
      data cf( 270)/0.34534719091322114909134319779459D+07/
      data i( 270)/  1/,j( 270)/  5/,k( 270)/  5/
      data cf( 271)/-.61155356377107686066503293738734D+06/
      data i( 271)/  6/,j( 271)/  3/,k( 271)/  2/
      data cf( 272)/-.61155356377107686066503293738734D+06/
      data i( 272)/  6/,j( 272)/  2/,k( 272)/  3/
      data cf( 273)/-.61155356377107686066503293738734D+06/
      data i( 273)/  3/,j( 273)/  6/,k( 273)/  2/
      data cf( 274)/-.61155356377107686066503293738734D+06/
      data i( 274)/  3/,j( 274)/  2/,k( 274)/  6/
      data cf( 275)/-.61155356377107686066503293738734D+06/
      data i( 275)/  2/,j( 275)/  6/,k( 275)/  3/
      data cf( 276)/-.61155356377107686066503293738734D+06/
      data i( 276)/  2/,j( 276)/  3/,k( 276)/  6/
      data cf( 277)/0.11975085915713738465791398840907D+07/
      data i( 277)/  6/,j( 277)/  4/,k( 277)/  1/
      data cf( 278)/0.11975085915713738465791398840907D+07/
      data i( 278)/  6/,j( 278)/  1/,k( 278)/  4/
      data cf( 279)/0.11975085915713738465791398840907D+07/
      data i( 279)/  4/,j( 279)/  6/,k( 279)/  1/
      data cf( 280)/0.11975085915713738465791398840907D+07/
      data i( 280)/  4/,j( 280)/  1/,k( 280)/  6/
      data cf( 281)/0.11975085915713738465791398840907D+07/
      data i( 281)/  1/,j( 281)/  6/,k( 281)/  4/
      data cf( 282)/0.11975085915713738465791398840907D+07/
      data i( 282)/  1/,j( 282)/  4/,k( 282)/  6/
      data cf( 283)/0.10698768793568222819955301685753D+08/
      data i( 283)/  6/,j( 283)/  5/,k( 283)/  0/
      data cf( 284)/0.10698768793568222819955301685753D+08/
      data i( 284)/  6/,j( 284)/  0/,k( 284)/  5/
      data cf( 285)/0.10698768793568222819955301685753D+08/
      data i( 285)/  5/,j( 285)/  6/,k( 285)/  0/
      data cf( 286)/0.10698768793568222819955301685753D+08/
      data i( 286)/  5/,j( 286)/  0/,k( 286)/  6/
      data cf( 287)/0.10698768793568222819955301685753D+08/
      data i( 287)/  0/,j( 287)/  6/,k( 287)/  5/
      data cf( 288)/0.10698768793568222819955301685753D+08/
      data i( 288)/  0/,j( 288)/  5/,k( 288)/  6/
      data cf( 289)/-.55283189104956319966903867476206D+07/
      data i( 289)/  7/,j( 289)/  2/,k( 289)/  2/
      data cf( 290)/-.55283189104956319966903867476206D+07/
      data i( 290)/  2/,j( 290)/  7/,k( 290)/  2/
      data cf( 291)/-.55283189104956319966903867476206D+07/
      data i( 291)/  2/,j( 291)/  2/,k( 291)/  7/
      data cf( 292)/-.37711220938320363537099630231387D+07/
      data i( 292)/  7/,j( 292)/  3/,k( 292)/  1/
      data cf( 293)/-.37711220938320363537099630231387D+07/
      data i( 293)/  7/,j( 293)/  1/,k( 293)/  3/
      data cf( 294)/-.37711220938320363537099630231387D+07/
      data i( 294)/  3/,j( 294)/  7/,k( 294)/  1/
      data cf( 295)/-.37711220938320363537099630231387D+07/
      data i( 295)/  3/,j( 295)/  1/,k( 295)/  7/
      data cf( 296)/-.37711220938320363537099630231387D+07/
      data i( 296)/  1/,j( 296)/  7/,k( 296)/  3/
      data cf( 297)/-.37711220938320363537099630231387D+07/
      data i( 297)/  1/,j( 297)/  3/,k( 297)/  7/
      data cf( 298)/0.11223020731115249797719221773068D+08/
      data i( 298)/  7/,j( 298)/  4/,k( 298)/  0/
      data cf( 299)/0.11223020731115249797719221773068D+08/
      data i( 299)/  7/,j( 299)/  0/,k( 299)/  4/
      data cf( 300)/0.11223020731115249797719221773068D+08/
      data i( 300)/  4/,j( 300)/  7/,k( 300)/  0/
      data cf( 301)/0.11223020731115249797719221773068D+08/
      data i( 301)/  4/,j( 301)/  0/,k( 301)/  7/
      data cf( 302)/0.11223020731115249797719221773068D+08/
      data i( 302)/  0/,j( 302)/  7/,k( 302)/  4/
      data cf( 303)/0.11223020731115249797719221773068D+08/
      data i( 303)/  0/,j( 303)/  4/,k( 303)/  7/
      data cf( 304)/-.74985292549430941380995809565814D+06/
      data i( 304)/  8/,j( 304)/  2/,k( 304)/  1/
      data cf( 305)/-.74985292549430941380995809565814D+06/
      data i( 305)/  8/,j( 305)/  1/,k( 305)/  2/
      data cf( 306)/-.74985292549430941380995809565814D+06/
      data i( 306)/  2/,j( 306)/  8/,k( 306)/  1/
      data cf( 307)/-.74985292549430941380995809565814D+06/
      data i( 307)/  2/,j( 307)/  1/,k( 307)/  8/
      data cf( 308)/-.74985292549430941380995809565814D+06/
      data i( 308)/  1/,j( 308)/  8/,k( 308)/  2/
      data cf( 309)/-.74985292549430941380995809565814D+06/
      data i( 309)/  1/,j( 309)/  2/,k( 309)/  8/
      data cf( 310)/0.16402486795332626059603076895642D+08/
      data i( 310)/  8/,j( 310)/  3/,k( 310)/  0/
      data cf( 311)/0.16402486795332626059603076895642D+08/
      data i( 311)/  8/,j( 311)/  0/,k( 311)/  3/
      data cf( 312)/0.16402486795332626059603076895642D+08/
      data i( 312)/  3/,j( 312)/  8/,k( 312)/  0/
      data cf( 313)/0.16402486795332626059603076895642D+08/
      data i( 313)/  3/,j( 313)/  0/,k( 313)/  8/
      data cf( 314)/0.16402486795332626059603076895642D+08/
      data i( 314)/  0/,j( 314)/  8/,k( 314)/  3/
      data cf( 315)/0.16402486795332626059603076895642D+08/
      data i( 315)/  0/,j( 315)/  3/,k( 315)/  8/
      data cf( 316)/-.43782287057546535257553363070282D+07/
      data i( 316)/  9/,j( 316)/  1/,k( 316)/  1/
      data cf( 317)/-.43782287057546535257553363070282D+07/
      data i( 317)/  1/,j( 317)/  9/,k( 317)/  1/
      data cf( 318)/-.43782287057546535257553363070282D+07/
      data i( 318)/  1/,j( 318)/  1/,k( 318)/  9/
      data cf( 319)/0.52382081804942984470490611787930D+07/
      data i( 319)/  9/,j( 319)/  2/,k( 319)/  0/
      data cf( 320)/0.52382081804942984470490611787930D+07/
      data i( 320)/  9/,j( 320)/  0/,k( 320)/  2/
      data cf( 321)/0.52382081804942984470490611787930D+07/
      data i( 321)/  2/,j( 321)/  9/,k( 321)/  0/
      data cf( 322)/0.52382081804942984470490611787930D+07/
      data i( 322)/  2/,j( 322)/  0/,k( 322)/  9/
      data cf( 323)/0.52382081804942984470490611787930D+07/
      data i( 323)/  0/,j( 323)/  9/,k( 323)/  2/
      data cf( 324)/0.52382081804942984470490611787930D+07/
      data i( 324)/  0/,j( 324)/  2/,k( 324)/  9/
      data cf( 325)/0.33448462606762958080999981057018D+07/
      data i( 325)/ 10/,j( 325)/  1/,k( 325)/  0/
      data cf( 326)/0.33448462606762958080999981057018D+07/
      data i( 326)/ 10/,j( 326)/  0/,k( 326)/  1/
      data cf( 327)/0.33448462606762958080999981057018D+07/
      data i( 327)/  1/,j( 327)/ 10/,k( 327)/  0/
      data cf( 328)/0.33448462606762958080999981057018D+07/
      data i( 328)/  1/,j( 328)/  0/,k( 328)/ 10/
      data cf( 329)/0.33448462606762958080999981057018D+07/
      data i( 329)/  0/,j( 329)/ 10/,k( 329)/  1/
      data cf( 330)/0.33448462606762958080999981057018D+07/
      data i( 330)/  0/,j( 330)/  1/,k( 330)/ 10/
      data cf( 331)/0.59797447690210099979586444785987D+06/
      data i( 331)/  4/,j( 331)/  4/,k( 331)/  4/
      data cf( 332)/-.12393970426137407424037591784517D+07/
      data i( 332)/  5/,j( 332)/  4/,k( 332)/  3/
      data cf( 333)/-.12393970426137407424037591784517D+07/
      data i( 333)/  5/,j( 333)/  3/,k( 333)/  4/
      data cf( 334)/-.12393970426137407424037591784517D+07/
      data i( 334)/  4/,j( 334)/  5/,k( 334)/  3/
      data cf( 335)/-.12393970426137407424037591784517D+07/
      data i( 335)/  4/,j( 335)/  3/,k( 335)/  5/
      data cf( 336)/-.12393970426137407424037591784517D+07/
      data i( 336)/  3/,j( 336)/  5/,k( 336)/  4/
      data cf( 337)/-.12393970426137407424037591784517D+07/
      data i( 337)/  3/,j( 337)/  4/,k( 337)/  5/
      data cf( 338)/0.56303736422101674729017032611037D+07/
      data i( 338)/  5/,j( 338)/  5/,k( 338)/  2/
      data cf( 339)/0.56303736422101674729017032611037D+07/
      data i( 339)/  5/,j( 339)/  2/,k( 339)/  5/
      data cf( 340)/0.56303736422101674729017032611037D+07/
      data i( 340)/  2/,j( 340)/  5/,k( 340)/  5/
      data cf( 341)/0.15852279403505396724411899796164D+07/
      data i( 341)/  6/,j( 341)/  3/,k( 341)/  3/
      data cf( 342)/0.15852279403505396724411899796164D+07/
      data i( 342)/  3/,j( 342)/  6/,k( 342)/  3/
      data cf( 343)/0.15852279403505396724411899796164D+07/
      data i( 343)/  3/,j( 343)/  3/,k( 343)/  6/
      data cf( 344)/-.13976376628889985287986171710368D+07/
      data i( 344)/  6/,j( 344)/  4/,k( 344)/  2/
      data cf( 345)/-.13976376628889985287986171710368D+07/
      data i( 345)/  6/,j( 345)/  2/,k( 345)/  4/
      data cf( 346)/-.13976376628889985287986171710368D+07/
      data i( 346)/  4/,j( 346)/  6/,k( 346)/  2/
      data cf( 347)/-.13976376628889985287986171710368D+07/
      data i( 347)/  4/,j( 347)/  2/,k( 347)/  6/
      data cf( 348)/-.13976376628889985287986171710368D+07/
      data i( 348)/  2/,j( 348)/  6/,k( 348)/  4/
      data cf( 349)/-.13976376628889985287986171710368D+07/
      data i( 349)/  2/,j( 349)/  4/,k( 349)/  6/
      data cf( 350)/-.23147712607681523212126123815198D+07/
      data i( 350)/  6/,j( 350)/  5/,k( 350)/  1/
      data cf( 351)/-.23147712607681523212126123815198D+07/
      data i( 351)/  6/,j( 351)/  1/,k( 351)/  5/
      data cf( 352)/-.23147712607681523212126123815198D+07/
      data i( 352)/  5/,j( 352)/  6/,k( 352)/  1/
      data cf( 353)/-.23147712607681523212126123815198D+07/
      data i( 353)/  5/,j( 353)/  1/,k( 353)/  6/
      data cf( 354)/-.23147712607681523212126123815198D+07/
      data i( 354)/  1/,j( 354)/  6/,k( 354)/  5/
      data cf( 355)/-.23147712607681523212126123815198D+07/
      data i( 355)/  1/,j( 355)/  5/,k( 355)/  6/
      data cf( 356)/-.92712184115187397025913109599901D+07/
      data i( 356)/  6/,j( 356)/  6/,k( 356)/  0/
      data cf( 357)/-.92712184115187397025913109599901D+07/
      data i( 357)/  6/,j( 357)/  0/,k( 357)/  6/
      data cf( 358)/-.92712184115187397025913109599901D+07/
      data i( 358)/  0/,j( 358)/  6/,k( 358)/  6/
      data cf( 359)/0.43874314656579342170870726203951D+06/
      data i( 359)/  7/,j( 359)/  3/,k( 359)/  2/
      data cf( 360)/0.43874314656579342170870726203951D+06/
      data i( 360)/  7/,j( 360)/  2/,k( 360)/  3/
      data cf( 361)/0.43874314656579342170870726203951D+06/
      data i( 361)/  3/,j( 361)/  7/,k( 361)/  2/
      data cf( 362)/0.43874314656579342170870726203951D+06/
      data i( 362)/  3/,j( 362)/  2/,k( 362)/  7/
      data cf( 363)/0.43874314656579342170870726203951D+06/
      data i( 363)/  2/,j( 363)/  7/,k( 363)/  3/
      data cf( 364)/0.43874314656579342170870726203951D+06/
      data i( 364)/  2/,j( 364)/  3/,k( 364)/  7/
      data cf( 365)/0.13747497160327036626109536678161D+07/
      data i( 365)/  7/,j( 365)/  4/,k( 365)/  1/
      data cf( 366)/0.13747497160327036626109536678161D+07/
      data i( 366)/  7/,j( 366)/  1/,k( 366)/  4/
      data cf( 367)/0.13747497160327036626109536678161D+07/
      data i( 367)/  4/,j( 367)/  7/,k( 367)/  1/
      data cf( 368)/0.13747497160327036626109536678161D+07/
      data i( 368)/  4/,j( 368)/  1/,k( 368)/  7/
      data cf( 369)/0.13747497160327036626109536678161D+07/
      data i( 369)/  1/,j( 369)/  7/,k( 369)/  4/
      data cf( 370)/0.13747497160327036626109536678161D+07/
      data i( 370)/  1/,j( 370)/  4/,k( 370)/  7/
      data cf( 371)/-.27929742247807615420230843280893D+07/
      data i( 371)/  7/,j( 371)/  5/,k( 371)/  0/
      data cf( 372)/-.27929742247807615420230843280893D+07/
      data i( 372)/  7/,j( 372)/  0/,k( 372)/  5/
      data cf( 373)/-.27929742247807615420230843280893D+07/
      data i( 373)/  5/,j( 373)/  7/,k( 373)/  0/
      data cf( 374)/-.27929742247807615420230843280893D+07/
      data i( 374)/  5/,j( 374)/  0/,k( 374)/  7/
      data cf( 375)/-.27929742247807615420230843280893D+07/
      data i( 375)/  0/,j( 375)/  7/,k( 375)/  5/
      data cf( 376)/-.27929742247807615420230843280893D+07/
      data i( 376)/  0/,j( 376)/  5/,k( 376)/  7/
      data cf( 377)/0.30599226611802792458556281686342D+07/
      data i( 377)/  8/,j( 377)/  2/,k( 377)/  2/
      data cf( 378)/0.30599226611802792458556281686342D+07/
      data i( 378)/  2/,j( 378)/  8/,k( 378)/  2/
      data cf( 379)/0.30599226611802792458556281686342D+07/
      data i( 379)/  2/,j( 379)/  2/,k( 379)/  8/
      data cf( 380)/0.11881517670050509998701383919061D+07/
      data i( 380)/  8/,j( 380)/  3/,k( 380)/  1/
      data cf( 381)/0.11881517670050509998701383919061D+07/
      data i( 381)/  8/,j( 381)/  1/,k( 381)/  3/
      data cf( 382)/0.11881517670050509998701383919061D+07/
      data i( 382)/  3/,j( 382)/  8/,k( 382)/  1/
      data cf( 383)/0.11881517670050509998701383919061D+07/
      data i( 383)/  3/,j( 383)/  1/,k( 383)/  8/
      data cf( 384)/0.11881517670050509998701383919061D+07/
      data i( 384)/  1/,j( 384)/  8/,k( 384)/  3/
      data cf( 385)/0.11881517670050509998701383919061D+07/
      data i( 385)/  1/,j( 385)/  3/,k( 385)/  8/
      data cf( 386)/-.67256610816316195914622029539818D+07/
      data i( 386)/  8/,j( 386)/  4/,k( 386)/  0/
      data cf( 387)/-.67256610816316195914622029539818D+07/
      data i( 387)/  8/,j( 387)/  0/,k( 387)/  4/
      data cf( 388)/-.67256610816316195914622029539818D+07/
      data i( 388)/  4/,j( 388)/  8/,k( 388)/  0/
      data cf( 389)/-.67256610816316195914622029539818D+07/
      data i( 389)/  4/,j( 389)/  0/,k( 389)/  8/
      data cf( 390)/-.67256610816316195914622029539818D+07/
      data i( 390)/  0/,j( 390)/  8/,k( 390)/  4/
      data cf( 391)/-.67256610816316195914622029539818D+07/
      data i( 391)/  0/,j( 391)/  4/,k( 391)/  8/
      data cf( 392)/-.26992599739992188476818987942203D+06/
      data i( 392)/  9/,j( 392)/  2/,k( 392)/  1/
      data cf( 393)/-.26992599739992188476818987942203D+06/
      data i( 393)/  9/,j( 393)/  1/,k( 393)/  2/
      data cf( 394)/-.26992599739992188476818987942203D+06/
      data i( 394)/  2/,j( 394)/  9/,k( 394)/  1/
      data cf( 395)/-.26992599739992188476818987942203D+06/
      data i( 395)/  2/,j( 395)/  1/,k( 395)/  9/
      data cf( 396)/-.26992599739992188476818987942203D+06/
      data i( 396)/  1/,j( 396)/  9/,k( 396)/  2/
      data cf( 397)/-.26992599739992188476818987942203D+06/
      data i( 397)/  1/,j( 397)/  2/,k( 397)/  9/
      data cf( 398)/-.98196281702020969861814829706640D+07/
      data i( 398)/  9/,j( 398)/  3/,k( 398)/  0/
      data cf( 399)/-.98196281702020969861814829706640D+07/
      data i( 399)/  9/,j( 399)/  0/,k( 399)/  3/
      data cf( 400)/-.98196281702020969861814829706640D+07/
      data i( 400)/  3/,j( 400)/  9/,k( 400)/  0/
      data cf( 401)/-.98196281702020969861814829706640D+07/
      data i( 401)/  3/,j( 401)/  0/,k( 401)/  9/
      data cf( 402)/-.98196281702020969861814829706640D+07/
      data i( 402)/  0/,j( 402)/  9/,k( 402)/  3/
      data cf( 403)/-.98196281702020969861814829706640D+07/
      data i( 403)/  0/,j( 403)/  3/,k( 403)/  9/
      data cf( 404)/0.17997827518539795323466803282112D+07/
      data i( 404)/ 10/,j( 404)/  1/,k( 404)/  1/
      data cf( 405)/0.17997827518539795323466803282112D+07/
      data i( 405)/  1/,j( 405)/ 10/,k( 405)/  1/
      data cf( 406)/0.17997827518539795323466803282112D+07/
      data i( 406)/  1/,j( 406)/  1/,k( 406)/ 10/
      data cf( 407)/0.24600956280432086268692396897257D+06/
      data i( 407)/ 10/,j( 407)/  2/,k( 407)/  0/
      data cf( 408)/0.24600956280432086268692396897257D+06/
      data i( 408)/ 10/,j( 408)/  0/,k( 408)/  2/
      data cf( 409)/0.24600956280432086268692396897257D+06/
      data i( 409)/  2/,j( 409)/ 10/,k( 409)/  0/
      data cf( 410)/0.24600956280432086268692396897257D+06/
      data i( 410)/  2/,j( 410)/  0/,k( 410)/ 10/
      data cf( 411)/0.24600956280432086268692396897257D+06/
      data i( 411)/  0/,j( 411)/ 10/,k( 411)/  2/
      data cf( 412)/0.24600956280432086268692396897257D+06/
      data i( 412)/  0/,j( 412)/  2/,k( 412)/ 10/
      data cf( 413)/-.14652900810410734365795679178066D+07/
      data i( 413)/ 11/,j( 413)/  1/,k( 413)/  0/
      data cf( 414)/-.14652900810410734365795679178066D+07/
      data i( 414)/ 11/,j( 414)/  0/,k( 414)/  1/
      data cf( 415)/-.14652900810410734365795679178066D+07/
      data i( 415)/  1/,j( 415)/ 11/,k( 415)/  0/
      data cf( 416)/-.14652900810410734365795679178066D+07/
      data i( 416)/  1/,j( 416)/  0/,k( 416)/ 11/
      data cf( 417)/-.14652900810410734365795679178066D+07/
      data i( 417)/  0/,j( 417)/ 11/,k( 417)/  1/
      data cf( 418)/-.14652900810410734365795679178066D+07/
      data i( 418)/  0/,j( 418)/  1/,k( 418)/ 11/
      data cf( 419)/-.72522829627116392131595828071969D+03/
      data i( 419)/  1/,j( 419)/  1/,k( 419)/  0/
      data cf( 420)/-.72522829627116392131595828071969D+03/
      data i( 420)/  1/,j( 420)/  0/,k( 420)/  1/
      data cf( 421)/-.72522829627116392131595828071969D+03/
      data i( 421)/  0/,j( 421)/  1/,k( 421)/  1/
      data cf( 422)/-.61967683813129776385492952791541D+05/
      data i( 422)/  1/,j( 422)/  1/,k( 422)/  1/
      data cf( 423)/0.41206519746672972623302097787573D+05/
      data i( 423)/  2/,j( 423)/  1/,k( 423)/  0/
      data cf( 424)/0.41206519746672972623302097787573D+05/
      data i( 424)/  2/,j( 424)/  0/,k( 424)/  1/
      data cf( 425)/0.41206519746672972623302097787573D+05/
      data i( 425)/  1/,j( 425)/  2/,k( 425)/  0/
      data cf( 426)/0.41206519746672972623302097787573D+05/
      data i( 426)/  1/,j( 426)/  0/,k( 426)/  2/
      data cf( 427)/0.41206519746672972623302097787573D+05/
      data i( 427)/  0/,j( 427)/  2/,k( 427)/  1/
      data cf( 428)/0.41206519746672972623302097787573D+05/
      data i( 428)/  0/,j( 428)/  1/,k( 428)/  2/
      data cf( 429)/0.52434629852945219863232120683649D+06/
      data i( 429)/  2/,j( 429)/  1/,k( 429)/  1/
      data cf( 430)/0.52434629852945219863232120683649D+06/
      data i( 430)/  1/,j( 430)/  2/,k( 430)/  1/
      data cf( 431)/0.52434629852945219863232120683649D+06/
      data i( 431)/  1/,j( 431)/  1/,k( 431)/  2/
      data cf( 432)/-.38101843277477255811914609653367D+06/
      data i( 432)/  2/,j( 432)/  2/,k( 432)/  0/
      data cf( 433)/-.38101843277477255811914609653367D+06/
      data i( 433)/  2/,j( 433)/  0/,k( 433)/  2/
      data cf( 434)/-.38101843277477255811914609653367D+06/
      data i( 434)/  0/,j( 434)/  2/,k( 434)/  2/
      data cf( 435)/-.86216673105153246874191278855443D+06/
      data i( 435)/  3/,j( 435)/  1/,k( 435)/  0/
      data cf( 436)/-.86216673105153246874191278855443D+06/
      data i( 436)/  3/,j( 436)/  0/,k( 436)/  1/
      data cf( 437)/-.86216673105153246874191278855443D+06/
      data i( 437)/  1/,j( 437)/  3/,k( 437)/  0/
      data cf( 438)/-.86216673105153246874191278855443D+06/
      data i( 438)/  1/,j( 438)/  0/,k( 438)/  3/
      data cf( 439)/-.86216673105153246874191278855443D+06/
      data i( 439)/  0/,j( 439)/  3/,k( 439)/  1/
      data cf( 440)/-.86216673105153246874191278855443D+06/
      data i( 440)/  0/,j( 440)/  1/,k( 440)/  3/
      data cf( 441)/-.49662014106046966368721004909470D+07/
      data i( 441)/  2/,j( 441)/  2/,k( 441)/  1/
      data cf( 442)/-.49662014106046966368721004909470D+07/
      data i( 442)/  2/,j( 442)/  1/,k( 442)/  2/
      data cf( 443)/-.49662014106046966368721004909470D+07/
      data i( 443)/  1/,j( 443)/  2/,k( 443)/  2/
      data cf( 444)/-.13992853771444356263254369706718D+07/
      data i( 444)/  3/,j( 444)/  1/,k( 444)/  1/
      data cf( 445)/-.13992853771444356263254369706718D+07/
      data i( 445)/  1/,j( 445)/  3/,k( 445)/  1/
      data cf( 446)/-.13992853771444356263254369706718D+07/
      data i( 446)/  1/,j( 446)/  1/,k( 446)/  3/
      data cf( 447)/0.52557769475242515153954665749608D+07/
      data i( 447)/  3/,j( 447)/  2/,k( 447)/  0/
      data cf( 448)/0.52557769475242515153954665749608D+07/
      data i( 448)/  3/,j( 448)/  0/,k( 448)/  2/
      data cf( 449)/0.52557769475242515153954665749608D+07/
      data i( 449)/  2/,j( 449)/  3/,k( 449)/  0/
      data cf( 450)/0.52557769475242515153954665749608D+07/
      data i( 450)/  2/,j( 450)/  0/,k( 450)/  3/
      data cf( 451)/0.52557769475242515153954665749608D+07/
      data i( 451)/  0/,j( 451)/  3/,k( 451)/  2/
      data cf( 452)/0.52557769475242515153954665749608D+07/
      data i( 452)/  0/,j( 452)/  2/,k( 452)/  3/
      data cf( 453)/0.95752510468970836390467208941262D+07/
      data i( 453)/  4/,j( 453)/  1/,k( 453)/  0/
      data cf( 454)/0.95752510468970836390467208941262D+07/
      data i( 454)/  4/,j( 454)/  0/,k( 454)/  1/
      data cf( 455)/0.95752510468970836390467208941262D+07/
      data i( 455)/  1/,j( 455)/  4/,k( 455)/  0/
      data cf( 456)/0.95752510468970836390467208941262D+07/
      data i( 456)/  1/,j( 456)/  0/,k( 456)/  4/
      data cf( 457)/0.95752510468970836390467208941262D+07/
      data i( 457)/  0/,j( 457)/  4/,k( 457)/  1/
      data cf( 458)/0.95752510468970836390467208941262D+07/
      data i( 458)/  0/,j( 458)/  1/,k( 458)/  4/
      data cf( 459)/0.41142691500781596377169882959643D+08/
      data i( 459)/  2/,j( 459)/  2/,k( 459)/  2/
      data cf( 460)/0.20759231193906756895792724565397D+08/
      data i( 460)/  3/,j( 460)/  2/,k( 460)/  1/
      data cf( 461)/0.20759231193906756895792724565397D+08/
      data i( 461)/  3/,j( 461)/  1/,k( 461)/  2/
      data cf( 462)/0.20759231193906756895792724565397D+08/
      data i( 462)/  2/,j( 462)/  3/,k( 462)/  1/
      data cf( 463)/0.20759231193906756895792724565397D+08/
      data i( 463)/  2/,j( 463)/  1/,k( 463)/  3/
      data cf( 464)/0.20759231193906756895792724565397D+08/
      data i( 464)/  1/,j( 464)/  3/,k( 464)/  2/
      data cf( 465)/0.20759231193906756895792724565397D+08/
      data i( 465)/  1/,j( 465)/  2/,k( 465)/  3/
      data cf( 466)/-.19417534318017157066619491816205D+08/
      data i( 466)/  3/,j( 466)/  3/,k( 466)/  0/
      data cf( 467)/-.19417534318017157066619491816205D+08/
      data i( 467)/  3/,j( 467)/  0/,k( 467)/  3/
      data cf( 468)/-.19417534318017157066619491816205D+08/
      data i( 468)/  0/,j( 468)/  3/,k( 468)/  3/
      data cf( 469)/-.71890546060792673840896576016358D+07/
      data i( 469)/  4/,j( 469)/  1/,k( 469)/  1/
      data cf( 470)/-.71890546060792673840896576016358D+07/
      data i( 470)/  1/,j( 470)/  4/,k( 470)/  1/
      data cf( 471)/-.71890546060792673840896576016358D+07/
      data i( 471)/  1/,j( 471)/  1/,k( 471)/  4/
      data cf( 472)/-.63973672946511250544077174400245D+08/
      data i( 472)/  4/,j( 472)/  2/,k( 472)/  0/
      data cf( 473)/-.63973672946511250544077174400245D+08/
      data i( 473)/  4/,j( 473)/  0/,k( 473)/  2/
      data cf( 474)/-.63973672946511250544077174400245D+08/
      data i( 474)/  2/,j( 474)/  4/,k( 474)/  0/
      data cf( 475)/-.63973672946511250544077174400245D+08/
      data i( 475)/  2/,j( 475)/  0/,k( 475)/  4/
      data cf( 476)/-.63973672946511250544077174400245D+08/
      data i( 476)/  0/,j( 476)/  4/,k( 476)/  2/
      data cf( 477)/-.63973672946511250544077174400245D+08/
      data i( 477)/  0/,j( 477)/  2/,k( 477)/  4/
      data cf( 478)/-.64042346054470764261803704106245D+08/
      data i( 478)/  5/,j( 478)/  1/,k( 478)/  0/
      data cf( 479)/-.64042346054470764261803704106245D+08/
      data i( 479)/  5/,j( 479)/  0/,k( 479)/  1/
      data cf( 480)/-.64042346054470764261803704106245D+08/
      data i( 480)/  1/,j( 480)/  5/,k( 480)/  0/
      data cf( 481)/-.64042346054470764261803704106245D+08/
      data i( 481)/  1/,j( 481)/  0/,k( 481)/  5/
      data cf( 482)/-.64042346054470764261803704106245D+08/
      data i( 482)/  0/,j( 482)/  5/,k( 482)/  1/
      data cf( 483)/-.64042346054470764261803704106245D+08/
      data i( 483)/  0/,j( 483)/  1/,k( 483)/  5/
      data cf( 484)/-.16744866138426363527790067954409D+09/
      data i( 484)/  3/,j( 484)/  2/,k( 484)/  2/
      data cf( 485)/-.16744866138426363527790067954409D+09/
      data i( 485)/  2/,j( 485)/  3/,k( 485)/  2/
      data cf( 486)/-.16744866138426363527790067954409D+09/
      data i( 486)/  2/,j( 486)/  2/,k( 486)/  3/
      data cf( 487)/-.12726053659456480209381819897857D+09/
      data i( 487)/  3/,j( 487)/  3/,k( 487)/  1/
      data cf( 488)/-.12726053659456480209381819897857D+09/
      data i( 488)/  3/,j( 488)/  1/,k( 488)/  3/
      data cf( 489)/-.12726053659456480209381819897857D+09/
      data i( 489)/  1/,j( 489)/  3/,k( 489)/  3/
      data cf( 490)/-.14313338363124844889645123292001D+08/
      data i( 490)/  4/,j( 490)/  2/,k( 490)/  1/
      data cf( 491)/-.14313338363124844889645123292001D+08/
      data i( 491)/  4/,j( 491)/  1/,k( 491)/  2/
      data cf( 492)/-.14313338363124844889645123292001D+08/
      data i( 492)/  2/,j( 492)/  4/,k( 492)/  1/
      data cf( 493)/-.14313338363124844889645123292001D+08/
      data i( 493)/  2/,j( 493)/  1/,k( 493)/  4/
      data cf( 494)/-.14313338363124844889645123292001D+08/
      data i( 494)/  1/,j( 494)/  4/,k( 494)/  2/
      data cf( 495)/-.14313338363124844889645123292001D+08/
      data i( 495)/  1/,j( 495)/  2/,k( 495)/  4/
      data cf( 496)/0.16935794457065965462813460673300D+09/
      data i( 496)/  4/,j( 496)/  3/,k( 496)/  0/
      data cf( 497)/0.16935794457065965462813460673300D+09/
      data i( 497)/  4/,j( 497)/  0/,k( 497)/  3/
      data cf( 498)/0.16935794457065965462813460673300D+09/
      data i( 498)/  3/,j( 498)/  4/,k( 498)/  0/
      data cf( 499)/0.16935794457065965462813460673300D+09/
      data i( 499)/  3/,j( 499)/  0/,k( 499)/  4/
      data cf( 500)/0.16935794457065965462813460673300D+09/
      data i( 500)/  0/,j( 500)/  4/,k( 500)/  3/
      data cf( 501)/0.16935794457065965462813460673300D+09/
      data i( 501)/  0/,j( 501)/  3/,k( 501)/  4/
      data cf( 502)/0.76402903542738151348980143808572D+08/
      data i( 502)/  5/,j( 502)/  1/,k( 502)/  1/
      data cf( 503)/0.76402903542738151348980143808572D+08/
      data i( 503)/  1/,j( 503)/  5/,k( 503)/  1/
      data cf( 504)/0.76402903542738151348980143808572D+08/
      data i( 504)/  1/,j( 504)/  1/,k( 504)/  5/
      data cf( 505)/0.43979846545931080755615090100847D+09/
      data i( 505)/  5/,j( 505)/  2/,k( 505)/  0/
      data cf( 506)/0.43979846545931080755615090100847D+09/
      data i( 506)/  5/,j( 506)/  0/,k( 506)/  2/
      data cf( 507)/0.43979846545931080755615090100847D+09/
      data i( 507)/  2/,j( 507)/  5/,k( 507)/  0/
      data cf( 508)/0.43979846545931080755615090100847D+09/
      data i( 508)/  2/,j( 508)/  0/,k( 508)/  5/
      data cf( 509)/0.43979846545931080755615090100847D+09/
      data i( 509)/  0/,j( 509)/  5/,k( 509)/  2/
      data cf( 510)/0.43979846545931080755615090100847D+09/
      data i( 510)/  0/,j( 510)/  2/,k( 510)/  5/
      data cf( 511)/0.27607333372757796615635911088164D+09/
      data i( 511)/  6/,j( 511)/  1/,k( 511)/  0/
      data cf( 512)/0.27607333372757796615635911088164D+09/
      data i( 512)/  6/,j( 512)/  0/,k( 512)/  1/
      data cf( 513)/0.27607333372757796615635911088164D+09/
      data i( 513)/  1/,j( 513)/  6/,k( 513)/  0/
      data cf( 514)/0.27607333372757796615635911088164D+09/
      data i( 514)/  1/,j( 514)/  0/,k( 514)/  6/
      data cf( 515)/0.27607333372757796615635911088164D+09/
      data i( 515)/  0/,j( 515)/  6/,k( 515)/  1/
      data cf( 516)/0.27607333372757796615635911088164D+09/
      data i( 516)/  0/,j( 516)/  1/,k( 516)/  6/
      data cf( 517)/0.71397640988085856296000426661100D+09/
      data i( 517)/  3/,j( 517)/  3/,k( 517)/  2/
      data cf( 518)/0.71397640988085856296000426661100D+09/
      data i( 518)/  3/,j( 518)/  2/,k( 518)/  3/
      data cf( 519)/0.71397640988085856296000426661100D+09/
      data i( 519)/  2/,j( 519)/  3/,k( 519)/  3/
      data cf( 520)/0.26737995027950411189793933311983D+09/
      data i( 520)/  4/,j( 520)/  2/,k( 520)/  2/
      data cf( 521)/0.26737995027950411189793933311983D+09/
      data i( 521)/  2/,j( 521)/  4/,k( 521)/  2/
      data cf( 522)/0.26737995027950411189793933311983D+09/
      data i( 522)/  2/,j( 522)/  2/,k( 522)/  4/
      data cf( 523)/0.34019503378357302023083976460225D+09/
      data i( 523)/  4/,j( 523)/  3/,k( 523)/  1/
      data cf( 524)/0.34019503378357302023083976460225D+09/
      data i( 524)/  4/,j( 524)/  1/,k( 524)/  3/
      data cf( 525)/0.34019503378357302023083976460225D+09/
      data i( 525)/  3/,j( 525)/  4/,k( 525)/  1/
      data cf( 526)/0.34019503378357302023083976460225D+09/
      data i( 526)/  3/,j( 526)/  1/,k( 526)/  4/
      data cf( 527)/0.34019503378357302023083976460225D+09/
      data i( 527)/  1/,j( 527)/  4/,k( 527)/  3/
      data cf( 528)/0.34019503378357302023083976460225D+09/
      data i( 528)/  1/,j( 528)/  3/,k( 528)/  4/
      data cf( 529)/-.29178571593661837716257696546298D+09/
      data i( 529)/  4/,j( 529)/  4/,k( 529)/  0/
      data cf( 530)/-.29178571593661837716257696546298D+09/
      data i( 530)/  4/,j( 530)/  0/,k( 530)/  4/
      data cf( 531)/-.29178571593661837716257696546298D+09/
      data i( 531)/  0/,j( 531)/  4/,k( 531)/  4/
      data cf( 532)/-.22832442532454057656723357185519D+09/
      data i( 532)/  5/,j( 532)/  2/,k( 532)/  1/
      data cf( 533)/-.22832442532454057656723357185519D+09/
      data i( 533)/  5/,j( 533)/  1/,k( 533)/  2/
      data cf( 534)/-.22832442532454057656723357185519D+09/
      data i( 534)/  2/,j( 534)/  5/,k( 534)/  1/
      data cf( 535)/-.22832442532454057656723357185519D+09/
      data i( 535)/  2/,j( 535)/  1/,k( 535)/  5/
      data cf( 536)/-.22832442532454057656723357185519D+09/
      data i( 536)/  1/,j( 536)/  5/,k( 536)/  2/
      data cf( 537)/-.22832442532454057656723357185519D+09/
      data i( 537)/  1/,j( 537)/  2/,k( 537)/  5/
      data cf( 538)/-.12446163563309238868152869167620D+10/
      data i( 538)/  5/,j( 538)/  3/,k( 538)/  0/
      data cf( 539)/-.12446163563309238868152869167620D+10/
      data i( 539)/  5/,j( 539)/  0/,k( 539)/  3/
      data cf( 540)/-.12446163563309238868152869167620D+10/
      data i( 540)/  3/,j( 540)/  5/,k( 540)/  0/
      data cf( 541)/-.12446163563309238868152869167620D+10/
      data i( 541)/  3/,j( 541)/  0/,k( 541)/  5/
      data cf( 542)/-.12446163563309238868152869167620D+10/
      data i( 542)/  0/,j( 542)/  5/,k( 542)/  3/
      data cf( 543)/-.12446163563309238868152869167620D+10/
      data i( 543)/  0/,j( 543)/  3/,k( 543)/  5/
      data cf( 544)/-.31799293610125175715892049230448D+09/
      data i( 544)/  6/,j( 544)/  1/,k( 544)/  1/
      data cf( 545)/-.31799293610125175715892049230448D+09/
      data i( 545)/  1/,j( 545)/  6/,k( 545)/  1/
      data cf( 546)/-.31799293610125175715892049230448D+09/
      data i( 546)/  1/,j( 546)/  1/,k( 546)/  6/
      data cf( 547)/-.17697859484210400365302489700869D+10/
      data i( 547)/  6/,j( 547)/  2/,k( 547)/  0/
      data cf( 548)/-.17697859484210400365302489700869D+10/
      data i( 548)/  6/,j( 548)/  0/,k( 548)/  2/
      data cf( 549)/-.17697859484210400365302489700869D+10/
      data i( 549)/  2/,j( 549)/  6/,k( 549)/  0/
      data cf( 550)/-.17697859484210400365302489700869D+10/
      data i( 550)/  2/,j( 550)/  0/,k( 550)/  6/
      data cf( 551)/-.17697859484210400365302489700869D+10/
      data i( 551)/  0/,j( 551)/  6/,k( 551)/  2/
      data cf( 552)/-.17697859484210400365302489700869D+10/
      data i( 552)/  0/,j( 552)/  2/,k( 552)/  6/
      data cf( 553)/-.81177732961683850006144213408670D+09/
      data i( 553)/  7/,j( 553)/  1/,k( 553)/  0/
      data cf( 554)/-.81177732961683850006144213408670D+09/
      data i( 554)/  7/,j( 554)/  0/,k( 554)/  1/
      data cf( 555)/-.81177732961683850006144213408670D+09/
      data i( 555)/  1/,j( 555)/  7/,k( 555)/  0/
      data cf( 556)/-.81177732961683850006144213408670D+09/
      data i( 556)/  1/,j( 556)/  0/,k( 556)/  7/
      data cf( 557)/-.81177732961683850006144213408670D+09/
      data i( 557)/  0/,j( 557)/  7/,k( 557)/  1/
      data cf( 558)/-.81177732961683850006144213408670D+09/
      data i( 558)/  0/,j( 558)/  1/,k( 558)/  7/
      data cf( 559)/-.22767773163796808801807508361030D+10/
      data i( 559)/  3/,j( 559)/  3/,k( 559)/  3/
      data cf( 560)/-.15247688384395518681652733430210D+10/
      data i( 560)/  4/,j( 560)/  3/,k( 560)/  2/
      data cf( 561)/-.15247688384395518681652733430210D+10/
      data i( 561)/  4/,j( 561)/  2/,k( 561)/  3/
      data cf( 562)/-.15247688384395518681652733430210D+10/
      data i( 562)/  3/,j( 562)/  4/,k( 562)/  2/
      data cf( 563)/-.15247688384395518681652733430210D+10/
      data i( 563)/  3/,j( 563)/  2/,k( 563)/  4/
      data cf( 564)/-.15247688384395518681652733430210D+10/
      data i( 564)/  2/,j( 564)/  4/,k( 564)/  3/
      data cf( 565)/-.15247688384395518681652733430210D+10/
      data i( 565)/  2/,j( 565)/  3/,k( 565)/  4/
      data cf( 566)/-.13049711372721738619655107806946D+10/
      data i( 566)/  4/,j( 566)/  4/,k( 566)/  1/
      data cf( 567)/-.13049711372721738619655107806946D+10/
      data i( 567)/  4/,j( 567)/  1/,k( 567)/  4/
      data cf( 568)/-.13049711372721738619655107806946D+10/
      data i( 568)/  1/,j( 568)/  4/,k( 568)/  4/
      data cf( 569)/0.42751753258469390769690098207705D+09/
      data i( 569)/  5/,j( 569)/  2/,k( 569)/  2/
      data cf( 570)/0.42751753258469390769690098207705D+09/
      data i( 570)/  2/,j( 570)/  5/,k( 570)/  2/
      data cf( 571)/0.42751753258469390769690098207705D+09/
      data i( 571)/  2/,j( 571)/  2/,k( 571)/  5/
      data cf( 572)/-.17841960450007390002246509067221D+09/
      data i( 572)/  5/,j( 572)/  3/,k( 572)/  1/
      data cf( 573)/-.17841960450007390002246509067221D+09/
      data i( 573)/  5/,j( 573)/  1/,k( 573)/  3/
      data cf( 574)/-.17841960450007390002246509067221D+09/
      data i( 574)/  3/,j( 574)/  5/,k( 574)/  1/
      data cf( 575)/-.17841960450007390002246509067221D+09/
      data i( 575)/  3/,j( 575)/  1/,k( 575)/  5/
      data cf( 576)/-.17841960450007390002246509067221D+09/
      data i( 576)/  1/,j( 576)/  5/,k( 576)/  3/
      data cf( 577)/-.17841960450007390002246509067221D+09/
      data i( 577)/  1/,j( 577)/  3/,k( 577)/  5/
      data cf( 578)/0.13734480129271688205620444664569D+10/
      data i( 578)/  5/,j( 578)/  4/,k( 578)/  0/
      data cf( 579)/0.13734480129271688205620444664569D+10/
      data i( 579)/  5/,j( 579)/  0/,k( 579)/  4/
      data cf( 580)/0.13734480129271688205620444664569D+10/
      data i( 580)/  4/,j( 580)/  5/,k( 580)/  0/
      data cf( 581)/0.13734480129271688205620444664569D+10/
      data i( 581)/  4/,j( 581)/  0/,k( 581)/  5/
      data cf( 582)/0.13734480129271688205620444664569D+10/
      data i( 582)/  0/,j( 582)/  5/,k( 582)/  4/
      data cf( 583)/0.13734480129271688205620444664569D+10/
      data i( 583)/  0/,j( 583)/  4/,k( 583)/  5/
      data cf( 584)/0.10224676905685012490392495850528D+10/
      data i( 584)/  6/,j( 584)/  2/,k( 584)/  1/
      data cf( 585)/0.10224676905685012490392495850528D+10/
      data i( 585)/  6/,j( 585)/  1/,k( 585)/  2/
      data cf( 586)/0.10224676905685012490392495850528D+10/
      data i( 586)/  2/,j( 586)/  6/,k( 586)/  1/
      data cf( 587)/0.10224676905685012490392495850528D+10/
      data i( 587)/  2/,j( 587)/  1/,k( 587)/  6/
      data cf( 588)/0.10224676905685012490392495850528D+10/
      data i( 588)/  1/,j( 588)/  6/,k( 588)/  2/
      data cf( 589)/0.10224676905685012490392495850528D+10/
      data i( 589)/  1/,j( 589)/  2/,k( 589)/  6/
      data cf( 590)/0.48012987935330841979081110092668D+10/
      data i( 590)/  6/,j( 590)/  3/,k( 590)/  0/
      data cf( 591)/0.48012987935330841979081110092668D+10/
      data i( 591)/  6/,j( 591)/  0/,k( 591)/  3/
      data cf( 592)/0.48012987935330841979081110092668D+10/
      data i( 592)/  3/,j( 592)/  6/,k( 592)/  0/
      data cf( 593)/0.48012987935330841979081110092668D+10/
      data i( 593)/  3/,j( 593)/  0/,k( 593)/  6/
      data cf( 594)/0.48012987935330841979081110092668D+10/
      data i( 594)/  0/,j( 594)/  6/,k( 594)/  3/
      data cf( 595)/0.48012987935330841979081110092668D+10/
      data i( 595)/  0/,j( 595)/  3/,k( 595)/  6/
      data cf( 596)/0.80088924021852261493208953894666D+09/
      data i( 596)/  7/,j( 596)/  1/,k( 596)/  1/
      data cf( 597)/0.80088924021852261493208953894666D+09/
      data i( 597)/  1/,j( 597)/  7/,k( 597)/  1/
      data cf( 598)/0.80088924021852261493208953894666D+09/
      data i( 598)/  1/,j( 598)/  1/,k( 598)/  7/
      data cf( 599)/0.43980967527168240130625555749137D+10/
      data i( 599)/  7/,j( 599)/  2/,k( 599)/  0/
      data cf( 600)/0.43980967527168240130625555749137D+10/
      data i( 600)/  7/,j( 600)/  0/,k( 600)/  2/
      data cf( 601)/0.43980967527168240130625555749137D+10/
      data i( 601)/  2/,j( 601)/  7/,k( 601)/  0/
      data cf( 602)/0.43980967527168240130625555749137D+10/
      data i( 602)/  2/,j( 602)/  0/,k( 602)/  7/
      data cf( 603)/0.43980967527168240130625555749137D+10/
      data i( 603)/  0/,j( 603)/  7/,k( 603)/  2/
      data cf( 604)/0.43980967527168240130625555749137D+10/
      data i( 604)/  0/,j( 604)/  2/,k( 604)/  7/
      data cf( 605)/0.17300592196277065093938164993648D+10/
      data i( 605)/  8/,j( 605)/  1/,k( 605)/  0/
      data cf( 606)/0.17300592196277065093938164993648D+10/
      data i( 606)/  8/,j( 606)/  0/,k( 606)/  1/
      data cf( 607)/0.17300592196277065093938164993648D+10/
      data i( 607)/  1/,j( 607)/  8/,k( 607)/  0/
      data cf( 608)/0.17300592196277065093938164993648D+10/
      data i( 608)/  1/,j( 608)/  0/,k( 608)/  8/
      data cf( 609)/0.17300592196277065093938164993648D+10/
      data i( 609)/  0/,j( 609)/  8/,k( 609)/  1/
      data cf( 610)/0.17300592196277065093938164993648D+10/
      data i( 610)/  0/,j( 610)/  1/,k( 610)/  8/
      data cf( 611)/0.39624949917209751460252887466697D+10/
      data i( 611)/  4/,j( 611)/  3/,k( 611)/  3/
      data cf( 612)/0.39624949917209751460252887466697D+10/
      data i( 612)/  3/,j( 612)/  4/,k( 612)/  3/
      data cf( 613)/0.39624949917209751460252887466697D+10/
      data i( 613)/  3/,j( 613)/  3/,k( 613)/  4/
      data cf( 614)/0.31387210160247975885100814249403D+10/
      data i( 614)/  4/,j( 614)/  4/,k( 614)/  2/
      data cf( 615)/0.31387210160247975885100814249403D+10/
      data i( 615)/  4/,j( 615)/  2/,k( 615)/  4/
      data cf( 616)/0.31387210160247975885100814249403D+10/
      data i( 616)/  2/,j( 616)/  4/,k( 616)/  4/
      data cf( 617)/0.13736804972412932893959146444437D+10/
      data i( 617)/  5/,j( 617)/  3/,k( 617)/  2/
      data cf( 618)/0.13736804972412932893959146444437D+10/
      data i( 618)/  5/,j( 618)/  2/,k( 618)/  3/
      data cf( 619)/0.13736804972412932893959146444437D+10/
      data i( 619)/  3/,j( 619)/  5/,k( 619)/  2/
      data cf( 620)/0.13736804972412932893959146444437D+10/
      data i( 620)/  3/,j( 620)/  2/,k( 620)/  5/
      data cf( 621)/0.13736804972412932893959146444437D+10/
      data i( 621)/  2/,j( 621)/  5/,k( 621)/  3/
      data cf( 622)/0.13736804972412932893959146444437D+10/
      data i( 622)/  2/,j( 622)/  3/,k( 622)/  5/
      data cf( 623)/0.19755272690490937631413075459707D+10/
      data i( 623)/  5/,j( 623)/  4/,k( 623)/  1/
      data cf( 624)/0.19755272690490937631413075459707D+10/
      data i( 624)/  5/,j( 624)/  1/,k( 624)/  4/
      data cf( 625)/0.19755272690490937631413075459707D+10/
      data i( 625)/  4/,j( 625)/  5/,k( 625)/  1/
      data cf( 626)/0.19755272690490937631413075459707D+10/
      data i( 626)/  4/,j( 626)/  1/,k( 626)/  5/
      data cf( 627)/0.19755272690490937631413075459707D+10/
      data i( 627)/  1/,j( 627)/  5/,k( 627)/  4/
      data cf( 628)/0.19755272690490937631413075459707D+10/
      data i( 628)/  1/,j( 628)/  4/,k( 628)/  5/
      data cf( 629)/-.11975475885900946977845347811030D+10/
      data i( 629)/  5/,j( 629)/  5/,k( 629)/  0/
      data cf( 630)/-.11975475885900946977845347811030D+10/
      data i( 630)/  5/,j( 630)/  0/,k( 630)/  5/
      data cf( 631)/-.11975475885900946977845347811030D+10/
      data i( 631)/  0/,j( 631)/  5/,k( 631)/  5/
      data cf( 632)/-.30286508384703128158070162191972D+10/
      data i( 632)/  6/,j( 632)/  2/,k( 632)/  2/
      data cf( 633)/-.30286508384703128158070162191972D+10/
      data i( 633)/  2/,j( 633)/  6/,k( 633)/  2/
      data cf( 634)/-.30286508384703128158070162191972D+10/
      data i( 634)/  2/,j( 634)/  2/,k( 634)/  6/
      data cf( 635)/-.10478384678289604587319062496813D+10/
      data i( 635)/  6/,j( 635)/  3/,k( 635)/  1/
      data cf( 636)/-.10478384678289604587319062496813D+10/
      data i( 636)/  6/,j( 636)/  1/,k( 636)/  3/
      data cf( 637)/-.10478384678289604587319062496813D+10/
      data i( 637)/  3/,j( 637)/  6/,k( 637)/  1/
      data cf( 638)/-.10478384678289604587319062496813D+10/
      data i( 638)/  3/,j( 638)/  1/,k( 638)/  6/
      data cf( 639)/-.10478384678289604587319062496813D+10/
      data i( 639)/  1/,j( 639)/  6/,k( 639)/  3/
      data cf( 640)/-.10478384678289604587319062496813D+10/
      data i( 640)/  1/,j( 640)/  3/,k( 640)/  6/
      data cf( 641)/-.47190498018383866412873338794201D+10/
      data i( 641)/  6/,j( 641)/  4/,k( 641)/  0/
      data cf( 642)/-.47190498018383866412873338794201D+10/
      data i( 642)/  6/,j( 642)/  0/,k( 642)/  4/
      data cf( 643)/-.47190498018383866412873338794201D+10/
      data i( 643)/  4/,j( 643)/  6/,k( 643)/  0/
      data cf( 644)/-.47190498018383866412873338794201D+10/
      data i( 644)/  4/,j( 644)/  0/,k( 644)/  6/
      data cf( 645)/-.47190498018383866412873338794201D+10/
      data i( 645)/  0/,j( 645)/  6/,k( 645)/  4/
      data cf( 646)/-.47190498018383866412873338794201D+10/
      data i( 646)/  0/,j( 646)/  4/,k( 646)/  6/
      data cf( 647)/-.18277130244813748322882316263908D+10/
      data i( 647)/  7/,j( 647)/  2/,k( 647)/  1/
      data cf( 648)/-.18277130244813748322882316263908D+10/
      data i( 648)/  7/,j( 648)/  1/,k( 648)/  2/
      data cf( 649)/-.18277130244813748322882316263908D+10/
      data i( 649)/  2/,j( 649)/  7/,k( 649)/  1/
      data cf( 650)/-.18277130244813748322882316263908D+10/
      data i( 650)/  2/,j( 650)/  1/,k( 650)/  7/
      data cf( 651)/-.18277130244813748322882316263908D+10/
      data i( 651)/  1/,j( 651)/  7/,k( 651)/  2/
      data cf( 652)/-.18277130244813748322882316263908D+10/
      data i( 652)/  1/,j( 652)/  2/,k( 652)/  7/
      data cf( 653)/-.10586414878593898471882201640355D+11/
      data i( 653)/  7/,j( 653)/  3/,k( 653)/  0/
      data cf( 654)/-.10586414878593898471882201640355D+11/
      data i( 654)/  7/,j( 654)/  0/,k( 654)/  3/
      data cf( 655)/-.10586414878593898471882201640355D+11/
      data i( 655)/  3/,j( 655)/  7/,k( 655)/  0/
      data cf( 656)/-.10586414878593898471882201640355D+11/
      data i( 656)/  3/,j( 656)/  0/,k( 656)/  7/
      data cf( 657)/-.10586414878593898471882201640355D+11/
      data i( 657)/  0/,j( 657)/  7/,k( 657)/  3/
      data cf( 658)/-.10586414878593898471882201640355D+11/
      data i( 658)/  0/,j( 658)/  3/,k( 658)/  7/
      data cf( 659)/-.14437586018424052775722595373364D+10/
      data i( 659)/  8/,j( 659)/  1/,k( 659)/  1/
      data cf( 660)/-.14437586018424052775722595373364D+10/
      data i( 660)/  1/,j( 660)/  8/,k( 660)/  1/
      data cf( 661)/-.14437586018424052775722595373364D+10/
      data i( 661)/  1/,j( 661)/  1/,k( 661)/  8/
      data cf( 662)/-.66953825528831580677846933345487D+10/
      data i( 662)/  8/,j( 662)/  2/,k( 662)/  0/
      data cf( 663)/-.66953825528831580677846933345487D+10/
      data i( 663)/  8/,j( 663)/  0/,k( 663)/  2/
      data cf( 664)/-.66953825528831580677846933345487D+10/
      data i( 664)/  2/,j( 664)/  8/,k( 664)/  0/
      data cf( 665)/-.66953825528831580677846933345487D+10/
      data i( 665)/  2/,j( 665)/  0/,k( 665)/  8/
      data cf( 666)/-.66953825528831580677846933345487D+10/
      data i( 666)/  0/,j( 666)/  8/,k( 666)/  2/
      data cf( 667)/-.66953825528831580677846933345487D+10/
      data i( 667)/  0/,j( 667)/  2/,k( 667)/  8/
      data cf( 668)/-.27526285158491160652428820686674D+10/
      data i( 668)/  9/,j( 668)/  1/,k( 668)/  0/
      data cf( 669)/-.27526285158491160652428820686674D+10/
      data i( 669)/  9/,j( 669)/  0/,k( 669)/  1/
      data cf( 670)/-.27526285158491160652428820686674D+10/
      data i( 670)/  1/,j( 670)/  9/,k( 670)/  0/
      data cf( 671)/-.27526285158491160652428820686674D+10/
      data i( 671)/  1/,j( 671)/  0/,k( 671)/  9/
      data cf( 672)/-.27526285158491160652428820686674D+10/
      data i( 672)/  0/,j( 672)/  9/,k( 672)/  1/
      data cf( 673)/-.27526285158491160652428820686674D+10/
      data i( 673)/  0/,j( 673)/  1/,k( 673)/  9/
      data cf( 674)/-.54622169873442354785351622699059D+10/
      data i( 674)/  4/,j( 674)/  4/,k( 674)/  3/
      data cf( 675)/-.54622169873442354785351622699059D+10/
      data i( 675)/  4/,j( 675)/  3/,k( 675)/  4/
      data cf( 676)/-.54622169873442354785351622699059D+10/
      data i( 676)/  3/,j( 676)/  4/,k( 676)/  4/
      data cf( 677)/-.26776092393833553078305077576594D+10/
      data i( 677)/  5/,j( 677)/  3/,k( 677)/  3/
      data cf( 678)/-.26776092393833553078305077576594D+10/
      data i( 678)/  3/,j( 678)/  5/,k( 678)/  3/
      data cf( 679)/-.26776092393833553078305077576594D+10/
      data i( 679)/  3/,j( 679)/  3/,k( 679)/  5/
      data cf( 680)/-.25027313239538314887535389118175D+10/
      data i( 680)/  5/,j( 680)/  4/,k( 680)/  2/
      data cf( 681)/-.25027313239538314887535389118175D+10/
      data i( 681)/  5/,j( 681)/  2/,k( 681)/  4/
      data cf( 682)/-.25027313239538314887535389118175D+10/
      data i( 682)/  4/,j( 682)/  5/,k( 682)/  2/
      data cf( 683)/-.25027313239538314887535389118175D+10/
      data i( 683)/  4/,j( 683)/  2/,k( 683)/  5/
      data cf( 684)/-.25027313239538314887535389118175D+10/
      data i( 684)/  2/,j( 684)/  5/,k( 684)/  4/
      data cf( 685)/-.25027313239538314887535389118175D+10/
      data i( 685)/  2/,j( 685)/  4/,k( 685)/  5/
      data cf( 686)/-.35825329820985049510213190281559D+10/
      data i( 686)/  5/,j( 686)/  5/,k( 686)/  1/
      data cf( 687)/-.35825329820985049510213190281559D+10/
      data i( 687)/  5/,j( 687)/  1/,k( 687)/  5/
      data cf( 688)/-.35825329820985049510213190281559D+10/
      data i( 688)/  1/,j( 688)/  5/,k( 688)/  5/
      data cf( 689)/-.20699746105325597018605728510199D+09/
      data i( 689)/  6/,j( 689)/  3/,k( 689)/  2/
      data cf( 690)/-.20699746105325597018605728510199D+09/
      data i( 690)/  6/,j( 690)/  2/,k( 690)/  3/
      data cf( 691)/-.20699746105325597018605728510199D+09/
      data i( 691)/  3/,j( 691)/  6/,k( 691)/  2/
      data cf( 692)/-.20699746105325597018605728510199D+09/
      data i( 692)/  3/,j( 692)/  2/,k( 692)/  6/
      data cf( 693)/-.20699746105325597018605728510199D+09/
      data i( 693)/  2/,j( 693)/  6/,k( 693)/  3/
      data cf( 694)/-.20699746105325597018605728510199D+09/
      data i( 694)/  2/,j( 694)/  3/,k( 694)/  6/
      data cf( 695)/-.63645814877236734928460936792064D+09/
      data i( 695)/  6/,j( 695)/  4/,k( 695)/  1/
      data cf( 696)/-.63645814877236734928460936792064D+09/
      data i( 696)/  6/,j( 696)/  1/,k( 696)/  4/
      data cf( 697)/-.63645814877236734928460936792064D+09/
      data i( 697)/  4/,j( 697)/  6/,k( 697)/  1/
      data cf( 698)/-.63645814877236734928460936792064D+09/
      data i( 698)/  4/,j( 698)/  1/,k( 698)/  6/
      data cf( 699)/-.63645814877236734928460936792064D+09/
      data i( 699)/  1/,j( 699)/  6/,k( 699)/  4/
      data cf( 700)/-.63645814877236734928460936792064D+09/
      data i( 700)/  1/,j( 700)/  4/,k( 700)/  6/
      data cf( 701)/0.21077185855557813563595439152307D+10/
      data i( 701)/  6/,j( 701)/  5/,k( 701)/  0/
      data cf( 702)/0.21077185855557813563595439152307D+10/
      data i( 702)/  6/,j( 702)/  0/,k( 702)/  5/
      data cf( 703)/0.21077185855557813563595439152307D+10/
      data i( 703)/  5/,j( 703)/  6/,k( 703)/  0/
      data cf( 704)/0.21077185855557813563595439152307D+10/
      data i( 704)/  5/,j( 704)/  0/,k( 704)/  6/
      data cf( 705)/0.21077185855557813563595439152307D+10/
      data i( 705)/  0/,j( 705)/  6/,k( 705)/  5/
      data cf( 706)/0.21077185855557813563595439152307D+10/
      data i( 706)/  0/,j( 706)/  5/,k( 706)/  6/
      data cf( 707)/0.66218596243603212224789071658823D+10/
      data i( 707)/  7/,j( 707)/  2/,k( 707)/  2/
      data cf( 708)/0.66218596243603212224789071658823D+10/
      data i( 708)/  2/,j( 708)/  7/,k( 708)/  2/
      data cf( 709)/0.66218596243603212224789071658823D+10/
      data i( 709)/  2/,j( 709)/  2/,k( 709)/  7/
      data cf( 710)/0.21985144780462238336440883631064D+10/
      data i( 710)/  7/,j( 710)/  3/,k( 710)/  1/
      data cf( 711)/0.21985144780462238336440883631064D+10/
      data i( 711)/  7/,j( 711)/  1/,k( 711)/  3/
      data cf( 712)/0.21985144780462238336440883631064D+10/
      data i( 712)/  3/,j( 712)/  7/,k( 712)/  1/
      data cf( 713)/0.21985144780462238336440883631064D+10/
      data i( 713)/  3/,j( 713)/  1/,k( 713)/  7/
      data cf( 714)/0.21985144780462238336440883631064D+10/
      data i( 714)/  1/,j( 714)/  7/,k( 714)/  3/
      data cf( 715)/0.21985144780462238336440883631064D+10/
      data i( 715)/  1/,j( 715)/  3/,k( 715)/  7/
      data cf( 716)/0.77726479245131860732279084649047D+10/
      data i( 716)/  7/,j( 716)/  4/,k( 716)/  0/
      data cf( 717)/0.77726479245131860732279084649047D+10/
      data i( 717)/  7/,j( 717)/  0/,k( 717)/  4/
      data cf( 718)/0.77726479245131860732279084649047D+10/
      data i( 718)/  4/,j( 718)/  7/,k( 718)/  0/
      data cf( 719)/0.77726479245131860732279084649047D+10/
      data i( 719)/  4/,j( 719)/  0/,k( 719)/  7/
      data cf( 720)/0.77726479245131860732279084649047D+10/
      data i( 720)/  0/,j( 720)/  7/,k( 720)/  4/
      data cf( 721)/0.77726479245131860732279084649047D+10/
      data i( 721)/  0/,j( 721)/  4/,k( 721)/  7/
      data cf( 722)/0.91003855895965983134798914571292D+09/
      data i( 722)/  8/,j( 722)/  2/,k( 722)/  1/
      data cf( 723)/0.91003855895965983134798914571292D+09/
      data i( 723)/  8/,j( 723)/  1/,k( 723)/  2/
      data cf( 724)/0.91003855895965983134798914571292D+09/
      data i( 724)/  2/,j( 724)/  8/,k( 724)/  1/
      data cf( 725)/0.91003855895965983134798914571292D+09/
      data i( 725)/  2/,j( 725)/  1/,k( 725)/  8/
      data cf( 726)/0.91003855895965983134798914571292D+09/
      data i( 726)/  1/,j( 726)/  8/,k( 726)/  2/
      data cf( 727)/0.91003855895965983134798914571292D+09/
      data i( 727)/  1/,j( 727)/  2/,k( 727)/  8/
      data cf( 728)/0.13123652898761687294709445500750D+11/
      data i( 728)/  8/,j( 728)/  3/,k( 728)/  0/
      data cf( 729)/0.13123652898761687294709445500750D+11/
      data i( 729)/  8/,j( 729)/  0/,k( 729)/  3/
      data cf( 730)/0.13123652898761687294709445500750D+11/
      data i( 730)/  3/,j( 730)/  8/,k( 730)/  0/
      data cf( 731)/0.13123652898761687294709445500750D+11/
      data i( 731)/  3/,j( 731)/  0/,k( 731)/  8/
      data cf( 732)/0.13123652898761687294709445500750D+11/
      data i( 732)/  0/,j( 732)/  8/,k( 732)/  3/
      data cf( 733)/0.13123652898761687294709445500750D+11/
      data i( 733)/  0/,j( 733)/  3/,k( 733)/  8/
      data cf( 734)/0.21112770692320467982413860304060D+10/
      data i( 734)/  9/,j( 734)/  1/,k( 734)/  1/
      data cf( 735)/0.21112770692320467982413860304060D+10/
      data i( 735)/  1/,j( 735)/  9/,k( 735)/  1/
      data cf( 736)/0.21112770692320467982413860304060D+10/
      data i( 736)/  1/,j( 736)/  1/,k( 736)/  9/
      data cf( 737)/0.56373674079743988116205227133852D+10/
      data i( 737)/  9/,j( 737)/  2/,k( 737)/  0/
      data cf( 738)/0.56373674079743988116205227133852D+10/
      data i( 738)/  9/,j( 738)/  0/,k( 738)/  2/
      data cf( 739)/0.56373674079743988116205227133852D+10/
      data i( 739)/  2/,j( 739)/  9/,k( 739)/  0/
      data cf( 740)/0.56373674079743988116205227133852D+10/
      data i( 740)/  2/,j( 740)/  0/,k( 740)/  9/
      data cf( 741)/0.56373674079743988116205227133852D+10/
      data i( 741)/  0/,j( 741)/  9/,k( 741)/  2/
      data cf( 742)/0.56373674079743988116205227133852D+10/
      data i( 742)/  0/,j( 742)/  2/,k( 742)/  9/
      data cf( 743)/0.30240068174204469101021576444005D+10/
      data i( 743)/ 10/,j( 743)/  1/,k( 743)/  0/
      data cf( 744)/0.30240068174204469101021576444005D+10/
      data i( 744)/ 10/,j( 744)/  0/,k( 744)/  1/
      data cf( 745)/0.30240068174204469101021576444005D+10/
      data i( 745)/  1/,j( 745)/ 10/,k( 745)/  0/
      data cf( 746)/0.30240068174204469101021576444005D+10/
      data i( 746)/  1/,j( 746)/  0/,k( 746)/ 10/
      data cf( 747)/0.30240068174204469101021576444005D+10/
      data i( 747)/  0/,j( 747)/ 10/,k( 747)/  1/
      data cf( 748)/0.30240068174204469101021576444005D+10/
      data i( 748)/  0/,j( 748)/  1/,k( 748)/ 10/
      data cf( 749)/0.93949621121465522200511631789753D+10/
      data i( 749)/  4/,j( 749)/  4/,k( 749)/  4/
      data cf( 750)/0.51753639415622911326596326711994D+09/
      data i( 750)/  5/,j( 750)/  4/,k( 750)/  3/
      data cf( 751)/0.51753639415622911326596326711994D+09/
      data i( 751)/  5/,j( 751)/  3/,k( 751)/  4/
      data cf( 752)/0.51753639415622911326596326711994D+09/
      data i( 752)/  4/,j( 752)/  5/,k( 752)/  3/
      data cf( 753)/0.51753639415622911326596326711994D+09/
      data i( 753)/  4/,j( 753)/  3/,k( 753)/  5/
      data cf( 754)/0.51753639415622911326596326711994D+09/
      data i( 754)/  3/,j( 754)/  5/,k( 754)/  4/
      data cf( 755)/0.51753639415622911326596326711994D+09/
      data i( 755)/  3/,j( 755)/  4/,k( 755)/  5/
      data cf( 756)/0.89699684967283299869136090298643D+09/
      data i( 756)/  5/,j( 756)/  5/,k( 756)/  2/
      data cf( 757)/0.89699684967283299869136090298643D+09/
      data i( 757)/  5/,j( 757)/  2/,k( 757)/  5/
      data cf( 758)/0.89699684967283299869136090298643D+09/
      data i( 758)/  2/,j( 758)/  5/,k( 758)/  5/
      data cf( 759)/0.17914861193395949910220038695921D+10/
      data i( 759)/  6/,j( 759)/  3/,k( 759)/  3/
      data cf( 760)/0.17914861193395949910220038695921D+10/
      data i( 760)/  3/,j( 760)/  6/,k( 760)/  3/
      data cf( 761)/0.17914861193395949910220038695921D+10/
      data i( 761)/  3/,j( 761)/  3/,k( 761)/  6/
      data cf( 762)/0.14213281255493412528756900050538D+10/
      data i( 762)/  6/,j( 762)/  4/,k( 762)/  2/
      data cf( 763)/0.14213281255493412528756900050538D+10/
      data i( 763)/  6/,j( 763)/  2/,k( 763)/  4/
      data cf( 764)/0.14213281255493412528756900050538D+10/
      data i( 764)/  4/,j( 764)/  6/,k( 764)/  2/
      data cf( 765)/0.14213281255493412528756900050538D+10/
      data i( 765)/  4/,j( 765)/  2/,k( 765)/  6/
      data cf( 766)/0.14213281255493412528756900050538D+10/
      data i( 766)/  2/,j( 766)/  6/,k( 766)/  4/
      data cf( 767)/0.14213281255493412528756900050538D+10/
      data i( 767)/  2/,j( 767)/  4/,k( 767)/  6/
      data cf( 768)/0.16930216484752070758385594028233D+10/
      data i( 768)/  6/,j( 768)/  5/,k( 768)/  1/
      data cf( 769)/0.16930216484752070758385594028233D+10/
      data i( 769)/  6/,j( 769)/  1/,k( 769)/  5/
      data cf( 770)/0.16930216484752070758385594028233D+10/
      data i( 770)/  5/,j( 770)/  6/,k( 770)/  1/
      data cf( 771)/0.16930216484752070758385594028233D+10/
      data i( 771)/  5/,j( 771)/  1/,k( 771)/  6/
      data cf( 772)/0.16930216484752070758385594028233D+10/
      data i( 772)/  1/,j( 772)/  6/,k( 772)/  5/
      data cf( 773)/0.16930216484752070758385594028233D+10/
      data i( 773)/  1/,j( 773)/  5/,k( 773)/  6/
      data cf( 774)/0.38284581810638115849523722814251D+10/
      data i( 774)/  6/,j( 774)/  6/,k( 774)/  0/
      data cf( 775)/0.38284581810638115849523722814251D+10/
      data i( 775)/  6/,j( 775)/  0/,k( 775)/  6/
      data cf( 776)/0.38284581810638115849523722814251D+10/
      data i( 776)/  0/,j( 776)/  6/,k( 776)/  6/
      data cf( 777)/-.83174167960952663506290990015527D+09/
      data i( 777)/  7/,j( 777)/  3/,k( 777)/  2/
      data cf( 778)/-.83174167960952663506290990015527D+09/
      data i( 778)/  7/,j( 778)/  2/,k( 778)/  3/
      data cf( 779)/-.83174167960952663506290990015527D+09/
      data i( 779)/  3/,j( 779)/  7/,k( 779)/  2/
      data cf( 780)/-.83174167960952663506290990015527D+09/
      data i( 780)/  3/,j( 780)/  2/,k( 780)/  7/
      data cf( 781)/-.83174167960952663506290990015527D+09/
      data i( 781)/  2/,j( 781)/  7/,k( 781)/  3/
      data cf( 782)/-.83174167960952663506290990015527D+09/
      data i( 782)/  2/,j( 782)/  3/,k( 782)/  7/
      data cf( 783)/-.65153198954535109426516426428836D+09/
      data i( 783)/  7/,j( 783)/  4/,k( 783)/  1/
      data cf( 784)/-.65153198954535109426516426428836D+09/
      data i( 784)/  7/,j( 784)/  1/,k( 784)/  4/
      data cf( 785)/-.65153198954535109426516426428836D+09/
      data i( 785)/  4/,j( 785)/  7/,k( 785)/  1/
      data cf( 786)/-.65153198954535109426516426428836D+09/
      data i( 786)/  4/,j( 786)/  1/,k( 786)/  7/
      data cf( 787)/-.65153198954535109426516426428836D+09/
      data i( 787)/  1/,j( 787)/  7/,k( 787)/  4/
      data cf( 788)/-.65153198954535109426516426428836D+09/
      data i( 788)/  1/,j( 788)/  4/,k( 788)/  7/
      data cf( 789)/-.56600989901129829667181445797843D+10/
      data i( 789)/  7/,j( 789)/  5/,k( 789)/  0/
      data cf( 790)/-.56600989901129829667181445797843D+10/
      data i( 790)/  7/,j( 790)/  0/,k( 790)/  5/
      data cf( 791)/-.56600989901129829667181445797843D+10/
      data i( 791)/  5/,j( 791)/  7/,k( 791)/  0/
      data cf( 792)/-.56600989901129829667181445797843D+10/
      data i( 792)/  5/,j( 792)/  0/,k( 792)/  7/
      data cf( 793)/-.56600989901129829667181445797843D+10/
      data i( 793)/  0/,j( 793)/  7/,k( 793)/  5/
      data cf( 794)/-.56600989901129829667181445797843D+10/
      data i( 794)/  0/,j( 794)/  5/,k( 794)/  7/
      data cf( 795)/-.58739635891774091103883813835853D+10/
      data i( 795)/  8/,j( 795)/  2/,k( 795)/  2/
      data cf( 796)/-.58739635891774091103883813835853D+10/
      data i( 796)/  2/,j( 796)/  8/,k( 796)/  2/
      data cf( 797)/-.58739635891774091103883813835853D+10/
      data i( 797)/  2/,j( 797)/  2/,k( 797)/  8/
      data cf( 798)/-.13611215368121367522485516199010D+10/
      data i( 798)/  8/,j( 798)/  3/,k( 798)/  1/
      data cf( 799)/-.13611215368121367522485516199010D+10/
      data i( 799)/  8/,j( 799)/  1/,k( 799)/  3/
      data cf( 800)/-.13611215368121367522485516199010D+10/
      data i( 800)/  3/,j( 800)/  8/,k( 800)/  1/
      data cf( 801)/-.13611215368121367522485516199010D+10/
      data i( 801)/  3/,j( 801)/  1/,k( 801)/  8/
      data cf( 802)/-.13611215368121367522485516199010D+10/
      data i( 802)/  1/,j( 802)/  8/,k( 802)/  3/
      data cf( 803)/-.13611215368121367522485516199010D+10/
      data i( 803)/  1/,j( 803)/  3/,k( 803)/  8/
      data cf( 804)/-.30857656790566272814932424758027D+10/
      data i( 804)/  8/,j( 804)/  4/,k( 804)/  0/
      data cf( 805)/-.30857656790566272814932424758027D+10/
      data i( 805)/  8/,j( 805)/  0/,k( 805)/  4/
      data cf( 806)/-.30857656790566272814932424758027D+10/
      data i( 806)/  4/,j( 806)/  8/,k( 806)/  0/
      data cf( 807)/-.30857656790566272814932424758027D+10/
      data i( 807)/  4/,j( 807)/  0/,k( 807)/  8/
      data cf( 808)/-.30857656790566272814932424758027D+10/
      data i( 808)/  0/,j( 808)/  8/,k( 808)/  4/
      data cf( 809)/-.30857656790566272814932424758027D+10/
      data i( 809)/  0/,j( 809)/  4/,k( 809)/  8/
      data cf( 810)/0.10031646798952093065015771245307D+10/
      data i( 810)/  9/,j( 810)/  2/,k( 810)/  1/
      data cf( 811)/0.10031646798952093065015771245307D+10/
      data i( 811)/  9/,j( 811)/  1/,k( 811)/  2/
      data cf( 812)/0.10031646798952093065015771245307D+10/
      data i( 812)/  2/,j( 812)/  9/,k( 812)/  1/
      data cf( 813)/0.10031646798952093065015771245307D+10/
      data i( 813)/  2/,j( 813)/  1/,k( 813)/  9/
      data cf( 814)/0.10031646798952093065015771245307D+10/
      data i( 814)/  1/,j( 814)/  9/,k( 814)/  2/
      data cf( 815)/0.10031646798952093065015771245307D+10/
      data i( 815)/  1/,j( 815)/  2/,k( 815)/  9/
      data cf( 816)/-.81733676007987249399495501640013D+10/
      data i( 816)/  9/,j( 816)/  3/,k( 816)/  0/
      data cf( 817)/-.81733676007987249399495501640013D+10/
      data i( 817)/  9/,j( 817)/  0/,k( 817)/  3/
      data cf( 818)/-.81733676007987249399495501640013D+10/
      data i( 818)/  3/,j( 818)/  9/,k( 818)/  0/
      data cf( 819)/-.81733676007987249399495501640013D+10/
      data i( 819)/  3/,j( 819)/  0/,k( 819)/  9/
      data cf( 820)/-.81733676007987249399495501640013D+10/
      data i( 820)/  0/,j( 820)/  9/,k( 820)/  3/
      data cf( 821)/-.81733676007987249399495501640013D+10/
      data i( 821)/  0/,j( 821)/  3/,k( 821)/  9/
      data cf( 822)/-.20033779643019716849108696833597D+10/
      data i( 822)/ 10/,j( 822)/  1/,k( 822)/  1/
      data cf( 823)/-.20033779643019716849108696833597D+10/
      data i( 823)/  1/,j( 823)/ 10/,k( 823)/  1/
      data cf( 824)/-.20033779643019716849108696833597D+10/
      data i( 824)/  1/,j( 824)/  1/,k( 824)/ 10/
      data cf( 825)/-.15425349853929030945144958024108D+10/
      data i( 825)/ 10/,j( 825)/  2/,k( 825)/  0/
      data cf( 826)/-.15425349853929030945144958024108D+10/
      data i( 826)/ 10/,j( 826)/  0/,k( 826)/  2/
      data cf( 827)/-.15425349853929030945144958024108D+10/
      data i( 827)/  2/,j( 827)/ 10/,k( 827)/  0/
      data cf( 828)/-.15425349853929030945144958024108D+10/
      data i( 828)/  2/,j( 828)/  0/,k( 828)/ 10/
      data cf( 829)/-.15425349853929030945144958024108D+10/
      data i( 829)/  0/,j( 829)/ 10/,k( 829)/  2/
      data cf( 830)/-.15425349853929030945144958024108D+10/
      data i( 830)/  0/,j( 830)/  2/,k( 830)/ 10/
      data cf( 831)/-.17526038872452903568345704512940D+10/
      data i( 831)/ 11/,j( 831)/  1/,k( 831)/  0/
      data cf( 832)/-.17526038872452903568345704512940D+10/
      data i( 832)/ 11/,j( 832)/  0/,k( 832)/  1/
      data cf( 833)/-.17526038872452903568345704512940D+10/
      data i( 833)/  1/,j( 833)/ 11/,k( 833)/  0/
      data cf( 834)/-.17526038872452903568345704512940D+10/
      data i( 834)/  1/,j( 834)/  0/,k( 834)/ 11/
      data cf( 835)/-.17526038872452903568345704512940D+10/
      data i( 835)/  0/,j( 835)/ 11/,k( 835)/  1/
      data cf( 836)/-.17526038872452903568345704512940D+10/
      data i( 836)/  0/,j( 836)/  1/,k( 836)/ 11/
      vex1 = 0.89575297231198267710764684084340D+00
      vex2 = 0.15257487585819803915026193722483D+01
      f12(0)=1.d0
      f13(0)=1.d0
      f23(0)=1.d0
      bux12=x*exp(-vex1*x)
      bux13=y*exp(-vex1*y)
      bux23=z*exp(-vex1*z)
      g12(0)=1.d0
      g13(0)=1.d0
      g23(0)=1.d0
      cux12=x*exp(-vex2*x)
      cux13=y*exp(-vex2*y)
      cux23=z*exp(-vex2*z)
      do 1 l=1, 11
         f12(l)=f12(l-1)*bux12
         f13(l)=f13(l-1)*bux13
         f23(l)=f23(l-1)*bux23
         g12(l)=g12(l-1)*cux12
         g13(l)=g13(l-1)*cux13
         g23(l)=g23(l-1)*cux23
1     continue
      ener = 0.d0
      do 2 l=1, 418
         aux=f12(i(l))*f13(j(l))*f23(k(l)) 
         ener=ener+cf(l)*aux               
    2 continue                           
      do 3 l= 419, 836
         aux=g12(i(l))*g13(j(l))*g23(k(l)) 
         ener=ener+cf(l)*aux               
    3 continue                           
      return
      end
