#!/bin/bash
#
# Script for listing the Band Origins of multiple calculations
#

#daTALOC="$1"

#cd $daTALOC
if [ -z $1 ] || [ -d $1 ] ; then
  ouTPUTFILES=(`find ./ -maxdepth 1 -name "*.out"`)
  if [ ${#ouTPUTFILES[@]} -gt 0 ] ; then
    echo "Comparing" ${#ouTPUTFILES[@]} "output files"
    for i in *.out ; do
      fiLE="${i%.*}"
      fiLE2=$(basename "$fiLE")
      fiLE3=${fiLE2##*_}
      awk '/eigenvalues in hartrees/ {for(j=1; j<=2; j++) {getline; print}}' ${i} >> "$fiLE"tempa
      awk '/Lowest/ {for(k=1; k<=2; k++) {getline; print}}' ${i} >> "$fiLE"tempa
      awk '/Band origins in/ {for(l=1; l<=2; l++) {getline; print}}' ${i} >> "$fiLE"tempa
#     exit
#     sed -e '1,/Band origins in wavenumbers/d' ${i} > "$fiLE"temp
#     if [[ $fiLE3 = lob ]] ; then
#        sed -i '/Line/,$d' "$fiLE"temp
#     fi
#     sed -i '/^$/d' "$fiLE"temp
      awk '{for (j=1;j<=NF;j++) print $j}' "$fiLE"tempa >> "$fiLE"_BO
#     awk '{for (j=1;j<=NF;j++) print $j}' "$fiLE"tempa >> "$fiLE"tempb
#     sed -e :a -e '$d;N;2,4ba' -e 'P;D' "$fiLE"tempb > "$fiLE"_BO
      sed -i '1i '"#"$fiLE2'' "$fiLE"_BO
    done
#   touch $daTALOC/tempc
    touch tempc
    for i in *_BO ; do
      pr -mts tempc ${i} > tempd
      mv tempd tempc
    done
    column -t < tempc > compare_all
    echo "Output written to: " $daTALOC"compare_all"
    rm *temp*
    exit
  else
    echo "There are no output files to analyse here..."
    echo " errrr..."
    echo "Quitting."
    exit
  fi
else
  echo "I think this may be a type of radiation we've never encountered before..."
  exit
fi


