#!/bin/bash
#
# Script for listing the Band Origins of multiple calculations
#

daTALOC="$1"

cd $daTALOC
if [ -z $1 ] || [ -d $1 ] ; then
  ouTPUTFILES=(`find ./ -maxdepth 1 -name "*.out"`)
  if [ ${#ouTPUTFILES[@]} -gt 0 ] ; then
    echo "Comparing" ${#ouTPUTFILES[@]} "output files"
    for i in *.out ; do
      fiLE="${i%.*}"
      fiLE2=$(basename "$fiLE")
      fiLE3=${fiLE2##*_}
      sed -n '/eigenvalues in hartrees/{n;p;n;p}' ${i} >> "$fiLE"tempa
      sed -n '/Lowest/{n;p;n;p}' ${i} >> "$fiLE"tempa
      sed -n '/Band origins in/{n;p;n;p}' ${i} >> "$fiLE"tempa 
      sed -n '/secs CPU time/p' ${i} >> "$fiLE"tempa
#     sed -e 's/      /\n /g' "$fiLE"tempa >> "$fiLE"tempb
#     sed -e 's/     /\n /g' "$fiLE"tempb  >> "$fiLE"tempX 
#     sed -e 's/ /\n /g' "$fiLE"tempX >> "$fiLE"tempY
      awk '{for (j=1;j<=NF;j++) print $j}' "$fiLE"tempa >> "$fiLE"tempb
      sed -e :a -e '$d;N;2,3ba' -e 'P;D' "$fiLE"tempb > "$fiLE"_BO_eig_tim
#     sed -e :a -e '$d;N;2,3ba' -e 'P;D' "$fiLE"tempa > "$fiLE"_BO_eig_tim
      sed -i '1i '"#"$fiLE2'' "$fiLE"_BO_eig_tim
#     cp "$fiLE"tempa "$fiLE"B4
#     cp "$fiLE"tempY "$fiLE"4B
    done
    touch $daTALOC/tempc
    touch tempc
    for i in *_BO_eig_tim ; do
      pr -mts tempc ${i} > tempd
      mv tempd tempc
    done
    column -t < tempc > compare_eig_all
    echo "Output written to: " $daTALOC"compare_eig_all"
    rm *temp*
    exit
  else
    echo "There are no output files to analyse here..."
    echo " errrr..."
    echo "Quitting."
    exit
  fi
else
  echo "I think this may be a type of radiation we've never encountered before..."
  exit
fi


