#!/bin/bash
#
# Script for comparing the Band Origins of two calculations
#

fiLE1="$1"
fiLE2="$2"
fiLE1a=$(basename "$fiLE1")
fiLE2a=$(basename "$fiLE2")
fiLE1b="${fiLE1a%%.*}"
fiLE2b="${fiLE2a%%.*}"
fiLE1x="${fiLE1b##*_}"
fiLE2x="${fiLE2b##*_}"
fiLE1c="$fiLE1b"_BO
fiLE2c="$fiLE2b"_BO

sed -e '1,/Band origins in wavenumbers/d' $fiLE1 > temp1
sed -e '1,/Band origins in wavenumbers/d' $fiLE2 > temp2
if [[ $fiLE1x = lob ]] ; then
  sed -i '/Line/,$d' temp1
fi
if [[ $fiLE2x = lob ]] ; then
  sed -i '/Line/,$d' temp2
fi
sed -i '/^$/d' temp1
sed -i '/^$/d' temp2
awk '{for (i=1;i<=NF;i++) print $i}' temp1 >> temp1b
awk '{for (i=1;i<=NF;i++) print $i}' temp2 >> temp2b
sed -e :a -e '$d;N;2,4ba' -e 'P;D' temp1b > "$fiLE1c"
sed -e :a -e '$d;N;2,4ba' -e 'P;D' temp2b > "$fiLE2c"
paste "$fiLE1c" "$fiLE2c" | column -t >> compared1
awk 'BEGIN{ OFS = "\t" } { $3 = $1 - $2 } 1' compared1 > compared2
sed -i '1i '"#"$fiLE1b'\t '$fiLE2b'\t diff' compared2
column -t < compared2 > "$fiLE1b"_"$fiLE2b"_compared 
echo "Output written to: " $fiLE1b"_"$fiLE2b"_compared"
rm temp*
rm compared*


