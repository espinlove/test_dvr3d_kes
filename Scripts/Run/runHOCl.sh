#!/bin/bash

# LOBCODE_DIR - The top directory where the code is located.
#               This must either be included in this script or can be added
#               to the .bashrc as
# export LOBCODE_DIR=/home/NAME/codetopdir
#               and sourced
# fiLE - This is the base name of the .job files. Helps ensure tidier output
#        names.
#For UCL computers
# ruNLOC     - this is the location that calculation is called from
# hoME       - the home/NAME of the user
# myHOME     - just the NAME of the user
# scR        - location of the scratch space
# mySCR      - a directory in the scratch space which will be the scratch/NAME
# caLCLOC    - the directory containing the input file/s
# caLCLOCSCR - the caLCLOC directory being created on the scratch space 
#              ie scratch/NAME/caLCLOC
ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

if [ -z $1 ] ; then
  echo " "
  echo "Need at least 1 argument. -h for help"
  echo " "
  exit
elif [ $1 = "-h" ]; then
  echo " "
  echo "Runscript Help"
  echo "./SCRIPT.sh [RESULTSDIR] [COMPILE?] [SELECTCODE]"
  echo " "
  echo "RESULTSDIR - the directory in which the "
  echo "             calculation will be run "
  echo "COMPILE? can be: -c or -call     Compile all "
  echo "                 -clob or -cnew  Compile new source " 
  echo "                 -cold           Compile old source "
  echo "                 -cori           Compile original dvr3d "
  echo "                 -dc             Don't compile  "
  echo "SELECTCODE - If -dc (Don't compile) is selected  "
  echo "                 -all          Run all"
  echo "                 -lob or -new  Run new source " 
  echo "                 -old          Run old source "
  echo "                 -ori          Run original dvr3d "
  echo " "
  exit
fi

# SETTING LOCATION OF CALCULATION AND THE TARGET RUN DIR ON SCRATCH *********** 
caLCLOCA="$1"
caLCLOC=${caLCLOCA%/} 
echo $caLCLOC
coMPILE="$2"
caLCLOCSCR="$mySCR/$caLCLOC"
cd $caLCLOC
# TIDYING *********************************************************************
rm *.x
rm *.out
rm *.mod
rm fort.*
# COMPILE CODES AND MOVE TO CALC DIR ******************************************
cd $LOBCODE_DIR/Scripts/Compile
if [ $2 = "-c" ] || [ $2 = "-call" ] ; then
  caLCTYP=1
  echo "Compiling new Lobatto version...."
  bash compileHOClLob.sh
  echo "... Compiled"
  echo "Compiling old code version...."
  echo "This is used for spherical and morse!"
  bash compileHOClOldVer.sh
  echo "... Compiled"
  echo "Compiling original code version...."
  bash compileHOCldvr3dmaster.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_ori.x $ruNLOC/$caLCLOC
elif [ $2 = "-clob" ] || [ $2 = "-cnew" ] ; then
  caLCTYP=2
  echo "Compiling new Lobatto version...."
  bash compileHOClLob.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
elif [ $2 = "-cold" ] ; then
  caLCTYP=3
  echo "Compiling old code version...."
  echo "This is used for spherical and morse!"
  bash compileHOClOldVer.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
elif [ $2 = "-cori" ] ; then
  caLCTYP=4
  echo "Compiling original code version...."
  bash compileHOCldvr3dmaster.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_ori.x $ruNLOC/$caLCLOC
elif [ $2 = "-dc" ] ; then
  echo "No compilation requested. "
  echo "Copying from "$LOBCODE_DIR"/Executable_code"
  if [ $3 = "-all" ] ; then
    caLCTYP=1
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_ori.x $ruNLOC/$caLCLOC
  elif [ $3 = "-lob" ] || [ $3 = "-new" ] ; then
    caLCTYP=2
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
  elif [ $3 = "-old" ] ; then
    caLCTYP=3
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
  elif [ $3 = "-ori" ] ; then
    caLCTYP=4
    cp $LOBCODE_DIR/Executable_code/dvr_HOCl_ori.x $ruNLOC/$caLCLOC
  else
    echo "Please select version of code required"
    echo "For help type ./runHOCl -h"
    exit
  fi
else
  echo "Please select version of code required"
  echo "For help type ./runHOCl -h"
  exit 
fi 
# COPYING FITTING ROUTINE TO CALC DIR *****************************************
cp $LOBCODE_DIR/Potentials/HOCl/hocl.fits $ruNLOC/$caLCLOC
# COPY TESTDIR TO SCRATCH SPACE AND MOVE THERE*********************************
cd $ruNLOC
if [ -e "$caLCLOCSCR" ] ; then
  cp "$caLCLOC"/* $caLCLOCSCR
else
  cp -rn "$caLCLOC" "$caLCLOCSCR"
fi
cd $caLCLOCSCR
# RUN CALCULATIONS ************************************************************
if [ $caLCTYP == 1 ] ; then
  for i in *ori.job ; do
    fiLE="${i%.*}"
    echo "Original dvr3d-erynbodge Methodology: Morse"
    ./dvr_HOCl_ori.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
  for i in *old.job ; do
    fiLE="${i%.*}"
    echo "Old Code Methodology: Morse"
    ./dvr_HOCl_Old.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
  for i in *lob.job ; do
    fiLE="${i%.*}"
    echo "New Lobatto Methodology"
    ./dvr_HOCl_Lob.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
elif [ $caLCTYP == 2 ] ; then
  for i in *lob.job ; do
    fiLE="${i%.*}"
    echo "New Lobatto Methodology"
    ./dvr_HOCl_Lob.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
elif [ $caLCTYP == 3 ] ; then
  for i in *old.job ; do
    fiLE="${i%.*}"
    echo "Old Code Methodology: Morse"
    ./dvr_HOCl_Old.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
elif [ $caLCTYP == 4 ] ; then
  for i in *ori.job ; do
    fiLE="${i%.*}"
    echo "Original dvr3d-erynbodge Methodology: Morse"
    ./dvr_HOCl_ori.x < ${i} > "$fiLE".out
    echo "****************************"
    echo 'Band origins'
    grep -A 10 'Band origin' "$fiLE".out | grep '\.'
    echo "DONE"
  done
else
  echo " Errrm... not sure what to do!"
  echo " Quitting..."
  exit
fi
 
# echo '3D eigenvalues'
# grep -A 6 'Lowest 400 eigenvalues in wavenumbers' "$fiLE".out | grep 'D'
# COPY BACK *******************************************************************
echo 'Copy back from scratch? yes/no (then hit return)'
echo 'You have 5 seconds to comply...'
echo '(timeout will prompt automatic copy back)'
read -t 5 YN
if [ $? == 0 ]; then
  if [ $YN = 'y' ] || [ $YN = 'yes' ]; then
    echo 'Copying back from scratch'
    cp -r $caLCLOCSCR/* $ruNLOC/$caLCLOC
    mv $caLCLOCSCR $caLCLOCSCR"_"$(date +%d%m_%H%M)/
    cd $ruNLOC
    exit
  elif [ $YN = 'n' ] || [ $YN = 'no' ]; then
    echo 'No copy back'
    cd $ruNLOC
    mv $caLCLOCSCR $caLCLOCSCR"_"$(date +%d%m_%H%M)
    exit
  else
    echo '...well, you need to choose something sensible!'
  fi
else
  echo '...out of time. Copying back.'
  cp -r $mySCR/$caLCLOC/* $ruNLOC/$caLCLOC
  mv $caLCLOCSCR $caLCLOCSCR"_"$(date +%d%m_%H%M)
  cd $ruNLOC
  exit
fi
echo 'Wubbalubbadubdub!'
