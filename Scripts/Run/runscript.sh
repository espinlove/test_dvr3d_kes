#!/bin/bash

ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

if [ -z $1 ] ; then
  echo " "
  echo "Need at least 1 argument. -h for help"
  echo " "
  exit
elif [ $1 = "-h" ]; then
  echo " "
  echo "HOW2 run calculations"
  echo "dvr3d [POTENTIAL] [RESULTSDIR] [SELECTCODE] [COMPILE?]"
  echo " "
  echo "POTENTIAL - The potential required for the calculation"
  echo "            Can be: -hocl "
  echo "                    -h3p"
  echo "                    -h3pold"
  echo "RESULTSDIR - the directory in which the "
  echo "             calculation will be run "
  echo "SELECTCODE -     -all          Run all"
  echo "                 -lob or -new  Run new source " 
  echo "                 -ori          Run original dvr3d "
  echo "COMPILE? can be: -c            Compile code "
  echo "                 -dc           Don't compile  "
  echo "                 If this is left blank -dc is selected"
  echo " "
  exit
fi


