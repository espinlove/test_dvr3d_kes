#!/bin/bash

#rm */*.x
#rm */*.out
#rm */*.mod

#for UCL computers
ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

caLCLOC="$1"
echo "$caLCLOC"
caLCLOCSCR="$mySCR/$caLCLOC"
echo "$caLCLOCSCR"
cd $caLCLOC
rm *.x
rm *.out
rm *.mod
#echo 'Input test dir;'
#read TESTDIR
# COMPILE CODES AND MOVE TO TEST DIR ******************************************
cd $LOBCODE_DIR/Compile_scripts
echo "Compiling old code version...."
echo "This is used for spherical and morse!"
bash compileHOClOldVer.sh
echo "... Compiled"
cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
# COPY TESTDIR TO SCRATCH SPACE AND MOVE THERE*********************************
cd $ruNLOC
if [ -e "$caLCLOCSCR" ] ; then
  echo "cp "$caLCLOC"/* $caLCLOCSCR"
  cp "$caLCLOC"/* $caLCLOCSCR
else
  echo "cp -rn "$caLCLOC" "$caLCLOCSCR""
  cp -rn "$caLCLOC" "$caLCLOCSCR"
fi
#cp dvr_read.x $mySCR/$TESTDIR
cd $caLCLOCSCR
echo pwd
# RUN CALCULATIONS ************************************************************
echo "Original Methodology: Morse"
#.$ruNLOC/oldsource/dvr_old.x < HOCl_morse.job > HOCl_morse.out
for i in HOCl_morse_*.job ; do
./dvr_HOCl_Old.x < ${i} > ${i}.out
echo "****************************"
echo 'Band origins'
grep -A 10 'Band origin' ${i}.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' ${i}.out | grep 'D'
echo "DONE"
done
# COPY BACK *******************************************************************
echo 'Copy back from scratch? y/n'
read YN
if [ $YN = 'y' ] || [ $YN = 'yes' ]; then
  cp -r $mySCR/$caLCLOC/* $ruNLOC/$caLCLOC
  cd $ruNLOC
  exit
elif [ $YN = 'n' ] || [ $YN = 'no' ]; then
  cd $ruNLOC
  exit
else
  echo 'Not gonna do anything until you give a proper answer'
fi
echo 'Shlumshlum shlippiddydop!'
