#!/bin/bash

#rm */*.x
#rm */*.out
#rm */*.mod

#for UCL computers
ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

if [ -z $1 ] ; then
  echo " "
  echo "Need at least 1 argument. -h for help"
  echo " "
  exit
elif [ $1 = "-h" ]; then
  echo " "
  echo "Runscript Help"
  echo "./SCRIPT.sh [RESULTSDIR] [COMPILE?]"
  echo " "
  echo "RESULTSDIR - the directory in which the "
  echo "             calculation will be run "
  echo "COMPILE? can be: -ca Compile all "
  echo "                 -cn Compile new source " 
  echo "                 -co Compile old source "
  echo "                 -dc Don't compile "
  echo " "
  exit
fi

caLCLOC="$1"
echo "$caLCLOC"
caLCLOCSCR="$mySCR/$caLCLOC"
echo "$caLCLOCSCR"
cd $caLCLOC
rm *.x
rm *.out
rm *.mod
#echo 'Input test dir;'
#read TESTDIR
# COMPILE CODES AND MOVE TO TEST DIR ******************************************
if [ $2 = "-ca" ] ; then
  echo "Compiling both versions of the code:"
  cd $LOBCODE_DIR/Compile_scripts
  echo "Compiling new Lobatto version...."
  bash compileHOClLob.sh
  echo "... Compiled"
  echo "Compiling old code version...."
  echo "This is used for spherical and morse!"
  bash compileHOClOldVer.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/*.x $ruNLOC/$caLCLOC
elif [ $2 = "-cn" ] ; then
  echo "Compiling the new version of the code only:"
  cd $LOBCODE_DIR/Compile_scripts
  echo "Compiling new Lobatto version...."
  bash compileHOClLob.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
elif [ $2 = "-co" ] ; then
  echo "Compiling the old version of the code only:"
  cd $LOBCODE_DIR/Compile_scripts
  echo "Compiling old code version...."
  echo "This is used for spherical and morse!"
  bash compileHOClOldVer.sh
  echo "... Compiled"
  cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
elif [ $2 = "-dc" ] ; then
  echo "No compile selected. Copying existing .x files to run"
  cp $LOBCODE_DIR/Executable_code/*.x $ruNLOC/$caLCLOC
fi
#cd $LOBCODE_DIR/Compile_scripts
#echo "Compiling new Lobatto version...."
#bash compileHOClLob.sh
#echo "... Compiled"
#cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
#cd $LOBCODE_DIR/Compile_scripts
#echo "Compiling old code version...."
#echo "This is used for spherical and morse!"
#bash compileHOClOldVer.sh
#echo "... Compiled"
#cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
# COPY TESTDIR TO SCRATCH SPACE AND MOVE THERE*********************************
cd "$ruNLOC"
if [ -e "$caLCLOCSCR" ] ; then
  cp "$caLCLOC"/* $caLCLOCSCR
else
  cp -rn "$caLCLOC" "$caLCLOCSCR"
fi
#cp dvr_read.x $mySCR/$TESTDIR
cd $caLCLOCSCR
# RUN CALCULATIONS ************************************************************
echo "New Lobatto Methodology"
#.$ruNLOC/source/dvr_HOCl.x < HOCl_Lob.job > HOCl_Lob.out
./dvr_HOCl_Lob.x < HOCl_Lob.job > HOCl_Lob.out
echo "****************************"
echo 'Band origins'
grep -A 10 'Band origin' HOCl_Lob.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_Lob.out | grep 'D'
echo "DONE"
echo "Original Methodology: Spherical"
#.$ruNLOC/oldsource/dvr_old.x < HOCl_spherical.job > HOCl_spherical.out
./dvr_HOCl_Old.x < HOCl_spherical.job > HOCl_spherical.out
echo "****************************"
echo 'Band origins' 
grep -A 10 'Band origin' HOCl_spherical.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_spherical.out | grep 'D'
echo "DONE"
echo "Original Methodology: Morse"
sed -i 's/zmors2=.false./zmors2=.true./g' HOCl_morse.job
#.$ruNLOC/oldsource/dvr_old.x < HOCl_morse.job > HOCl_morse.out
./dvr_HOCl_Old.x < HOCl_morse.job > HOCl_morse.out
echo "****************************"
echo 'Band origins'
grep -A 10 'Band origin' HOCl_morse.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_morse.out | grep 'D'
echo "DONE"
# COPY BACK *******************************************************************
echo 'Copy back from scratch? y/n'
read YN
if [ $YN = 'y' ] || [ $YN = 'yes' ]; then
  cp -r $mySCR/$caLCLOC/* $ruNLOC/$caLCLOC
  cd $ruNLOC
  exit
elif [ $YN = 'n' ] || [ $YN = 'no' ]; then
  cd $ruNLOC
  exit
else
  echo 'Not gonna do anything until you give a proper answer'
fi
echo 'Shlumshlum shlippiddydop!'
