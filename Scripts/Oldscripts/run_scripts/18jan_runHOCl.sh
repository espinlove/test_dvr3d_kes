#!/bin/bash

rm */*.x
rm */*.out
rm */*.mod

#for UCL computers
ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

caLCLOC="$1"
echo "HERE5"
echo "$caLCLOC"
caLCLOCSCR="$mySCR/$caLCLOC"
echo "$caLCLOCSCR"
if [ -e "$caLCLOCSCR" ] ; then
  cp "$caLCLOC"/* $caLCLOCSCR
else
cp -rn "$caLCLOC" "$caLCLOCSCR"
fi

#echo 'Input test dir;'
#read TESTDIR
# COMPILE CODES AND MOVE TO TEST DIR ******************************************
cd source
echo "Compiling new Lobatto version...."
bash compile.sh
echo "... Compiled"
cp dvr_HOCl.x ../$caLCLOC
cd ../oldsource
echo "Compiling old code version...."
bash compileHOCl.sh
echo "... Compiled"
echo "This is used for spherical and morse!"
mv dvr_HOCl.x dvr_old.x
cp dvr_old.x ../$caLCLOC
# COPY TESTDIR TO SCRATCH SPACE AND MOVE THERE*********************************
cp -r ../$caLCLOC $mySCR
#cp dvr_read.x $mySCR/$TESTDIR
cd $mySCR/$caLCLOC
# RUN CALCULATIONS ************************************************************
echo "New Lobatto Methodology"
#.$ruNLOC/source/dvr_HOCl.x < HOCl_Lob.job > HOCl_Lob.out
./dvr_HOCl.x < HOCl_Lob.job > HOCl_Lob.out
echo 'Band origins'
grep -A 10 'Band origin' HOCl_Lob.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_Lob.out | grep 'D'
echo "DONE"
echo "Original Methodology: Spherical"
#.$ruNLOC/oldsource/dvr_old.x < HOCl_spherical.job > HOCl_spherical.out
./dvr_old.x < HOCl_spherical.job > HOCl_spherical.out
echo "****************************"
echo 'Band origins' 
grep -A 10 'Band origin' HOCl_spherical.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_spherical.out | grep 'D'
echo "DONE"
echo "Original Methodology: Morse"
sed -i 's/zmors2=.false./zmors2=.true./g' HOCl_morse.job
#.$ruNLOC/oldsource/dvr_old.x < HOCl_morse.job > HOCl_morse.out
./dvr_old.x < HOCl_morse.job > HOCl_morse.out
echo "****************************"
echo 'Band origins'
grep -A 10 'Band origin' HOCl_morse.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_morse.out | grep 'D'
echo "DONE"
# COPY BACK *******************************************************************
echo 'Copy back from scratch? y/n'
read YN
if [ $YN = 'y' ] || [ $YN = 'yes' ]; then
  cp -r $mySCR/$caLCLOC/* $ruNLOC/$caLCLOC
  cd $ruNLOC
  exit
elif [ $YN = 'n' ] || [ $YN = 'no' ]; then
  cd $ruNLOC
  exit
else
  echo 'Not gonna do anything until you give a proper answer'
fi
echo 'Shlumshlum shlippiddydop!'
