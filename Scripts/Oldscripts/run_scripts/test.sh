#!/bin/bash

#rm */*.x
#rm */*.out
#rm */*.mod

#for UCL computers
ruNLOC="$(pwd)"
hoME=~/
myHOME=$(basename "$hoME")
scR=/scratch
mySCR="$scR/$myHOME"

mkdir -p "$mySCR"

caLCLOC="$1"
echo "$caLCLOC"
caLCLOCSCR="$mySCR/$caLCLOC"
echo "$caLCLOCSCR"
if [ -e "$caLCLOCSCR" ] ; then
  cp "$caLCLOC"/* $caLCLOCSCR
else
cp -rn "$caLCLOC" "$caLCLOCSCR"
fi
cd $caLCLOC
rm *.x
rm *.out
rm *.mod
#echo 'Input test dir;'
#read TESTDIR
# COMPILE CODES AND MOVE TO TEST DIR ******************************************
cd $LOBCODE_DIR/Compile_scripts
echo "Compiling new Lobatto version...."
bash compileHOClLob.sh
echo "... Compiled"
cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Lob.x $ruNLOC/$caLCLOC
cd $LOBCODE_DIR/Compile_scripts
echo "Compiling old code version...."
echo "This is used for spherical and morse!"
bash compileHOClOldVer.sh
echo "... Compiled"
cp $LOBCODE_DIR/Executable_code/dvr_HOCl_Old.x $ruNLOC/$caLCLOC
# COPY TESTDIR TO SCRATCH SPACE AND MOVE THERE*********************************
if [ -e "$caLCLOCSCR" ] ; then
  cp "$caLCLOC"/* $caLCLOCSCR
else
  cp -rn "$caLCLOC" "$caLCLOCSCR"
fi
#cp dvr_read.x $mySCR/$TESTDIR
cd $caLCLOCSCR
# RUN CALCULATIONS ************************************************************
echo "New Lobatto Methodology"
#.$ruNLOC/source/dvr_HOCl.x < HOCl_Lob.job > HOCl_Lob.out
for f in HOCl_Lob_*.job ; do
./dvr_HOCl_Lob.x < ${f} > ${f}.out
echo "****************************"
echo 'Band origins'
grep -A 10 'Band origin' ${f}.out | grep '\.'
echo '3D eigenvalues'
grep -A 6 'Lowest 400 eigenvalues in wavenumbers' ${f}.out | grep 'D'
echo "DONE"
done
#echo "Original Methodology: Spherical"
##.$ruNLOC/oldsource/dvr_old.x < HOCl_spherical.job > HOCl_spherical.out
#for g in HOCl_spherical_*.job ; do
#./dvr_HOCl_Old.x < ${g} > ${g}.out
#echo "****************************"
#echo 'Band origins' 
#grep -A 10 'Band origin' ${g}.out | grep '\.'
#echo '3D eigenvalues'
#grep -A 6 'Lowest 400 eigenvalues in wavenumbers' ${g}.out | grep 'D'
#echo "DONE"
#done
#echo "Original Methodology: Morse"
#sed -i 's/zmors2=.false./zmors2=.true./g' HOCl_morse.job
##.$ruNLOC/oldsource/dvr_old.x < HOCl_morse.job > HOCl_morse.out
#./dvr_HOCl_Old.x < HOCl_morse.job > HOCl_morse.out
#echo "****************************"
#echo 'Band origins'
#grep -A 10 'Band origin' HOCl_morse.out | grep '\.'
#echo '3D eigenvalues'
#grep -A 6 'Lowest 400 eigenvalues in wavenumbers' HOCl_morse.out | grep 'D'
#echo "DONE"
## COPY BACK *******************************************************************
echo 'Copy back from scratch? y/n'
read YN
if [ $YN = 'y' ] || [ $YN = 'yes' ]; then
  cp -r $mySCR/$caLCLOC/* $ruNLOC/$caLCLOC
  cd $ruNLOC
  exit
elif [ $YN = 'n' ] || [ $YN = 'no' ]; then
  cd $ruNLOC
  exit
else
  echo 'Not gonna do anything until you give a proper answer'
fi
echo 'Shlumshlum shlippiddydop!'
