#/bin/bash

cd $LOBCODE_DIR/Source/source/
rm *.mod
rm *.x
ifort -O3 -g -mkl main.f90 ccmain.f90 keints.f90 LobattoIntsDVR.f90 jhmain.f90 mkmain.f90 nfmain.f90 utilities.f90 $LOBCODE_DIR/Potentials/H3p_oldpot/pot_h3p.f90 -o dvr_oldH3p_Lob_n.x 
mv dvr_oldH3p_Lob_n.x $LOBCODE_DIR/Executable_code/

