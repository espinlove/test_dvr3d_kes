#/bin/bash

cd $LOBCODE_DIR/Source/oldsource/
rm *.mod
ifort -O3 -g -traceback -mkl main.f90 ints.f90 dvr3drjz.f90 $LOBCODE_DIR/Potentials/H3p_oldpot/pot_h3p.f90 -o dvr_oldH3p_Old.x 
mv dvr_oldH3p_Old.x $LOBCODE_DIR/Executable_code/

