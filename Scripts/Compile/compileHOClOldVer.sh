#/bin/bash

cd $LOBCODE_DIR/Source/oldsource/
rm *.mod
ifort -O3 -g -traceback -mkl main.f90 ints.f90 dvr3drjz.f90 $LOBCODE_DIR/Potentials/HOCl/hocl_pes.f90 -o dvr_HOCl_Old.x 
mv dvr_HOCl_Old.x $LOBCODE_DIR/Executable_code/

