#/bin/bash

cd $LOBCODE_DIR/Source/source_n/
rm *.mod
rm *.x
ifort -O3 -g -mkl main.f90 ccmain.f90 keints.f90 LobattoIntsDVR.f90 jhmain.f90 mkmain.f90 nfmain.f90 utilities.f90 $LOBCODE_DIR/Potentials/HOCl/hocl_pes.f90 -o dvr_HOCl_Lob_n.x 
mv dvr_HOCl_Lob_n.x $LOBCODE_DIR/Executable_code/
ifort -g -mkl main.f90 ccmain.f90 keints.f90 LobattoIntsDVR.f90 jhmain.f90 mkmain.f90 nfmain.f90 utilities.f90 $LOBCODE_DIR/Potentials/HOCl/hocl_pes.f90 -o dvr_HOCl_Lob_n.debug 
mv dvr_HOCl_Lob_n.debug $LOBCODE_DIR/Executable_code/
