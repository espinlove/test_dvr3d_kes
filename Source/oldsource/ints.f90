
!###########################################################################
      subroutine keints(hbl,fke,nlim,nmax,iu)

!     keints calculates analytic kinetic energy integrals over r    #012
!     for morse oscillator-like functions

      implicit real*8 (a-h,o-y), logical (z)

      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /rmatreact/ zfinitereg, zrmat, bndr
      real*8, dimension(nlim) ::  hbl

      data x0/0.0d0/
    
      if (zfinitereg) then
         write(6,*) "This basis set can't be used for a finite region calculation"
         stop
      end if

      index = 0
      do 10 n2=1,nmax+1
        do 20 n1=1,n2
          index=index+1
          if (n1 .eq. n2) then
!             special case:  n1 = n2
              hbl(index) = fke * dble(2*(n2-1)*(n2+iu)+iu+1)
          else if (n1+2 .eq. n2) then
!                  special case:  n2 = n1 + 2
        hbl(index) = - fke * sqrt(dble((iu+n1+1)*(iu+n1))*dble((n1+1)*n1))
          else
!           n1+1 = n2 or n2 > n1 + 2  all matrix elements are zero
            hbl(index) = x0
          endif
   20   continue
   10 continue
      if (.not. zprad) return
!     write kinetic energy integrals if requested
      write(6,500)
  500 format(//,5x,'radial kinetic energy matrix calculated',&
              ' analytically',/)
      call symout(hbl,nmax+1)
      return
      end

!#######################################################################

subroutine inttail2(tailints,nlim,nmax,npnt,alf,y2,r2,wt2)

!     inttail2 should be run with keint2 and calculates correction due to finite region
!     for the spherical oscillator basis functions

!implicit real*8 (a-h,o-y), logical (z)
implicit none

!common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
!zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
!zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
!idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
!ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
logical :: zfinitereg, zrmat
real*8  :: bndr,re2, diss2, we2, beta2, ur2, urr2, a2, iu2, alf,r2scaled
integer :: i, j, npnt, nmax, nlim, ni, n1, n2, index
real*8  :: bra, ket, ketail, vtail
real*8, dimension(npnt) :: y2, r2, wt2
real*8, dimension(nlim) :: tailints

common /rmatreact/ zfinitereg, zrmat, bndr
common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2
write(6,*) "This basis set can be used for a finite region calculation, but isn't fully coded yet"
write(6,*) "We are now calculating the corrections due to finite region integrals"
write(6,*) "the boundary region is ", bndr
write(6,*) "the parameters of basis function 2 are", re2,diss2,we2,beta2,ur2,urr2,a2,iu2

index=0;
do n1 = 1,nmax+1
do n2=n1,nmax+1
index=index+1
!write(6,*) "We are here"

ketail=0d0;
write(6,*) "in inttails, bndr = ", bndr
!tailints(i)=0d0;


do j=1,npnt
  r2scaled = r2(j)+bndr
  !write(6,*) "KE int start", Y2(j), WT2(j), R2(j), a2, beta2, n1, r2scaled(j)
  call dersphericalosc(bra,beta2,a2,n1,r2scaled)
  call dersphericalosc(ket,beta2,a2,n2,r2scaled)
  !bra is the value of the first derivative of the n1 bra basis function function at r2
  !ket is the value of the first derivative of the n2 ket basis function at r2
   ketail = ketail+Y2(j)*WT2(j)*bra*ket
end do
print *, "finished the KEtail"
write(6,*) "index = ", index, "n1 = ", n1, "n2 = ", n2, "nlim = ", nlim, "nmax = ", nmax
tailints(index)=ketail
vtail=0d0;
!call potv(ves,1.d0,1.d0,0.5d0)
tailints(index)=ketail+vtail
end do
end do
write(6,*) i, nlim

write(6,*) "End calculation of corrections due to finite region integrals"

return
end subroutine


!#################################################################


subroutine dersphericalosc(result,beta,alf,n,r)
! calculate the derivative of the spherical oscillator basis functions with respect to r (eventually)
! It might start with derivative w.r.t. y

implicit none

real*8  :: result, beta, alf,r
integer :: n, i, n2, l
real*8  :: gammaln, xn, dd
real*8  :: pf,dr,t1,t2,dy, y, lauralag

dr = 2*r*beta
y=beta*r**2
pf = -2**(-1/2)*exp(-y/2)*y**((alf-1)/2)*beta**(1/4)
t1=2*y*lauralag(n-1,1.5d0+alf,y)
t2=(y-alf-1)*lauralag(n,0.5d0+alf,y)

dy=pf*(t1+t2)
result=dr*dy

return

end subroutine


!******#####**************
function lauralag(nmax,alf,y)
implicit none

integer, intent(in) :: nmax
real*8, intent(in) ::  y, alf

integer :: i, n, n2, l
real*8  :: lauralag, gammaln
real*8  :: amx, x0, xp5, x1, x2,xn, xep, toler, en, dd, alfm1
  DATA X0,XP5,X1,X2/0.0D0,0.5D0,1.0D0,2.0D0/,TOLER/1.0D-8/
real, allocatable :: bass(:,:), dnormlaura(:)
allocate(bass(0:n,1),dnormlaura(0:n))



! set up laguerre's normalis
do n =0,nmax
xn =dble(n )
dd=gammaln(xn +1.d0)-gammaln(xn+alf+1.d0)
dnormlaura(n)=dexp(dd*0.5d0)
end do
!      csa=csa*(dnormnew(0)**2)




i=1
!     CALCULATE UNNORMALISED LAGUERRE POLYNOMIALS AT Y
!
!     POLYNOMIAL OF ORDER 0
!xep=dexp(-y(I)*0.5d0)
xep=dexp(-y*0.5d0)

BASS(0,I) = X1*xep



IF (NMAX .LT. 1) GOTO 70
!     POLYNOMIAL OF ORDER 1
AMX = ALF + X1 - Y
BASS(1,I) = AMX*xep
!     USE RECURRENCE RELATIONSHIPS FOR POLYNOMIALS OF ORDER > 2
!     N * L(N,ALF) = (2*N+ALF-1-X)*L(N-1,ALF) - (N+ALF-1)*L(N-2,ALF)
EN = X1
DO N=2,NMAX
EN = EN + X1
AMX = AMX + X2
BASS(N,I) = (AMX * BASS(N-1,I) - (ALFM1+EN) * BASS(N-2,I)) / EN
end do
70 CONTINUE
!
DO 90 N2=0,NMAX
!     NORMALISE POLYNOMIALS
BASS(N2,I) = BASS(N2,I) * dnormlaura(N2)
90 CONTINUE
60 CONTINUE

lauralag = bass(nmax,1);
deallocate(bass,dnormlaura)

return



end function lauralag

!#######################################################################


      subroutine keint2(hbl,fke,rm2,dnorm,nlim,nmax,npnt,alf)

!     keint2 calculates analytic kinetic energy integrals over r2   #013
!     and moment of intertia integral for spherical oscillator functions

      implicit real*8 (a-h,o-y), logical (z)

      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /rmatreact/ zfinitereg, zrmat, bndr
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1
      common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2

      integer :: ni, n1, n2
      real*8, dimension(nlim) :: hbl
      real*8, dimension(nlim) :: rm2
      real*8, dimension(*) :: dnorm
      real*8, dimension(nlim) :: tailints




      data x0/0.0d0/,xp5/0.5d0/,x1/1.0d0/
      gam = fke / (alf + xp5)
      do 10 n1=1,npnt
         gam = gam /(dble(n1)+alf+xp5)
   10 continue  
! gam = g(alf+0.5) / g(a+0.5+N+1)
      fact = x1
      fn = x0
      sum = gam
      do 20 n1 = 1,nmax+1
      index = (n1 * (n1+1)) / 2
      do 30 n2 = n1,nmax+1
      rm2(index) = dnorm(n1) * dnorm(n2) * sum
      index = index + n2
   30 continue
      gam = (fn+alf+xp5) * gam
      fn = fn + x1
      fact = fn * fact
      sum = sum + gam / fact
   20 continue
      fact = alf * (alf + x1)
      do 40 index=1,nlim
      hbl(index) = - fact * rm2(index)
   40 continue
      index = 0
      fn = - x1
      do 50 n2=1,nmax+1
      fn = fn + x1
      index=index+n2
!     special case:  n1 = n2
      hbl(index) = hbl(index) + fke * (fn+fn+alf+1.5d0)
!     special case:  n2 = n1 + 1
      if (n2 .gt. 1)&
          hbl(index-1) = hbl(index-1) + fke * sqrt(fn*(fn+alf+xp5))
   50 continue

      if (.not. zprad) return
!     write kinetic energy & inertia integrals if requested
      write(6,500)
  500 format(//5x,'radial kinetic energy matrix calculated',&
                   ' analytically'/)
      call symout(hbl,nmax+1)
      write(6,510)
  510 format(//5x,'moment of inertia matrix calculated analytically'/)
      call symout(rm2,nmax+1)



      return
      end





