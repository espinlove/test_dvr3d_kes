      program DVR3DRJZ
      call dvr3d
      stop
      end

!######################################################################
      subroutine dvr3d
!
!     program               d v r 3 d r j z
!                           ~~~~~~~~~~~~~~~
!     should be cited as:
!         *********** add here ********************
!     program to do ro-vibrational calculations on triatomic systems
!     using general length, length, angle coordinates and a choice of
!     embeddings, in a multidimensional dvr in choice of coordinate
!     orders.
!     there are various options:
!     (a) calculation at a frozen angle                       ztwod  = t
!     (b) atom - rigid diatom calculations                    ncoord = 2
!     (c) triatomic calculations in all 3 dimensions          ncoord = 3
!     additionally, for Radau coordinates there are options to:
!     (d) place the z-axis along the bisector                 zbisc  = t
!     (e) place the z-axis perpendicular to the molecule plane zperp = t
!     see:
!
!     j.r.henderson & j.tennyson, chem.phys.lett., 173, 133 (1990).
!     j.r.henderson, phd thesis, university of london (1990).
!     j.r.henderson, j.tennyson & b.t. sutcliffe, j.chem.phys. 98, 7191 (1993).
!     j. tennyson & b.t. sutcliffe, int. j. quantum chem. 42, 941 (1992).
!
!     use as follows:
!     comments on namelist parameters (& defaults) in block data
!     input data read in subroutines insize & setcon
!     the program needs the following subroutines:
!            subroutine potv(v,r1,r2,x) should return the potential v
!            in hartrees for the point x = cos(theta) and
!            bondlengths r1 & r2 in bohr.
!     2. dsyev to do in core diagonalisation (lapack f90 routine).
!     the programme works in **** atomic units ***** :
!     1. the subroutine potv should return the potential in
!        hartrees for distances in bohr.
!     2. all input in setcon is in hartree or bohr except
!     3. the nuclear masses are read in atomic mass units & converted.
!     4. the eigenvalues are printed in both hartree & wavenumbers.
!
!     Rewritten into fortran 95 by Max Kostin and Jonathan Tennyson
      implicit logical (z)
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      namelist/prt/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot,&
                    zfinitereg,zrmat,bndr
real*8 :: bndr
      common /rmatreact/ zfinitereg, zrmat, bndr
      common/timing/itime0

      write(6,1000)
 1000 format(5x,'Program DVR3DRJZ (version of June 2010)')

!     read in namelist input data (defaults in block data)
      read(5,prt)

      write(6,*) 'zfinitereg',zfinitereg,'zrmat',zrmat,'bndr',bndr

      if (zrmat .neqv. zfinitereg) then
          if (zrmat .eqv. .true.) then
             write(6,*) "You can't construct an rmatrix at an infinite boundary"
             write(6,*) "Please do a finite region calculation (zfinitereg=.true.) at the same time"
             stop
          else
             write(6,*) "WARNING: You are only running a finite region calculation (no Rmatrix)"
          end if
        
      end if

      call SYSTEM_CLOCK(itime0,irate2,imax2)

!     read in control parameters of problem.
      call insize

!     now do the calculation
      call ccmain

      call SYSTEM_CLOCK(itime2,irate2,imax2)
      itime=(itime2-itime0)/irate2
      write(6,1)itime
 1    format(/i10,' secs CPU time used'/) 
      stop
      end

!##############################################################################
      block data
!     stores defaults for namelist parameters  
      implicit logical (z)
real*8 :: bndr

!  zpham[f] = t requests printing of the hamiltonian matrix.
!  zprad[f] = t requests printing of the radial matrix elements.
!  zp1d [f] = t requests printing of the results of 1d calculations.
!  zp2d [f] = t requests printing of the results of 2d calculations.
!  zpmin[f] = t requests only minimal printing.
!  zpvec[f] = t requests printing of the eigenvectors.
!  zlmat[f] = t requests printing of the L-matrix.
!  zcut[f]  = t final dimension selected using an energy cut-off given
!             by emax2.
!            = f final dimension determined by nham3.
!  zmors1[t]= t use morse oscillator-like functions for r_1 coordinate;
!           = f use spherical oscillator functions.
!  zmors2[t]= t use morse oscillator-like functions for r_2 coordinate;
!           = f use spherical oscillator functions.
!  zrot[t]  = t do vibrational part of rotational calculation by
!               looping over k
!  zperp[f] = f  z in plane calculation (uses JHMAIN or NFMAIN)
!           = t  z perpendicular calculation (use MKMAIN)
!  zbisc[f] = t  bisector embedding (use NFMAIN), set by the program
!  zembed[t]= t z axis is along r2, = f z axis is along r1.
!               only used if J > 0 ZBISC = in JHMAIN ie if zbisc=f and zperp=f.
!  zlin     = t forces suppresion of functions at last dvr point
!               (zbisc=t only).
!  zladd[t] = t NALF kept constant as k increases
!           = f NALF decreases with k (=f has a bug), (only if zrot = .true.)
!  ztwod[f] = t perform 2D calculation only at specified grid point.
!  zvec[f]  = t store the eigenvectors from all the parts of the calculation
!             (1d,2d and 3d) on stream iout2.
!             further information relating to this (arrays iv1 and iv2) is
!             stored on stream iout1.
!  zall[f]  = t requests no truncation of the intermediate solution.
!  ztheta[t]= t let theta be first in the order of solution;
!           = f let theta be last in the order of solution,
!             (used if idia > -2 only).
!  zr2r1[t] = t let r_2 come before r_1 in the order of solution;
!           = f let r_1 come before r_2 in the order of solution.
!             (used if idia > -2 only).
!  ztran[f]= t perform the transformation of the solution coefficients
!             to the expression for the wavefunction amplitudes at the grid
!             points. store the data on stream iwave, ztran = t
!             automatically sets zvec = t for idia > -2.
!  zquad2[t]= t use the dvr quadrature approximation for the integrals of
!             the r_2^{-2} matrix, and hence make its dvr transformation
!             diagonal.
!           = f evaluate the r_2^{-2} integrals fully and perform the
!             full dvr transformation on them.
!             note that zquad2 = f is not implemented for zmors2 = t
!             or for ztheta = f.
!  zdiag[t] = f do not do final diagonalisation, instead the final Hamiltonian
!             matrix is written on units IDIAG1 and IDIAG2. 
!  zpfun[t] = t store energy levels on stream ilev
!  zplot[f] = t prints all needed wavefunctions for plotting purposes
!  ilev[14]     stream for final eigenvalues (formatted).
!  ieigs1[7]    stream for eigenvalues of the 1d solutions.
!  ivecs1[3]    stream for eigenvectors of the 1d solutions.
!  ieigs2[2]    stream for eigenvalues of the 2d solutions.
!  ivecs2[4]    stream for eigenvectors of the 2d solutions.
!  ivint[17]    a scratch stream used for storing intermediate vectors in
!               building the final hamiltonian.
!  intvec[16]   a scratch stream for intermediate storage of the 2d vectors.
!  iband[15]    scratch file used for storing bands of the final hamiltonian.
!  iout1[24]    stream for arrays iv1 and iv2, which record the sizes of
!               the truncated vctors. used when zvec = t.
!  iout2[25]    stream for the 1d, 2d and 3d vectors for use when zvec = t.
!  iwave[26]    stores the wavefunction amplitudes at the grid points when
!               ztran = t.
!
!
!
!  zfinitereg    performs a finite region calculation if true
!  zrmat         calculates rmatrix at bndr if true
!  bndr         r2 scattering coordinate of rmatrix boundary
!
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      data zpham/.false./,zprad/.false./,zpvec/.false./,zrot/.true./,&
           zladd/.true./,zembed/.true./,zmors2/.true./,&
           zpmin/.false./,zvec/.false./,zquad2/.true./,zcut/.false./,&
           zdiag/.true./,zlmat/.false./,zall/.false./,zplot/.false./,&
           zp1d/.false./,zp2d/.false./,zr2r1/.true./,ztheta/.true./,&
           zmors1/.true./,ztran/.false./,ztwod/.false./,zperp/.false./,&
           ieigs1/7/,ivecs1/3/,ieigs2/2/,ivecs2/4/,ivint/17/,&
           iband/15/,intvec/16/,idiag1/20/,idiag2/21/,iout1/24/,&
           iout2/25/,iwave/26/,zlin/.false./,zpfun/.false./,ilev/14/,idip/69/&
           idipd/70/,iwvpb/71/,iplot/40/
     common /rmatreact/ zfinitereg, zrmat, bndr
     data zfinitereg/.false./,zrmat/.false./,bndr/3d0/


 
     end

!############################################################################
      subroutine insize

!     set up common /size/ & write control parameters of problem  

      implicit real*8 (a-h,o-y), logical (z)

!     common /size/ stores control parameters for the problem
!     npnt1: number of (gauss-laguerre) dvr points in r1
!     npnt2: number of (gauss-laguerre) dvr points in r2
!     nmax1: max order of r1 radial laguerre polynomial ( = npnt1-1)
!     nmax2: max order of r2 radial laguerre polynomial ( = npnt2-1)
!     maxleg:max order of angular legendre polynomial   ( = nalf -1)
!     max2d :upper bound on size of intermediate 2d hamiltonian
!     max2d2:max2d for smaller block (zbisc=t only)
!     max3d :upper bound on size of full 3d hamiltonian
!     max3d2:max3d for smaller block (zbisc=t only)
!     nalf : number of (gauss-legendre) dvr points in theta
!     idvr : number of unique dvr points
!     jrot : jrot is total angular momentum of the molecule
!     kmin : zrot=t, kmin=1 sym. rot. basis, =0 anti-sym.
!                    kmin=2 loop over both sym & anti-sym (zbisc=t only)
!            zrot=f, kmin=fixed value of k
!     npnt : max(npnt1,npnt2)
!     idia : = 1 scattering coordinates heteronuclear diatomic
!            = 2 scattering coordinates homonuclear diatomic
!            =-1 radau      coordinates hetronuclear diatomic
!            =-2 radau      coordinates homonuclear  diatomic
!            = 0 radau      coordinates with the z axis perpendicular to
!                the molecular plane.
!     ipar : parity of basis - if idia=+/-2: ipar=0 for even & =1 for odd
!     nlim1: =nmax1+1*(nmax1+1+1)/2
!     nlim2: =nmax2+1*(nmax2+1+1)/2
!     npnta: the number of dvr points in
!            the coordinate to be treated first in the dvr successive
!            diagonalisation-truncation procedure
!     npntb: the number of dvr points in the coordinate to come second
!     npntc: the number of dvr points in the coordinate to come last
!     ndima: set equal to npnta at the start - used for dimensioning
!     ndimb: set equal to npntb at the start - used for dimensioning
!     ndimc: set equal to npntc at the start - used for dimensioning
!     neval: number of eigenvalues which have to actually be supplied
!            as output
!     ncoord: number of vibrational coordinates explicitly considered
!     if (ncoord .ne. 3) some of the above are dummies, see below.

      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      character(len=8) title(9)
!     read in control parameters of problem:

!     ncoord = 2: atom-diatom problem with diatom rigid
!     ncoord = 3: full 3-d triatomic problem
      read(5,5) ncoord
    5 format(15i5)
!     other paramters: see comments above on /size/ for meaning
!     if ncoord=2: also need lmax,lpot,idia,kmin
!     if ncoord=3: all paramters required

      read(5,5) npnt2,jrot,neval,nalf,max2d,max3d,idia,&
                kmin,npnt1,ipar,max3d2

      zbisc = .false.
      if (jrot .eq. 0) then
         zembed = .true.
         kmin = 0
         zrot = .false.
      endif
      if (idia .eq. -2) then
         ncoord=3
         npnt1=npnt2
         idvr=nalf
         zmors1=zmors2
         ztheta=.false.
         if (jrot .ne. 0.and.zperp.neqv..true.) zbisc=.true.
         if (zperp.and.zrot) then
            kmin=1
            if (jrot .eq. 0) kmin=0
         endif
      else
          if (ztran) zvec=.true.
      endif

      if (ztwod) then
         ncoord = 3
         idia = 1
         nalf = 1
         nalf2= 1
         idvr = 1
         zrot=.false.
         ztheta=.false.
         max3d=max2d
         neval=min(max2d,neval)
         goto 887
      endif

!     the gauss-legendre integration points are only acually
!     calculated for the half-range:
      nalf2 = (nalf+1)/2
!     are we doing atom, atom-rigid diatom or the full problem?
      if (ncoord .eq. 2) then
!        atom - rigid diatom case, set dummy /size/
         if (zr2r1) then
            write(6,1010)
 1010 format(/10x,'atom - rigid diatom vibrational analysis with:'/)
            npnt1 = 1
            nmax1 = 0
         else
            write(6,1011)
 1011 format(/5x,'Fixed - r2 vibrational analysis with:'/)
            npnt2 = 1
            nmax2 = 0
         endif
      else
!        full case: print data about extra radial basis
         ncoord  = 3
         write(6,1020) npnt1
 1020    format(/5x,'Full triatomic vibrational problem with'/&
                /5x,i5,3x,'radial r1 dvr points used,')
      endif
      
  887 continue
!     maximum order of polynomials;
      maxleg = nalf - 1
      nmax2  = npnt2- 1
      nmax1  = npnt1- 1

      if (ztheta) then
          npnta = nalf/max(1,idia)
          if (zr2r1) then
             npntb = npnt2
             npntc = npnt1
          else
             npntb = npnt1
             npntc = npnt2
          endif
      else
          if (zr2r1) then
             npnta = npnt2
             npntb = npnt1
          else
             npnta = npnt1
             npntb = npnt2
          endif
          npntc = nalf/max(1,idia)
      endif

      if (idia .eq. -2) then
         if (zrot .and. (jrot+kmin).gt.1) then
            max2d  = npnt1*(npnt1+1)/2
            max2d2 = npnt1*(npnt1-1)/2
         else
            max2d = npnt1*(npnt1+1-(ipar * 2))/2
            max2d2 = 0
         endif
         if (.not. zall)  max3d=min(max3d,max2d*nalf)
         if (zall) max3d=max2d*nalf
         if (zrot) then
            if (max3d2 .le. 0) max3d2=max3d
            max3d2=min(max3d,max2d2*nalf,max3d2)
         endif
      else
         if (zall) then
            max2d = npnta*npntb
            max3d = max2d*npntc
         else
            max2d=min(max2d,npnta*npntb)
            max3d=min(max3d,npnta*npntb*npntc)
         endif
      endif

      if (neval .le. 0) neval = 10
      neval=min(max3d,neval)
      if (ztwod) write(6,1023) npnt1
 1023 format(/5x,i5,3x,'radial r1 dvr points used,')
      if (ncoord .eq. 3) write(6,1030) npnt2,2,nalf,neval,max3d
      if (ncoord .eq. 2 .and. zr2r1)&
          write(6,1030) npnt2,2,nalf,neval,max3d
      if(ncoord .eq. 2 .and. .not. zr2r1)&
          write(6,1030) npnt1,1,nalf,neval,max3d
 1030 format(5x,i5,3x,'radial r',i1,' dvr points used,',&
            /5x,i5,3x,'angular dvr points used, with',&
            /5x,i5,3x,'lowest eigenvectors chosen from',&
            /5x,i5,3x,'maximum dimension secular problem'/)
      if(idia .eq. 2 .and. zperp) then
        write(6,1035)
        stop
 1035  format(/5x,'STOP!!!  ZPERP should be .false. for IDIA=2')
      endif      
! new bit regarding the zplot option:
      if (zplot) then
         if (.not.zperp) write(6,1038)
1038 format(5x,'As of 29th june 2006 zplot works only combined with',&
           /5x,' zperp=TRUE. Apologies for any inconveniences cause.',&
           /5x,' pb. '/)
         read(5,1037)nploti,nplotf,ithre,npth
1037 format(4I5)
         write(6,1036)nploti,nplotf,ithre,npth
 1036 format(5x,'Requested wavefunctions for plotting:', &
            /5x,'waves required from n. ',2x,i5,&
            /5x,'               to   n. ',2x,i5,&
            /5x,'cutting threshold set to',3x,i3,&
            /5x,'finally number of theta points selected:',3x,i3/)
         if (npth.lt.nalf) then
            npth=nalf
       write(6,*)'!#!#: n theta points found too small: increased to ',nalf
         end if
         if (nplotf.eq.0) nplotf=neval
      nplotf=min(nplotf,neval)
      if (nploti.gt.nplotf) then
         write(6,*)'Inconsinstency in call to wf plotting program'
         write(6,*)'Requested to print from wf: '
         write(6,*)'[ ',nploti,' - ',nplotf,' ] '
         write(6,*)'(number of vibrational levels found ',neval,' )'
         stop
      else
      nplot=nplotf-nploti+1
      end if
      end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      read(5,500)   title
  500 format(9a8)
      write(6,1040) title
 1040 format(5x,'Title: ',9a8/)
      if (ncoord .eq. 3) then
         if (zmors1)  write(6,1050) 1
         if (.not. zmors1) write(6,1060) 1
      endif
      if (ncoord .eq. 2 .and. .not. zr2r1) then
         if (zmors1)  write(6,1050) 1
         if (.not. zmors1) write(6,1060) 1
      else
         if (zmors2) write(6,1050) 2
         if (.not. zmors2) write(6,1060) 2
 1050 format(5x,'Morse oscillators used for r',i1,' basis')
 1060 format(5x,'Spherical oscillators used for r',i1,' basis')
         if (zquad2) then
            write(6,1051)
 1051 format(/5x,'Quadrature approximation used for the r2**(-2) terms'/)
         else
            write(6,1052)
 1052 format(/5x,'Quadrature approximation abandoned for r2**(-2) terms'/)
         endif
      endif
      if (zall) write(6,1067)
 1067 format(/5x,'All solutions from lower dimensions have been used')
      if (ztheta) then
         if (zr2r1) write(6,1042)
 1042    format(5x,'Problem solved in the order: theta -> r2 -> r1')
         if (.not. zr2r1) write(6,1043)
 1043    format(5x,'Problem solved in the order: theta -> r1 -> r2')
      else
         if (.not.ztwod) then
            if (zr2r1) write(6,1044)
 1044    format(5x,'Problem solved in the order: r2 -> r1 -> theta')
            if (.not. zr2r1) write(6,1045)
 1045    format(5x,'Problem solved in the order: r1 -> r2 -> theta')
         else
            if (zr2r1) write(6,1046)
 1046    format(5x,'Problem solved in the order: r2 -> r1')
            if (.not. zr2r1) write(6,1047)
 1047    format(5x,'Problem solved in the order: r1 -> r2')
         endif
      endif
      if (zcut) then
         write(6,1061)
 1061    format(5x,'Final basis selected using energy cut-off')
      else
         if (zrot .and. zbisc .and. (jrot+kmin).gt.1) then
            write(6,1062) max3d,max3d2
 1062    format(/5x,'Final basis comprises',i5,' lowest functions',&
                   ' for even parity hamiltonian'/&
                 5x,'Final basis comprises',i5,' lowest functions',&
                   ' for odd  parity hamiltonian')
         else
           if (.not. zcut) write(6,1064) max3d
 1064      format(5x,'final basis comprises',i5,' lowest functions')
         endif
      endif
      if (.not.ztwod) then
         if (zlmat) write(6,1065)
 1065    format(/5x,'Printing of L-matrix requested')
         if (.not.zlmat) write(6,1066)
 1066    format(/5x,'Printing of L-matrix not requested')
      endif
      if (zp1d) write(6,1071)
 1071 format(5x,'Printing of 1d eigenvalues requested')
      if (zp2d) write(6,1072)
 1072 format(5x,'Printing of 2d eigenvalues requested')
      if (zpham) write(6,1070)
 1070 format(5x,'Printing of hamiltonian matrix requested')
      if (.not.zpham) write(6,1080)
 1080 format(5x,'Printing of hamiltonian matrix not requested')
      if (zprad) write(6,1090)
 1090 format(5x,'Printing of radial matrix elements requested')
      if (.not.zprad) write(6,1100)
 1100 format(5x,'Printing of radial matrix elements not requested')
      if (zpvec) write(6,1110)
 1110 format(5x,'Printing of eigenvectors requested'/)
      if (.not.zpvec) write(6,1120)
 1120 format(5x,'Printing of eigenvectors not requested'/)
      if (zvec) then
         write(6,1130) iout2
         write(6,1131) iout1
 1130 format(5x,'Eigenvalues & vectors   written to stream IOUT2 =',i4)
 1131 format(5x,'Restart information     written to stream IOUT1 =',i4)
         open(unit=iout1, form='unformatted')
         open(unit=iout2, form='unformatted')
      endif
      if (ztran) then
         write(6,1132) iwave
 1132 format(5x,'Wavefunction amplitudes written to stream IWAVE =',i4)
         write(6,1133) idip
 1133 format(5x,'Wavefunctions for dipole written to stream IDIP =',i4)
         write(6,1136) idipd
 1136 format(5x,'Wavefunctions for expect written to stream IDIPD =',i4)
         open(unit=iwave, form='unformatted')
      endif
      if (abs(jrot) .gt. 1) zpfun=.false.
      if (zpfun) then
         open(unit=ilev,form='formatted')
         if (jrot .eq. 0 .and. mod(ipar,2) .eq. 0) then
!           header on file ilev
            write(ilev,500) title
            write(6,1134) ilev
 1134 format(/5x,'Eigenvalues      written to start of stream ilev =',i4)
         else
!           position file ilev
  200       read(ilev,*,end=210,err=210)
            goto 200
  210       continue
! ******  inclusion of the following card is machine dependent ******
!           backspace ilev
            write(6,1135) ilev
 1135 format(/5x,'Eigenvalues      written at end   of stream ilev =',i4)
         endif
      endif
      if (idia .gt. 0) write(6,1140)
 1140 format(/5x,'Calculation performed in scattering coordinates')
      if (idia .le. 0 .and.  .not.zperp) write(6,1150)
 1150 format(/5x,'Calculation performed in Radau coordinates')
      if (idia .le. 0 .and. zperp) write(6,1151)
 1151 format(/5x,'Calculation performed in Radau coordinates with Z axis', &
              /5x,'perpendicular to the plane')
      if (zperp) write(6,1152)
1152  format('zlin not implemented with z-perp')

      if (ztwod) goto 886

      if (abs(idia) .eq. 2) then
         write(6,1180)
 1180    format(/5x,'Diatomic assumed homonuclear')
         if (ipar .eq. 1) then
            write(6,1190)
 1190       format(5x,'Odd parity functions in basis set')
         else if (ipar .eq. 0) then
            write(6,1200)
 1200       format(5x,'Even parity functions in basis set')
         else
            write(6,1205)
 1205       format(5x,'Illegal value of ipar for idia = +/-2: STOP')
            stop
         endif
         if (idia .eq. 2) then
            idvr=nalf2
            if (2*idvr .ne. nalf) goto 960
         endif
      else
         write(6,1210)
 1210    format(/5x,'Diatomic assumed hetronuclear')
         idvr=nalf
         ipar=0
      endif
      if (jrot .ne. 0) then
         jrot=abs(jrot)
         if (zrot) then
            if (kmin .ne. 0 .and. .not. zbisc) kmin=1
            write(6,1220)
 1220 format(/5x,'***  vibrational part of rot-vib calculation  ***')
            write(6,1260) jrot
            if (kmin .eq. 1) then
               write(6,1270)
 1270 format(12x,'with symmetric |Jk> + |J-k> functions in basis')
            else if (kmin .eq. 0) then
               write(6,1280)
 1280 format(12x,'with anti-symmetric |Jk> - |J-k> functions in basis')
            else
               kmin=2
               write(6,1275)
 1275 format(12x,'loop over symmetric & anti-symmetric |jk> functions')
            endif
            if (zladd) write(6,1240)
 1240 format(5x,'Number of angular grid points to be kept constant with k')
            if (.not. zladd) write(6,1250)
 1250 format(/5x,'Nunber of angular grid points to decrease with k')
         else
            write(6,1230) jrot,kmin
 1230 format(5x,'J =',i3,'  k =',i3,&
             /5x,'***  option to neglect coriolis interactions  ***')
            if (abs(kmin) .gt. abs(jrot)) then
               write(6,1235)
 1235 format(5x,'Error: k greater than J. STOP')
               stop
            endif 
         endif
         if (zbisc) then
            zembed=.false.
            write(6,1330)
 1330 format(/5x,'z axis embedded along the biscetor of r1 and r2')
            if (zlin) write(6,1340)
 1340 format(/5x,'Removal of functions with theta = 0 enforced')
         else if (.not.zperp) then
            if (zembed) write(6,1290) 2
 1290       format(/5x,'z axis embedded along the r',i1,' coordinate')
            if (.not. zembed) write(6,1290) 1
         else if (zperp) then 
             write(6,1291) 
 1291       format(/5x,'z axis embedded perp to the molecular plane')
         endif
      else
!        case j = 0
         write(6,1260) jrot
 1260    format(/5x,'J =',i3,' rotational state')
      endif

       write (6,1440)
 1440 format(/5x,'Routine DSYEV to do in core diagonalisation')
  886 continue

!     check input parameters are consistent
      npnt = max(npnt1,npnt2)
      nmax = max(nmax1,nmax2)
!     dimension of square matrices stored in triangular form :
      nlim1 = npnt1 * (npnt1+1) / 2
      nlim2 = npnt2 * (npnt2+1) / 2

!     declare dvr sizes for dimensioning the arrays
      ndima=npnta
      if (idia .le. -2 .and. .not. zrot) ndima=ndima-ipar
      ndimb=npntb
      ndimc=npntc

!     store parameters on disk files requested

      if (zvec) write (iout2) idia,ipar,npnta,npntb,npntc,max2d,max3d,neval

      if (zmors2 .and. .not. zquad2) goto 961
!      if (.not. ztheta .and. .not. zquad2) goto 962
!      if (idia .le. -2 .and. .not. zquad2) goto 963

      return
  960 write(6,970)
  970 format(//6x,'** nalf must be even when idia=2: stop **')
      stop
  961 write(6,972)
  972 format(//6x,'** can''t have zquad2 = f with zmors2 = t : stop **',&
              /6x,'               (not yet implemented)               ')
      stop
  962 write(6,973)
  973 format(//6x,'** can''t have zquad2 = f with ztheta = f : stop **',&
              /6x,'               (not yet implemented)               ')
      stop
  963 write(6,974)
  974 format(//6x,'** can''t have zquad2 = f with idia = -2: stop **',&
              /6x,'               (not yet implemented)               ')
      stop
      end

