 

!########### SUBROUTINE ccmain ################################################
!
!     subroutine ccmain is the 'real' main programme & contains    
!     the calls to the various subroutines which set & solve the
!     intermediate and the final hamiltonians.
!##############################################################################

subroutine ccmain 

      implicit real*8 (a-h,o-y), logical (z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
!                   nploti,nplotf,nplot,ithre,npth,rmin,a0
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1
      common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /mass/ xmass(3),g1,g2,xmassr(3)
      common /rmatreact/ zfinitereg, zrmat,bndr
! 18dec18 boundaries defined in utilities.f90
      common /bound/ rmin,a0

      REAL*8, ALLOCATABLE, DIMENSION(:)   :: dnorm1
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: r1m2
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: r1m2t
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: dnorm2
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: r2m2
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: dz1
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: dz2
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: bass1
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: bass2
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: y1
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: r1
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: wt1
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: y2
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: r2, r2Lob
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: wt2,wt2Lob
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: b
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: c
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: hbl1
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: hbl2
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: tailint2
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: xalf
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: walf
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: pleg, plegw
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: xlmatr
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: xk1
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: xk2, xk2Lob
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: r2m2t
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: jxcos
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: jwalf
      REAL*8, ALLOCATABLE, DIMENSION(:)   :: sjwalf
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: pjac
 
      data x0/0.0d0/,x1/1.0d0/,x2/2.0d0/,x8/8.0d0/,xp5/0.5d0/,&
           toler/1.0d-8/

     ALLOCATE(dnorm1(0:nmax1),r1m2(nlim1),dnorm2(0:nmax2),&
               r2m2(nlim2),bass1(0:nmax1,npnt1),bass2(0:nmax2,npnt2),&
               y1(npnt1),r1(npnt1),wt1(npnt1),y2(npnt2),r2(npnt2),&
               r2Lob(npnt2+1),wt2(npnt2),wt2Lob(npnt2+1),b(npnt+1),c(npnt+1),&
                dz1(npnt1,npnt1),dz2(npnt2,npnt2),&
               hbl1(nlim1),hbl2(nlim2),xalf(idvr),walf(idvr),&
               xlmatr(idvr,idvr),pleg (0:maxleg,idvr),xk1(npnt1,npnt1),&
               xk2(npnt2,npnt2),xk2Lob(npnt2+1,npnt2+1),r2m2t(npnt2,npnt2),&
               r1m2t(npnt1,npnt1),&
               tailint2(nlim2),plegw(nalf,nalf))
! NEW ALLOCATE r2Lob(npnt2+1), wt2Lob(npnt2+1), xk2Lob(npnt2+1,npnt2+1),& 
!              tailint2(nlim2), plegw(nalf,nalf)

    if (zperp) ALLOCATE(sjwalf(idvr),&
                 jxcos(idvr),jwalf(idvr),pjac(0:maxleg,idvr))

!     open the streams......(most not needed for symmetrised radau case)
      if (idia .gt. -2) then
         open(unit=ieigs1,form='unformatted')
         open(unit=ieigs2,form='unformatted')
         open(unit=ivecs1,form='unformatted')
         open(unit=ivecs2,form='unformatted')
         open(unit=ivint, form='unformatted')
         open(unit=iband, form='unformatted')
      endif
      open(unit=intvec,form='unformatted')
      write(6,*) "14dec18 ccmain L92, before setcon called"
!     read in masses & basis set parameters
      call setcon(fixcos)
      write(6,*) "14dec18 ccmain L95, after setcon called"

!.....and save them if necessary
      if (zvec) write(iout2) zembed,zmors1,zmors2,ztheta,zr2r1,xmass,&
                             g1,g2

!     set up binomial and normalisation arrays

      call setfac(dnorm1,dnorm2,cc1,cc2)

!     set up points, weights & basis for numerical integration

      if (ncoord .eq. 2) then
!     in less than 3-d cases fix the 3-d paramters
         if (zr2r1) then
            bass1(0,1) = x1
            wt1(1) = x1
            r1(1) = re1
! Main change with respect to ALL previous versions: dz are the DVR<->FBR
! unitary transformation matrices for r, that is 
!                    dz_ij = L_j(x_i) sqrt(w_i)
! where L are normalized Laguerre pols, so there is no need for having the 
! weights and norms and whatever hanging around.
! But, in case of need, the weights are outputted from LAGPTNEW
!  wt = dsqrt(w). Those weights do not contain the exponential in themselves
! Also in case of need the polys are calculated and normalized in LAGPTNEW
! but not outputted. Finally, the norms as calculated in setfac can not be
! used with my definition of polys and weights, but are kept for they are used
! independently in keint_ routines.

     CALL LAGPTNEW(2,Y2,R2,WT2,DZ2,npnt2,nmax2,zmors2,RE2,beta2,A2,IU2)
            hbl2(1) = x0
!           setup kinetic energy & inertia integrals over r1
            if (zmors1) then
               fke = beta1 * beta1 / (x8 * ur1)
               call keints(hbl1,fke,nlim1,nmax1,iu1)
            else
               fke = beta1 / (x2 * ur1)
               call keint2(hbl1,fke,r1m2,dnorm1,nlim1,nmax1,npnt1,a1)
            endif
         else
            bass2(0,1) = x1
            wt2(1) = x1
            r2(1) = re2
      CALL LAGPTNEW(1,Y1,R1,WT1,DZ1,npnt1,nmax1,zmors1,RE1,WE1,A1,IU1)
           hbl1(1) = x0
!           setup kinetic energy & inertia integrals over r2
            if (zmors2) then
               fke = beta2 * beta2 / (x8 * ur2)
               call keints(hbl2,fke,nlim2,nmax2,iu2)
            else
               fke = beta2 / (x2 * ur2)
               call keint2(hbl2,fke,r2m2,dnorm2,nlim2,nmax2,npnt2,a2)

            endif
         endif
      else ! if ncoord eq 3 
       CALL LAGPTNEW(1,Y1,R1,WT1,DZ1,npnt1,nmax1,zmors1,RE1,beta1,A1,IU1)
        if (idia .gt. -2) then
if (zfinitereg .eqv. .true.) then
print *, "Different basis"
else
CALL LAGPTNEW(2,Y2,R2,WT2,DZ2,npnt2,nmax2,zmors2,RE2,beta2,A2,IU2)
end if
!           setup kinetic energy & inertia integrals over r2
            if (zmors2) then
              fke = beta2 * beta2 / (x8 * ur2)
              call keints(hbl2,fke,nlim2,nmax2,iu2)
            else
              fke = beta2 / (x2 * ur2)
              call keint2(hbl2,fke,r2m2,dnorm2,nlim2,nmax2,npnt2,a2)
            endif
         endif
!        setup kinetic energy & inertia integrals over r1
         if (zmors1) then
            fke = beta1 * beta1 / (x8 * ur1)
            call keints(hbl1,fke,nlim1,nmax1,iu1)
         else
            fke = beta1 / (x2 * ur1)
            call keint2(hbl1,fke,r1m2,dnorm1,nlim1,nmax1,npnt1,a1)
         endif
      endif

!     write the quadrature points to disk for zvec = .true.
      if (zvec) then
         write (iout2) r1
         if (idia .gt. -2) write (iout2) r2
      endif
      if (ncoord .eq. 3) then
      endif

!     take square roots of the weights
!PB:  there is not need for this anymore.....
!      wt1=sqrt(wt1)
!      if (idia .gt. -2) wt2=sqrt(wt2)

!     set up the transformed kinetic energy integrals,  t'(hbl) t
!                                                       ~  ~~~  ~
!      call k1k2(xk1,hbl1,bass1,wt1,npnt1,nmax1,nlim1)
  CALL K1K2NEW(XK1,HBL1,DZ1,NPNT1,NMAX1,NLIM1)

  CALL K1K2NEW(R1M2T,R1M2,DZ1,NPNT1,NMAX1,NLIM1)

      if (idia .gt. -2) then
if (zfinitereg .eqv. .true.) then
print *, "Different basis"
else
CALL K1K2NEW(XK2,HBL2,DZ2,NPNT2,NMAX2,NLIM2)
end if
end if
print *, "Before added bit"
print *, "14dec18, ccmain L206, a0=bndr=",a0
!a0=bndr
!rmin=2.0d0
if (zfinitereg .eqv. .true.) then
print *, "14dec18, ccmain L208, LobattoAbsWeights called"
    call LobattoAbsWeights(r2Lob,wt2Lob,npnt2+1,rmin,a0) !18dec18, added rmin
print *,"14dec18, ccmain L209, back from LobattoAbsWeights"
    r2 = r2Lob(2:npnt2+1)
    wt2 = wt2Lob(2:npnt2+1)
print *, "GAUSS-LOBATTO QUADRATURE POINTS AND WEIGHTS"
print *, "14dec18, ccmain L216, do i=1,npnt2  printing i, r2(i), and wt2(i)"
do i=1,npnt2
print *, i, r2(i), wt2(i)
end do
print *, "14dec18, ccmain L220, AllLobattoKE called"
    call AllLobattoKE(xk2Lob,npnt2+1)
print *, "14dec18, ccmain L221, back from AllLobattoKE"
xk2 = xk2Lob(2:npnt2+1,2:npnt2+1)/ur2
end if

print *, "KE compare", "ur2 = ", ur2
do i=10,20!npnt2
do j=10,20!npnt2
!if (abs(xk2(i,j)) .gt. 0.1) then
!print *, "KE compare", i, j, xk2(i,j)/ur2
!end if
end do
end do

print *, "14dec18, ccmain L234, a0 = ", a0,"rmin = ",rmin
print *, "after added bit"
!         call k1k2(xk2,hbl2,bass2,wt2,npnt2,nmax2,nlim2)

!     ...... and the inertia integrals for spherical oscillators
      if (.not.zmors2 .and. .not.ztwod .and. idia .gt. -2)&
  CALL K1K2NEW(r2m2t,r2m2,DZ2,NPNT2,NMAX2,NLIM2)
!          call k1k2(r2m2t,r2m2,bass2,wt2,npnt2,nmax2,nlim2)



!     some of the j>0 stuff to get the loop over k correct
      if (zrot) then
        kd = 1-min(kmin,1)
        ku = jrot
!       if looping over sym & anti-sym, do one extra k=1 block
        if (kmin .eq. 2) ku=ku+1
      else
        kd = kmin
        ku = kmin
      endif
      kkz12 = 0
      kkz0  = 0
!     for j > 0, store r**(-2) term for rotlev3
      if (ztran) then
         if (ku .gt. kd) then
            if (zembed .and. .not. zperp) then
              if (zquad2) then
print *, "Line 230",(xp5/(r2(i)*r2(i)*urr2),i=1,npnt2)

                write(iwave) (xp5/(r2(i)*r2(i)*urr2),i=1,npnt2)
              else
print *, "Line 235",((ur2*r2m2t(i,j)/urr2,i=1,npnt2),j=1,npnt2)

                write(iwave) ((ur2*r2m2t(i,j)/urr2,i=1,npnt2),j=1,npnt2)
              endif
            else
print *, "Line 240",(xp5/(r1(i)*r1(i)*urr1),i=1,npnt1)

              write(iwave) (xp5/(r1(i)*r1(i)*urr1),i=1,npnt1)
            endif
         else
            jdia=max(1,idia)
            jstart=kmin
            if (mod(jstart,jdia) .ne. ipar) jstart=jstart+1
            nang=(maxleg-jstart)/jdia+1
            mbass=idvr*npnt1*npnt2
            if (idia .eq. -2) mbass=idvr*max2d
print *, "Line 251", mbass, jstart, nang,mbass
            write(iwave) mbass,jstart,nang,mbass
         endif
print *, "Line 254", r1
         write(iwave) r1
         if (idia .gt. -2) write(iwave) r2
!        if (idia .gt. -2) print *, "Line 256", r2
      endif

      if (idia .eq. -2) then
         max2d1=max2d
         max3d1=max3d
         DEALLOCATE(xk2,r2,r2m2t)
      endif

      DEALLOCATE(dnorm1,r1m2,dnorm2,r2m2,bass1,bass2,&
                 y1,wt1,y2,wt2,b,c,hbl1,hbl2,dz1,dz2,tailint2)

      k_flag=0;
      iq=0

!     -------------  start rotational loop here  -------------
      do 40 kk=kd,ku
      if (kk .le. jrot) then
         kz=kk
      else
         kz=1
         kmin=0
         if(.not.zperp) ipar=mod(ipar+jrot,2)
      endif

!     first rewind the scratch files for a calculation with j>0
!     and, if needed, reposition iout2 after set up data.
60      if (kk .gt. kd) then
        if (idia .gt. -2) then
           rewind ieigs1
           rewind ieigs2
           rewind ivecs1
           rewind ivecs2
           rewind ivint
           rewind iband
        endif
        rewind intvec
        if (zvec) then
          rewind iout1
          rewind iout2
          do 45 ii=1,4
             read(iout2)
   45     continue
        endif
      endif

      realkz = dble(kz)
!     tswalf is the exact sum of weights for gauss-jacobi integration
      tswalf= x2**(kz+kz+1)/dble(kz+1)
      do 30 ia=1,kz
         tswalf=tswalf*dble(ia)/dble(kz+ia+1)
   30 continue

      if (zladd .or. kz .eq. kd) then
         nidvr = idvr
         nang  = nalf
         nang2 = nalf2
         lincr = kz
      else
         lincr = 0
         if (idia .ne. 2) then
            nidvr = idvr - kz
            nang  = nalf - kz
            nang2 = (nang+1)/2
         else if (ipar .eq. 0) then
            if(mod(kz,2).eq.1) kkz0 = kkz0 + 2
            nidvr = idvr - kkz0/2
            nang  = nalf - kkz0
            nang2 = (nang+1)/2
         else
            if(mod(kz,2).eq.0 .and. kz.gt.0) kkz12 = kkz12 + 2
            nidvr = idvr - kkz12/2
            nang  = nalf - kkz12
            nang2 = (nang+1)/2
         endif
      endif
      if (.not. zladd) then
         if (ztheta) then
            npnta=nidvr
         else
            npntc=nidvr
         endif
      endif

      if (ztwod) then
         xalf(1) = fixcos
         goto 333
      endif

      if(.not. zperp) then
        call jacobi(nang,nang2,xalf,walf,realkz,realkz,cswalf,tswalf)
        write(6,1000) nang,kz,(xalf(ii),walf(ii),ii=1,nang2)
 1000   format(//i8,' point Gauss-associated Legendre integration with',&
             ' k =',i3//5x,'integration points',11x,'weights',&
              //(f23.15,d25.12))
        write(6,1010) cswalf,tswalf
 1010   format(/4x,'Computed sum of weights',d22.15,&
              /4x,'Exact    sum of weights',d22.15//)
          if (abs((cswalf-tswalf)/tswalf) .gt. toler) then
             write(6,910)
  910       format(//5x,'Points & weights in error, adjust algorithm'//)
             stop
          endif
        call allpts(xalf,walf,nang,nang2)
        alf = x0
      else
        realj = DBLE(jrot)
        alf = dsqrt(0.5d0*(((realj)**2+realj)-realkz**2))
        bet = alf
        call jacbasis(xalf,walf,2*nang2,alf,bet)
        xalf=-xalf
        write(6,1020) nang,alf,(xalf(ii),walf(ii),ii=1,nang2)
 1020   format(//i8,' point Gauss-Jacobi integration with',&
             ' alpha = beta =',f7.3//5x,'integration points',11x,'weights',&
              //(f23.15,d25.12))
      endif

      if (zvec) write(iout2) xalf

      if (.not.zperp) then
!     set up Legendre polynomials for the transformation matrix
!     for z in plane case
        call asleg(pleg,maxleg,xalf,nidvr,kz,lincr)
      else
!     set up Jacobi polynomials for the transformation matrix
!     for z perpendicular case
        call jac_basis(nidvr,maxleg,alf,bet,xalf,pleg)
        do  i=1,nidvr
            walf(i) = sqrt(walf(i))
        enddo
      endif


!     save polinomials for rotlev3 or rotlev3b, or rotlev3z
      if (ztran) then
print *, "iwave written around lines 390"
!print *, "xalf = ",xalf, "kz=",kz, "maxleg=",maxleg, "nidvr=",nidvr, "lincr=",lincr
!print *, "((pleg(i,j)*walf(j),i=0,maxleg),j=1,nidvr) = ", ((pleg(i,j)*walf(j),i=0,maxleg),j=1,nidvr)
        write(iwave) kz,maxleg,nidvr,lincr
        write(iwave) xalf

! I have changed the order of these, which may cause problems when reading the file elsewhere - however, I didn't know how else to get hte nidvr information before the xalf (which requires teh length of nidvr (or at least idvr).
        write(iwave) ((pleg(i,j)*walf(j),i=0,maxleg),j=1,nidvr)
        write(iwvpb) ((pleg(i,j)*walf(j),i=0,maxleg),j=1,nidvr)
      endif
      
!     build the transformed angular momentum matrix xlmatr;
      ipar0=0
      if (idia .eq. 2 .and. ipar .eq. 1) ipar0=1
      call lmatrx(xlmatr,pleg,walf,kz,ipar0,nidvr,lincr,alf)


  333 continue

      if (zperp) write(6,1152) kz, iq
 1152 format(/5x,'Calculate block for K =', i3,/5x,'q symmetry =', i3,/)

!     for ab2 molecules in radau coordinates, use separate main
!     driving routine
      if (idia .le. -2) then
         if (zrot) then
            if (kz .gt. kd .and. .not.zperp) ipar=mod(ipar+1,2)
            if (ipar .gt. 0) then
               max2d=max2d2
               max3d=max3d2
            else
               max2d=max2d1
               max3d=max3d1
            endif
         endif
         if (zperp) then
           call mkmain(xk1,r1m2t,xlmatr,r1,xalf,kz,plegw)
         else
           call nfmain(xk1,r1m2t,xlmatr,r1,xalf,kz)
         endif    
      else
         call jhmain(xk1,xk2,xlmatr,r1,r2,xalf,r2m2t,kz)
      endif
      
!     calculate block k=1, iq=1 for z-perpendicular case
      if (zperp .and. kk .eq. 1 .and. k_flag .eq. 0) then
        kz=kk
        iq=1
        k_flag=1
        go to 60
      endif
      iq=0
      
   40 continue

      DEALLOCATE(xalf,walf,xlmatr,pleg,xk1,r1)
      if (zperp) DEALLOCATE(jxcos,jwalf,sjwalf,pjac)
      if (idia .gt. -2) DEALLOCATE(r2,xk2,r2m2t)
      return
      end


