
!##########################################################################

      subroutine jhmain(xk1,xk2,xlmatr,r1,r2,xalf,r2m2t,kz)

!     this routine controls the dvr calculation in all cases except
!     symmetrised radau coordinates.
!     written by james henderson

      implicit real*8(a-h,o-y),logical(z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot

      real*8, dimension(npnt1) :: r1
      real*8, dimension(npnt2) :: r2
      real*8, dimension(idvr) :: xalf
      real*8, dimension(idvr,idvr) :: xlmatr
      real*8, dimension(npnt1,npnt1) :: xk1
      real*8, dimension(npnt2,npnt2) :: xk2
      real*8, dimension(npnt2,npnt2) :: r2m2t
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: eigs2
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: ham1
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: hband
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eigs1d
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig1
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::vecs1d
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham2
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig2
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham3
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::cint
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::cintp
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eigs2d
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eval
      REAL*8, ALLOCATABLE, DIMENSION(:) ::evall
      REAL*8, ALLOCATABLE, DIMENSION(:,:) :: vecs1l
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::vecs2l
      REAL*8, ALLOCATABLE, DIMENSION(:) ::vecs3l
      REAL*8, ALLOCATABLE, DIMENSION(:,:,:) ::phi
      REAL*8, ALLOCATABLE, DIMENSION(:,:,:) ::psi 
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: iv1
      INTEGER, ALLOCATABLE, DIMENSION(:) ::ndim2d
      INTEGER, ALLOCATABLE, DIMENSION(:) ::iv2
      INTEGER, ALLOCATABLE, DIMENSION(:,:) ::iv1l
      INTEGER, ALLOCATABLE, DIMENSION(:) ::iv2l
      INTEGER, ALLOCATABLE, DIMENSION(:) :: ndim2l

      ALLOCATE(ham1(ndima,ndima),eigs1d(max2d),&
               eig1(ndima),iv1(ndimc,ndimb),vecs1d(max2d,ndima))

      do l1=1,idvr
         do l2=1,idvr
            write(39,*)l1,l2,xlmatr(l1,l2)
         end do
      end do


!      xlmatr=1.d0
      do l=1,idvr
!         xlmatr(l,l)=1.d0
      end do


      term  = dble(jrot * jrot + jrot - (2 * kz * kz))
!     construct the one-dimensional hamiltonian matrix h1(npnta,npnta')
!     for each npntb and npntc, then solve by diagonalisation.
      icall = 0
      nsum = 0
      do 10 ione = 1,npntc
      nham2 = 0
      do 20 itwo = 1,npntb

      if (zr2r1) then
         call mkham1(ham1,xlmatr,ione,itwo,term,r1,r2,xalf,xk1,xk2,kz)
      else
         call mkham1(ham1,xlmatr,itwo,ione,term,r1,r2,xalf,xk1,xk2,kz)
      endif

!     diagonalise each block, saving the eigenvalues and vectors that
!     fall below the cut-off energy emax1.
      call diag_dac(ham1,ndima,npnta,eig1)
      call cut1d(ham1,eig1,iv1(ione,itwo),eigs1d,vecs1d,nham2,icall)
   20 continue
!      write(6,985) nham2
  985 format(5x,' nham2 = ',i4)
      nsum = nsum + nham2
      if (ione .eq. npntc) write(6,986) nsum
  986 format(/5x,' sum = ',i5)
      if (nham2 .gt. 0) then
!        dump the 1d eigenavlues & vectors to disk for each ione
         call outrow(eigs1d,nham2,ieigs1)
         do 2 ka=1,npnta
         if (zvec) call outrow(vecs1d(1,ka),nham2,iout2)
         call outrow(vecs1d(1,ka),nham2,ivecs1)
    2    continue
      endif
   10 continue

      if (zvec) write (iout1) iv1


do i=1,max2d
print *, "i=",i,"eig1(i)=",eigs1d(i)
end do

      DEALLOCATE(ham1,eig1)
      ALLOCATE(ndim2d(ndimc),eigs2(max2d,ndimc),&
               ham2(max2d,max2d),eig2(max2d),iv2(ndimc),eigs2d(max3d))
      
!     now want to make the two-dimensional ham2(npntb,npntb',i,i'),
!     where i runs over the selected ham1(npnta,npnta') solutions.
      rewind ivecs1
      rewind ieigs1
      icall = 0
      low3d = 0
!     if zall then set zcut for convenience...
      if (zall) zcut = .true.
      do 31 ione = 1,npntc
      nham2 = 0
!     recall the size of ham2
      do 3 itwo = 1,npntb
      nham2 = nham2 + iv1(ione,itwo)
    3 continue
      ndim2d(ione) = nham2

      if ( nham2 .gt. 0 ) then
        call mkham2(ham2,eigs1d,vecs1d,xk1,xk2,iv1,ione,nham2)
        if (.not. ztwod) then
           call diag_dac(ham2,max2d,nham2,eig2)
        else
           call diag3d(ham2,nham2,eig2,kz)
           return
        endif

        if (zcut) then
          call cut2d(ham2,eig2,iv2(ione),nham2,low3d,icall)
        else
          do 4 ii = 1,nham2
            call outrow(ham2(1,ii),nham2,intvec)
            eigs2(ii,ione) = eig2(ii)
    4     continue
          low3d = max3d
        endif

     else
        iv2(ione) = 0
     endif
     
      if (ione .eq. npntc .and. zcut) write(6,987)  low3d,emax2
  987 format(/i14,' eigenvalues selected below ',d20.10)

   31 continue

      if (.not. zcut) call choose(eigs2,ndim2d,ham2,iv2,low3d)

      if (zvec) write (iout1) iv2

!      call timer

      nham3 = low3d

!     save the required bits to disk if zdiag = .false.
!     first open the required disk file
      if (.not. zdiag) then
        open(unit=idiag1,form='unformatted')
        open(unit=idiag2,form='unformatted')
        write(idiag1) npnta,npntb,npntc,max2d,nham3,zcut
        call ioutro(iv1,ndimb*ndimc,idiag1)
        call ioutro(iv2,ndimc,idiag1)

!       need also the 2-d eigenvalues to build the final hamiltonian
        if (zcut) then
          rewind ieigs2
          do 55 i1 = 1,npntc
          iv = iv2(i1)
          if (iv .gt. 0) call getrow(eig2,iv,ieigs2)
          if (iv .gt. 0) call outrow(eig2,iv,idiag1)
   55     continue
        else
          call outrow(eigs2,max2d*ndimc,idiag1)
        endif

      endif


do i=1,max3d
print *, "i=",i,"eig2(i)=",eig2(i),eig2(i)*219476.329477d0
end do


      DEALLOCATE(eigs1d,eig2)
      ALLOCATE(hband(max2d,nham3),&
               cint(ndima*ndimb,max2d),cintp(ndima*ndimb,max2d))

      call mkham3(nham3,ham2,cint,cintp,iv1,iv2,ndim2d,&
                  vecs1d,xk1,xk2,eigs2d,eigs2,hband,xlmatr,r1,r2,&
                  r2m2t,term)

      DEALLOCATE(ham2,cint,cintp,vecs1d,hband,iv1)
      ALLOCATE(ham3(nham3,nham3))

      call loadh(ham3,nham3,iv2,ndim2d,eigs2d,eigs2)

      DEALLOCATE(iv2,ndim2d,eigs2d,eigs2)
      ALLOCATE(eval(nham3))

      call diag3d(ham3,nham3,eval,kz)

      DEALLOCATE(ham3,eval)

!.....finally, can compute the actual wavefunction amplitude at
!.....the grid points if needed.
      if (ztran) then
         ALLOCATE(evall(neval),&
                 iv1l(ndimc,ndimb),iv2l(ndimc),ndim2l(ndimc),&
                 vecs1l(max2d,ndima),vecs2l(max2d,max2d),vecs3l(nham3),&
                 phi(nham3,ndima,ndimb),psi(idvr,npnt1,npnt2))

         call trans(iv1l,iv2l,ndim2l,vecs1l,&
                    vecs2l,vecs3l,phi,psi,evall,nham3,r1,r2)

         DEALLOCATE(iv1l,iv2l,ndim2l,vecs1l,vecs2l,vecs3l,phi,psi,evall)
      endif

      return
      end



!#########################################################################
      subroutine mkham1(ham1,xlmatr,i1,i2,term,r1,r2,xalf,xk1,xk2,kz)

      implicit real*8 (a-h,o-y), logical (z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1
      common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2

      real*8, dimension(ndima,ndima) :: ham1
      real*8, dimension(idvr,idvr) :: xlmatr
      real*8, dimension(npnt1) :: r1
      real*8, dimension(npnt2) :: r2
      real*8, dimension(idvr) :: xalf
      real*8, dimension(npnt1,npnt1) :: xk1
      real*8, dimension(npnt2,npnt2) :: xk2

      data x0/0.0d0/,xp5/0.50d0/,x1/1.0d0/

!     zero ham1
      ham1 = x0
!     zero rotational excitation term for j=0 cases
      wterm = x0
!.....theta first
      if (ztheta) then

         w1gama = xp5 / (r1(i1)*r1(i1)*ur1)
         w2beta = xp5 / (r2(i2)*r2(i2)*ur2)
         wsum = w1gama + w2beta

         if (jrot .gt. 0) then
            if (zembed) then
!              have term * r2**(-2) term
               wterm = term * w2beta * ur2/urr2
            else
!              have term * r1**(-2) term
               wterm = term * w1gama * ur1/urr1
            endif
!     extra NBO term if vib mass .ne. rot mass
            if (kz .gt. 0) then
               s1 = ur1/urr1-x1
               s2 = ur2/urr2-x1
               w3 = dble(kz*kz) * (s1*w1gama + s2*w2beta)
            endif
         endif
      endif

      do 10 k = 1,npnta
      if (ztheta) then
         call potv(v,r1(i1),r2(i2),xalf(k))
!         write(91,1098)r1(i1),r2(i2),xalf(k),v
!         1098 format(3(2x,f16.10),1x,d25.18)
      else
         if (zr2r1)then
            call potv(v,r1(i2),r2(k),xalf(i1))
          else
            call potv(v,r1(k),r2(i1),xalf(i2))
         endif
      endif
      if (ztheta) then
         if (kz .gt. 0) v = v + w3/(x1-xalf(k)**2)
         ham1(k,k) = v + wterm
         do 20 kp= 1,k
         ham1(k,kp) = ham1(k,kp) + xlmatr(k,kp)*wsum
   20    continue    

      else
         if (jrot .gt. 0) then
            if (zembed) then
              if (zr2r1)then
                 wterm = (term * xp5) / (r2(k)*r2(k)*ur2)
              else
                 wterm = (term * xp5) / (r2(i1)*r2(i1)*ur2)
              endif
            else
              if (zr2r1)then
                 wterm = (term * xp5) / (r1(i2)*r1(i2)*ur1)
              else
                 wterm = (term * xp5) / (r1(k)*r1(k)*ur1)
              endif
            endif
         endif
         ham1(k,k) = v + wterm

         if (zr2r1) then
            do 30 kp= 1,k
            ham1(k,kp) = ham1(k,kp) + xk2(k,kp)
   30       continue
         else
            do 40 kp= 1,k
            ham1(k,kp) = ham1(k,kp) + xk1(k,kp)
   40       continue
         endif
      endif
   10 continue

      return
      end

!####################################################################
      subroutine mkham2(ham2,eigs1d,vecs1d,xk1,xk2,iv1,ione,nham2)
      implicit real*8 (a-h,o-y), logical (z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot

      real*8, dimension(max2d,max2d) :: ham2
      real*8, dimension(npnt2,npnt2) :: xk2
      dimension iv1(ndimc,ndimb)
      real*8, dimension(max2d) :: eigs1d
      real*8, dimension(max2d,ndima) :: vecs1d
      real*8, dimension(npnt1,npnt1) :: xk1

!     zero ham2
      ham2 = 0.0d0

      do 10 i3= 1,npnta
         call getrow(vecs1d(1,i3),nham2,ivecs1)
         do 20 j = 1,nham2
           do 30 k = 1,j
             ham2(j,k) = ham2(j,k) + vecs1d(j,i3)*vecs1d(k,i3)
   30      continue
   20    continue
   10 continue
!     must now multiply by xk1 or xk2
      ivbsm = 0
      do 50 itwo = 1,npntb
        ivbpsm = 0
        ivb = iv1(ione,itwo)
        do 60 itwop = 1,itwo
          if (ztheta) then
            if (zr2r1) then
              xkterm = xk2(itwo,itwop)
            else
             xkterm = xk1(itwo,itwop)
            endif
          else
           if (zr2r1) then
           xkterm = xk1(itwo,itwop)
        else
           xkterm = xk2(itwo,itwop)
        endif
      endif
      ivbp = iv1(ione,itwop)
      do 70 j  = 1,ivb
        ind1 = ivbsm + j
        do 77 jp = 1,ivbp
          ind2 = ivbpsm + jp
          ham2(ind1,ind2) = ham2(ind1,ind2) * xkterm
   77   continue
   70 continue
      ivbpsm = ivbpsm + ivbp
   60 continue
      ivbsm = ivbsm + ivb
   50 continue

!     now add the 1-d eigenvalues along the diagonal
      call getrow(eigs1d,nham2,ieigs1)
      do 80 nn = 1,nham2
         ham2(nn,nn) = ham2(nn,nn) + eigs1d(nn)
   80 continue
      return
      end

!################################################################################
      subroutine mkham3(nham3,ham2,cint,cintp,iv1,iv2,ndim2d,&
                        vecs1d,xk1,xk2,eigs2d,eigs2,hband,xlmatr,&
                        r1,r2,r2m2t,term)

!     build the final 3-d hamiltonian matrix.

      implicit real*8 (a-h,o-y), logical (z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1
      common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2

      real*8, dimension(ndima*ndimb,max2d) :: cint
      real*8, dimension(max2d,max2d) :: ham2
      dimension iv1(ndimc,ndimb),iv2(ndimc),ndim2d(npntc)
      real*8, dimension(max2d,ndima) :: vecs1d
      real*8, dimension(ndima*ndimb,max2d) :: cintp
      real*8, dimension(npnt1,npnt1) :: xk1
      real*8, dimension(nham3) :: eigs2d
      real*8, dimension(npnt2,npnt2) :: xk2
      real*8, dimension(max2d,nham3) :: hband
      real*8, dimension(max2d,ndimc) :: eigs2
      real*8, dimension(idvr,idvr) :: xlmatr
      real*8, dimension(npnt1) :: r1
      real*8, dimension(npnt2) :: r2
      real*8, dimension(nham3) :: work3
      real*8, dimension(npnt2,npnt2) :: r2m2t

      data xp5/0.50d0/

!     if zdiag = .false. want eigs2d now
      if (.not. zdiag) then
        rewind ieigs2
        neig = 0
        do 184 ione = 1,npntc
          iv = iv2(ione)
          if (.not. zcut) then
            do 19 ii = 1,iv
              neig = neig + 1
              eigs2d(neig) = eigs2(ii,ione)
   19       continue
          endif
          if (iv .gt. 0 .and. zcut) call getrow(eigs2d,iv,ieigs2)
  184   continue
      endif

!     first do the intermediate transformation
      ncint = npnta*npntb
      ndimt = ndima*ndimb
      rewind ivecs2
      rewind ivecs1

      do 10 ione = 1,npntc
!        recall the size of the 2-d vectors
         nham2=ndim2d(ione)
         if (nham2 .eq. 0) goto 10
         cint = 0.0d0
!     bring back the 1-d vectors for each i1
         do 23 kk = 1,npnta
           if (iv2(ione) .gt. 0) then
             call getrow(vecs1d(1,kk),nham2,ivecs1)
           else
             read (ivecs1)
           endif
   23    continue
         do 20 j = 1,iv2(ione)
!     bring back the 2-d vectors for each npntc
           call getrow(ham2(1,j),nham2,ivecs2)
           ind2 = 0
           do 30 k = 1,npnta
             ind1 = 0
             do 40 itwo = 1,npntb
               ind2 = ind2 + 1
               do 50 i = 1,iv1(ione,itwo)
                 ind1 = ind1 + 1
                 cint(ind2,j) = cint(ind2,j) + ham2(ind1,j)*vecs1d(ind1,k)
   50          continue
   40        continue
   30      continue
           if (.not. ztheta) then
             in2 = 0
             do 37 ia = 1,npnta
               do 47 ib = 1,npntb
                 if (zr2r1) then
                    ii1 = ib
                    ii2 = ia
                 else
                    ii1 = ia
                    ii2 = ib
                 endif
                 w1 = xp5 / (r1(ii1)*r1(ii1)*ur1)
                 w2 = xp5 / (r2(ii2)*r2(ii2)*ur2)
                 wsum = w1 + w2
                 in2 = in2 + 1
                 cint(in2,j) = cint(in2,j)*sqrt(wsum)
   47          continue
   37        continue
           endif
   20    continue
!     store cint on disk for each npntc
         if (iv2(ione) .gt. 0) call outrow(cint,ndimt*iv2(ione),ivint)
   10 continue

!     now do the second part of the transformation
!      endfile ivint


      rewind ivint
      if (ztheta) then
        rm2t = 0.0d0
        length = 0
        do 61 ione = 1,npntc
!        set the hband to zero
         hband = 0.0d0
         ivsm = 0
         nham2 = ndim2d(ione)
         if (nham2 .eq. 0) goto 61
         iv = iv2(ione)
         if (iv .gt. 0) call getrow(cint,ndimt*iv,ivint)
         rewind ivint
         ivpsm = 0
         do 51 ionep = 1,ione
           if (.not.zquad2) then
             if (ione .eq. ionep) then
               d1r2 = r2m2t(ione,ionep)
               d2r2 = xp5 / (r2(ione)*r2(ione)*ur2)
               rm2t = d1r2 - d2r2
             else
               rm2t = r2m2t(ione,ionep)
             endif
           endif
           if (zr2r1) then
             xkterm = xk1(ione,ionep)
           else
             xkterm = xk2(ione,ionep)
             if(jrot .gt. 0 .and. .not. zquad2 .and. zembed) xkterm = xkterm + term*rm2t
           endif
           ivp = iv2(ionep)
           if (ivp .gt. 0) call getrow(cintp,ndimt*ivp,ivint)
           do 41 j=1,iv
             ind1 = ivsm + j
             do 31 jp =1,ivp
               ind2 = ivpsm + jp
               ind3 = 0
               do 81 k = 1,ncint
                  ind3 = ind3 + 1
                  hband(ind1,ind2) = hband(ind1,ind2)&
                                     + cint(ind3,j)*cintp(ind3,jp)*xkterm
   81          continue
               ind3 = 0
               do 83 na = 1,npnta
                  do 82 k = 1,npntb
                    ind3 = ind3 + 1
                    ind4 = k
                    do 84 nap = 1,npnta
                       hband(ind1,ind2) = hband(ind1,ind2) + cint(ind3,j)*cintp(ind4,jp)&
                                           * xlmatr(na,nap) * rm2t
                       ind4 = ind4 + npntb
   84               continue
   82             continue
   83          continue
   31        continue
   41      continue
           ivpsm = ivpsm + ivp
   51    continue
         if (zdiag) then
           do 71 jj=1,ind2
             if (iv .gt. 0) call outrow(hband(1,jj),iv,iband)
   71      continue
           ndim2d(ione)=ind2
         else
           do 93 isave = 1,iv
             length = length + 1
             do 94 jsave = 1,ind2
               work3(jsave)= hband(isave,jsave)
   94        continue
             work3(length) = work3(length) + eigs2d(length)
             call outrow(work3,length,idiag2)
   93      continue
         endif
   61 continue

      else
        length = 0
        do 66 ione = 1,npntc
!         set the hband to zero
          hband=0.0d0
          ivsm = 0
          if (ndim2d(ione) .eq. 0) goto 66
          iv = iv2(ione)
          if (iv .gt. 0) call getrow(cint,ndimt*iv,ivint)
          rewind ivint
          ivpsm = 0
          do 56 ionep = 1,ione
            xkterm = xlmatr(ione,ionep)
            ivp = iv2(ionep)
            if (ivp .gt. 0) call getrow(cintp,ndimt*ivp,ivint)
            do 46 j=1,iv
              ind1 = ivsm + j
              do 36 jp =1,ivp
                ind2 = ivpsm + jp
                ind3 = 0
                do 86 k = 1,ncint
                  ind3 = ind3 + 1
                  hband(ind1,ind2) = hband(ind1,ind2)&
                               + cint(ind3,j)*cintp(ind3,jp)*xkterm
   86           continue
   36         continue
   46       continue
            ivpsm = ivpsm + ivp
   56     continue
          if (zdiag) then
            do 76 jj=1,ind2
              if (iv .gt. 0) call outrow(hband(1,jj),iv,iband)
   76       continue
            ndim2d(ione)=ind2
          else
            do 95 isave = 1,iv
              length = length + 1
              do 96 jsave = 1,ind2
                work3(jsave)= hband(isave,jsave)
   96         continue
            work3(length) = work3(length) + eigs2d(length)
            call outrow(work3,length,idiag2)
   95       continue
          endif
   66  continue
      endif

      if (.not. zdiag) then
         write(6,1080)
 1080    format(/5x,'hamiltonian written to disk - not diagonalised')
         write(6,1081) idiag2
 1081    format(/5x,'hamiltonian bands written to stream idiag =',i4/)
         stop
       endif
       return
       end

      subroutine loadh(ham3,nham3,iv2,ndim2d,eigs2d,eigs2)

!     load the final 3-d hamiltonian matrix.

      implicit real*8 (a-h,o-y), logical (z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot

      dimension iv2(ndimc),ndim2d(npntc)
      real*8, dimension(nham3,nham3) :: ham3
      real*8, dimension(nham3) :: eigs2d
      real*8, dimension(max2d,ndimc) :: eigs2

      ham3 = 0.0d0
      rewind iband
      ivsm = 0
      do 62 ione = 1,npntc
        if (ndim2d(ione) .eq. 0) goto 62
        iv = iv2(ione)
        do 72 jj=1,ndim2d(ione)
          if (iv .gt. 0) call getrow(ham3(ivsm + 1,jj),iv,iband)
   72   continue
        ivsm = ivsm + iv
   62 continue

!     now put the 2-d eigensolutions along the diagonal

      rewind ieigs2
      nn = 0
      do 183 ione = 1,npntc
       iv = iv2(ione)
       if (.not. zcut) then
       do 9 ii = 1,iv
          eigs2d(ii) = eigs2(ii,ione)
    9  continue
       endif
       if (iv .gt. 0 .and. zcut) call getrow(eigs2d,iv,ieigs2)
       do 185 i = 1,iv
         nn = nn + 1
         ham3(nn,nn) = ham3(nn,nn) + eigs2d(i)
  185  continue
  183 continue

      if (zpham) call wrtham(ham3,nham3)

      return
      end


