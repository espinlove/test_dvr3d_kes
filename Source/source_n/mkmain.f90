
!########################################################################
      subroutine mkmain(hr,r1m2t,htheta,r,theta,kz,plegw)

!     this routine controls the dvr calculation in the case of
!     symmetrised radau coordinates with z axes perpendicular to the 
!     molecular plane.
!     written by max kostin, 2001.

      implicit real*8(a-h,o-y),logical(z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
                                      
      real*8, dimension(npnt,npnt) :: hr
      real*8, dimension(npnt,npnt) :: r1m2t
      real*8, dimension(nalf,nalf) :: htheta
      real*8, dimension(nalf,nalf) :: plegw
      real*8, dimension(npnt) :: r
      real*8, dimension(nalf) :: theta
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham2
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig2,eigtmp
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::vecs2d
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eigs2d
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham3
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig3
      INTEGER, ALLOCATABLE, DIMENSION(:,:) ::iv2
      INTEGER, ALLOCATABLE, DIMENSION(:) ::nv2

      data autocm /2.19474624d+05/

      ALLOCATE(ham2(max2d,max2d),eig2(max2d),iv2(2,nalf),&
               vecs2d(max2d,max3d),eigs2d(max3d),nv2(nalf))
      if (.not.zcut) ALLOCATE(eigtmp(nalf*max2d))

      emaxau=emax2/autocm

! no need for the 1d diagonalisations as there is no possiblity of
! truncation as the symmetry would be broken.
      write(6,130)
      call nftim('beginning of 2d loop mkmain')

!      do 30 igamma = nalf,1,-1     
      do 30 igamma = 1,nalf     
        nv2(igamma) = (npnt*(npnt+1-ipar*2))/2      
        nham2 = nv2(igamma)
        call z_blc2d1(theta(igamma),r,hr,ham2,nham2,&
                          term,term2,kz)

!        call diag_dac(ham2,nham2,nham2,eig2)
        call diag_rrr(ham2,nham2,nham2,eig2,zcut,emaxau)

        if (.not. zcut) then
          call choosr(igamma,nham2,eig2,ham2,iv2,eigs2d,&
                      vecs2d,nv2,eigtmp)

          if(igamma .eq. nalf .and. .not. zpmin)&
              write(6,110) (itmp, iv2(2,itmp),itmp=1,nalf)
        else
           call cut2dr(igamma,nham2,eig2,ham2,iv2,eigs2d,vecs2d)
          if (.not. zpmin) write(6,110) igamma, iv2(2,igamma)

        endif
30    continue

        nbass=max2d*nalf
      if (ztran) then
         write(iwave) nalf,nbass
         write(iwave) (iv2(2,ii),ii=1,nalf)
      endif
!      call testiv(iv2,nbass)

      nham3 = iv2(1,nalf) + iv2(2,nalf) - 1
      if (.not. zpmin) write(6,120) nham3
      call nftim('end of 2d loop')
      write(6,160)

      if (.not.zcut) DEALLOCATE(eigtmp)
      ALLOCATE(ham3(nham3,nham3))

      call bloc3d(htheta,r1m2t,ham3,eigs2d,vecs2d,&
                    iv2,nv2,ham2,nham3,r)

      DEALLOCATE(ham2,eig2,eigs2d,nv2)

      rr=0.d0
      zz=.true.

! writing J=0 output for dipole in stream idip
      if (jrot.eq.0.or.(jrot.eq.1.and.kz.eq.0)) then
     write(idip)ipar,ipar,ipar,npnt1,ipar,jrot,ipar,min(neval,nham3),0,ipar !1
         write(idip)zz,zz,zz,rr,rr,rr,rr,rr,zz                 !2
         write(idip)rr,rr,rr,rr,rr,rr                          !3
         write(idip)                                           !4
         write(idip)r                                          !5
     write(idipd)ipar,ipar,ipar,npnt1,ipar,jrot,ipar,min(neval,nham3),0,ipar !1
         write(idipd)zz,zz,zz,rr,rr,rr,rr,rr,zz                 !2
         write(idipd)rr,rr,rr,rr,rr,rr                          !3
         write(idipd)                                           !4
         write(idipd)r                                          !5
      end if
 
      call nftim('end of 3d ham building')
      write(6,170)
      ALLOCATE(eig3(nham3))

      call diag3d(ham3,nham3,eig3,kz)

      call nftim('end of diagonalising 3d')

      if (ztran) call transr2(iv2,vecs2d,ham3,eig3,nham3,nbass,kz,r)

! writing last line for J=0 output for dipole in stream idip
      if (jrot.eq.0.or.(jrot.eq.1.and.kz.eq.0)) then
         write(idip) theta
         write(idipd) theta
      end if

      DEALLOCATE(iv2,vecs2d,ham3,eig3)
      return
110   format(5x,'for gamma = ',i2,' selected ',i3,' energies.')
120   format(25x,'total = ',i5)
130   format(5x,'starting the 2d calculations.')
160   format(5x,'building the 3d hamiltonian.')
170   format(5x,'diagonalising the 3d hamiltonian.')  
    
      end 

