program readwffromjhmain 

! Fort.26 as input. Returns Psi(i,j) 
! i - enumerates grid points 
! j - enumerates solutions. Version for J=0 
! Emil Zak, 14 Sep 2016.

implicit none

  integer ifiledvr,iang2,ibass2,meval2,i,Npts,ibra,iket,idia,ipar,nalf,npnt,npnt2,jrot,kmin,neval,j,k
  integer mbass,jstart,nang
  double precision, allocatable:: e(:), d(:)
  double precision g1,g2,we1,re1,diss1,xmass,re2,diss2,we2 ! Basis set and coordinate system parameters
  double precision xm(3),dum2,dum3,walf,a
  logical zembed,zmors1,zmors2,zncor,zquad2 ! Input control words
  integer kz,maxleg,nidvr,lincr ! Gauss-Legenre control parameters
double precision, allocatable :: cpsi(:),psi(:,:),r(:),r2(:),ang(:,:),ang_un(:),xalf(:)
double precision, allocatable :: T(:,:), pleg(:,:)
  integer, allocatable :: iv2(:,:)

!  100 format(A8,A8,I5,A8,I5,A8,I5,A8,I5,A8,I5,A8,I5,A8,I5,A8,I5)
!  101 format(A8,A8,I5,A8,I5,A8,I5,A8,F14.6,A8,F14.6,A8,F14.6)
!  102 format(A8,A8,F10.4,A8,F10.4,A8,F10.4,A8,F10.4,A8,F10.4,A8,F10.4)
!  103 format(A8,I8,A8,I8,A8,I8,A8,I8)

  ifiledvr=26 ! File stream in. DVR3DJZ output.
  open(12,file='wf_dvr.dat') ! Output file - print wavefunction

  read(ifiledvr) idia,ipar,nalf,npnt,npnt2,jrot,kmin,neval
  write(*,*) "Line1:", "IDIA= ",idia,"IPAR= ", ipar,"NALF= ",nalf,"NPNT1= ",&
 npnt,"NPNT2= ",npnt2,"J= ",jrot,"KMIN= ",kmin,"NEVAL= ",neval

	allocate(r(npnt),r2(npnt2))
	allocate(ang_un(nalf))
	allocate(ang(nalf,jrot+1))
	allocate(iv2(2,nalf))

  read(ifiledvr) zembed,zmors1,zmors2,xmass,g1,g2
!  write(*,101) "Line 2: ","Zembed=",zembed, "Zmors1=",zmors1, "Zmors2=",zmors1,"Xmass=", xmass, "g1=",g1, "g2=", g2
  write(*,*) "Line 2: ","Zembed=",zembed, "Zmors1=",zmors1, "Zmors2=",zmors2,"Xmass=", xmass, "g1=",g1, "g2=", g2

  read(ifiledvr) re1,diss1,we1,re2,diss2,we2
!  write(*,102) "Line 3: ","Re1=",re1,"Diss1=",diss1,"we1=",we1,"Re1=",re1,"Diss1=",diss1,"we1=",we1
  write(*,*) "Line 3: ","Re1=",re1,"Diss1=",diss1,"we1=",we1,"Re1=",re2,"Diss1=",diss2,"we1=",we2


do k=1,jrot+1
  
  write(*,*)
  write(*,'(A20,A5,I3,A20)') "########################",  "k = ",k-1, "    ########################"
  write(*,*)

read(ifiledvr) mbass,jstart,nang,mbass
!  write(*,*) "Line 6: Gauss-Legendre control parameteres:"
  write(*,*) "mbass = ", mbass, "jstart = ", jstart, "nang = ", nang, "mbass = ", mbass


read(ifiledvr) r
write(*,*) "Radial quadrature points, r1 coord, Gauss-Laguerre quad:"
write(*,'(F12.6)')  r

read(ifiledvr) r2
write(*,*) "Radial quadrature points, r2 coord, Gauss-Lobatto quad:"
write(*,'(F12.6)')  r2
! if (k.eq.1) allocate(T(maxleg+1,nidvr))

!  write(30,*) ((T(i,j),i=0,maxleg),j=1,nidvr)
!  write(30,*)
 

!  read(ifiledvr) iang2,ibass2
!  write(*,*) "Line 8: ", "iang=", iang2,"Nbass=Npts= ",ibass2
  
!   npts=ibass2
!   if(k.eq.1) allocate(cpsi(npts))
!   if(k.eq.1) allocate(psi(npts,neval))

  read(ifiledvr)  kz,maxleg,nidvr,lincr
  write(*,*) "Line 9: ","kz,maxleg,nidvr,lincr= ",  kz,maxleg,nidvr,lincr

!allocate(pleg(0:maxleg,nidvr))
allocate(xalf(nidvr))!,walf(nidvr))
allocate(T(maxleg+1,nidvr))

write(*,*) "xalf: "
read(ifiledvr) xalf
do i=0,maxleg
write(*,*) i, xalf(i)
end do


!  104 format(ES16.6,ES16.6,ES16.6,ES16.6,ES16.6,ES16.6,ES16.6)
write(*,'(A90)') "Line 7: DVR transformation matrix for angular coordinate T(i,j)=Pleg(i,j)*walf(j) : "
read(ifiledvr) ((T(i,j),i=0,maxleg),j=1,nidvr)
do i=0,maxleg
do j=1,nidvr
write(*,*) i,j,T(i,j)
end do
end do

 do i=1,1000
   read(ifiledvr) a
    write(*,*), i, a
 end do
  !get energy levels
  if(k.eq.1) allocate(e(neval))
  call getrow(e,neval,ifiledvr)
  write(*,*) "Line 10: energy levels in Hartrees:"
  write(*,'(5F14.6)') e*2.19474624d+05


!  read(ifiledvr) ((iv2(i,j),j=1,nalf),i=1,2)
  write(*,*) "Line 11 : iv2-matrix: "
  write(*,'(2I6)') ((iv2(i,j),j=1,nalf),i=1,2)

 !get the eigenvectors. Number of eigenvectors is equal to meval2=neval
  do i=1, meval2
		
		call getrow(cpsi,ibass2,ifiledvr)

		do j=1,ibass2
			psi(j,i)=cpsi(j)
		end do
  enddo


write(12,*) ((psi(j,i), i=1,neval), j=1,ibass2)
end do

 close(12)

contains



subroutine getrow(row,nrow,iunit)
  integer iunit,nrow
  double precision row(nrow)     
  read(iunit) row
  return
end subroutine getrow


end program
