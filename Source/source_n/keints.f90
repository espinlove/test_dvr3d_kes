

!#######################################################################


      subroutine keint2(hbl,fke,rm2,dnorm,nlim,nmax,npnt,alf)

!     keint2 calculates analytic kinetic energy integrals over r2   #013
!     and moment of intertia integral for spherical oscillator functions

      implicit real*8 (a-h,o-y), logical (z)

      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
      common /rmatreact/ zfinitereg, zrmat, bndr
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1
      common /split2/ re2,diss2,we2,beta2,ur2,urr2,a2,iu2

      integer :: ni, n1, n2
      real*8, dimension(nlim) :: hbl
      real*8, dimension(nlim) :: rm2
      real*8, dimension(*) :: dnorm
      real*8, dimension(nlim) :: tailints


      data x0/0.0d0/,xp5/0.5d0/,x1/1.0d0/
      gam = fke / (alf + xp5)
      do 10 n1=1,npnt
         gam = gam /(dble(n1)+alf+xp5)
   10 continue  
! gam = g(alf+0.5) / g(a+0.5+N+1)
      fact = x1
      fn = x0
      sum = gam
      do 20 n1 = 1,nmax+1
      index = (n1 * (n1+1)) / 2
      do 30 n2 = n1,nmax+1
      rm2(index) = dnorm(n1) * dnorm(n2) * sum
      index = index + n2
   30 continue
      gam = (fn+alf+xp5) * gam
      fn = fn + x1
      fact = fn * fact
      sum = sum + gam / fact
   20 continue
      fact = alf * (alf + x1)
      do 40 index=1,nlim
      hbl(index) = - fact * rm2(index)
   40 continue
      index = 0
      fn = - x1
      do 50 n2=1,nmax+1
      fn = fn + x1
      index=index+n2
!     special case:  n1 = n2
      hbl(index) = hbl(index) + fke * (fn+fn+alf+1.5d0)
!     special case:  n2 = n1 + 1
      if (n2 .gt. 1)&
          hbl(index-1) = hbl(index-1) + fke * sqrt(fn*(fn+alf+xp5))
   50 continue

      if (.not. zprad) return
!     write kinetic energy & inertia integrals if requested
      write(6,500)
  500 format(//5x,'radial kinetic energy matrix calculated',&
                   ' analytically'/)
      call symout(hbl,nmax+1)
      write(6,510)
  510 format(//5x,'moment of inertia matrix calculated analytically'/)
      call symout(rm2,nmax+1)



      return
      end

!###########################################################################
subroutine keints(hbl,fke,nlim,nmax,iu)

!     keints calculates analytic kinetic energy integrals over r    #012
!     for morse oscillator-like functions

implicit real*8 (a-h,o-y), logical (z)

common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot
common /rmatreact/ zfinitereg, zrmat, bndr
real*8, dimension(nlim) ::  hbl

data x0/0.0d0/

if (zfinitereg) then
write(6,*) "This basis set can't be used for a finite region calculation"
!stop
end if

index = 0
do 10 n2=1,nmax+1
do 20 n1=1,n2
index=index+1
if (n1 .eq. n2) then
!             special case:  n1 = n2
hbl(index) = fke * dble(2*(n2-1)*(n2+iu)+iu+1)
else if (n1+2 .eq. n2) then
!                  special case:  n2 = n1 + 2
hbl(index) = - fke * sqrt(dble((iu+n1+1)*(iu+n1))*dble((n1+1)*n1))
else
!           n1+1 = n2 or n2 > n1 + 2  all matrix elements are zero
hbl(index) = x0
endif
20   continue
10 continue
if (.not. zprad) return
!     write kinetic energy integrals if requested
write(6,500)
500 format(//,5x,'radial kinetic energy matrix calculated',&
' analytically',/)
call symout(hbl,nmax+1)
return
end

!#######################################################################





