!###############################################################################
!                     LobattoIntsDVR.f90
! Contains subroutines:
!
! testingLobatto
! AllLobattoKE(KE,npnt2) 
! OlLobattoDVR(result,bra,ket,Ntot)
! KeLobattoDVR(result,bra,ket,Ntot,rpt,w,derLobMat)
! rminus2LobattoDVR(result,bra,ket,Ntot,rpt)
! derLobattoMat(result,Ntot,rpt,w)
! derLobatto(result,n,eta,Ntot,rpt,w)
! LobattoAbsWeights(rpt,w,Ntotp2,a0)
!  
!###############################################################################

!########### SUBROUTINE testingLobatto #########################################
!###############################################################################

subroutine testingLobatto

 implicit none

 integer      :: bra,ket,Ntot,Ntotp2
 real*8       :: OL, KE, rminus2int,a0
 real*8,allocatable,dimension(:)     :: rpt, w
 real*8, allocatable, dimension(:,:) :: derLobMat

 Ntot=6

 Ntotp2=Ntot+2;

 a0=10d0
 allocate(rpt(0:Ntot+1),w(0:Ntot+1))

 call LobattoAbsWeights(rpt,w,Ntotp2,a0)

print *, "Lobatto abscissa and weights"

 if (1==1) then
    do bra=0,Ntot+1
       print *, bra,rpt(bra),w(bra)
    end do
 end if
 allocate(derLobMat(0:Ntot+1,0:Ntot+1))
 call derLobattoMat(derLobMat,Ntot,rpt,w)

print *, ""
print *, "derivative, KE"

 do bra=0,Ntot+1
    do ket=0,Ntot+1
!print *, bra,ket, derLobMat(bra,ket)
       call OlLobattoDVR(OL,bra,ket,Ntot)
       call KeLobattoDVR(KE,bra,ket,Ntot,rpt,w,derLobMat)
       call rminus2LobattoDVR(rminus2int,bra,ket,Ntot,rpt)
       print *, bra,ket,derLobMat(bra,ket),KE
    end do
 end do

end subroutine testingLobatto

!########### SUBROUTINE AllLobattoKE ###########################################
!###############################################################################

subroutine AllLobattoKE(KE,npnt2)

 implicit none

 integer :: bra,ket,Ntot,Ntotp2,npnt2
 real*8  :: KE(1:npnt2,1:npnt2),a0
 real*8, allocatable, dimension(:)     :: rpt, w
 real*8, allocatable, dimension(:,:) :: derLobMat

 Ntot=npnt2-1

 Ntotp2=Ntot+2;

 a0=10d0
 allocate(rpt(0:Ntot+1),w(0:Ntot+1))

 call LobattoAbsWeights(rpt,w,Ntotp2,a0)

 allocate(derLobMat(0:Ntot+1,0:Ntot+1))
 call derLobattoMat(derLobMat,Ntot,rpt,w)


 do bra=1,Ntot+1
    do ket=1,Ntot+1
       call KeLobattoDVR(KE(bra,ket),bra,ket,Ntot,rpt,w,derLobMat)
    end do
 end do

 return

end subroutine AllLobattoKE

!########### SUBROUTINE OlLobattoDVR ###########################################
!###############################################################################

subroutine OlLobattoDVR(result,bra,ket,Ntot)
 
 implicit none
 
 integer, intent(in) :: bra, ket, Ntot
 real*8, intent(out) :: result


 if (bra == ket) then
    result=1d0
 else
    result=0d0
 end if

 return

end subroutine OlLobattoDVR

!########### SUBROUTINE KeLobattoDVR ###########################################
!###############################################################################

subroutine KeLobattoDVR(result,bra,ket,Ntot,rpt,w,derLobMat)

 implicit none

 integer :: k
 integer, intent(in):: bra,ket,Ntot
 real*8 :: pf, intresult
!real*8, intent(out) :: result,rpt(0:Ntot+1),w(0:Ntot+1)
 real*8, intent(out) :: result
 real*8, intent(inout) :: rpt(0:Ntot+1),w(0:Ntot+1)
 real*8, intent(in)  :: derLobMat(0:Ntot+1,0:Ntot+1)


 pf=1d0!/sqrt(w(bra)*w(ket))
 intresult=0d0;

 do k=0,Ntot+1
    intresult=intresult+w(k)*derLobMat(bra,k)*derLobMat(ket,k)
 end do

 result=intresult*pf;

 return

end subroutine KeLobattoDVR

!########### SUBROUTINE rminus2LobattoDVR ######################################
!###############################################################################

subroutine rminus2LobattoDVR(result,bra,ket,Ntot,rpt)

 implicit none

 integer,intent(in) :: bra,ket,Ntot
 real*8, intent(out) :: result
 real*8, intent(in)  :: rpt(0:Ntot+1)


 if (bra == ket) then
    result=1d0/(rpt(bra)**2)
 else
    result=0d0
 end if

 return

end subroutine rminus2LobattoDVR

!########### SUBROUTINE derLobattoMat ##########################################
!###############################################################################

subroutine derLobattoMat(result,Ntot,rpt,w)

 implicit none

 integer :: n,eta,k
 integer, intent(in) :: Ntot
 real*8 :: elementres
 real*8, intent(out) :: result(0:Ntot+1,0:Ntot+1)
 real*8, intent(in)  :: rpt(0:Ntot+1),w(0:Ntot+1)

 do n=0,Ntot+1
    do eta=0,Ntot+1
       call derLobatto(result(n,eta),n,eta,Ntot,rpt,w)
    end do
 end do

 return

end subroutine derLobattoMat

!########### SUBROUTINE derLobatto #############################################
!
!    Calculates u'_n(r_eta)
!###############################################################################

subroutine derLobatto(result,n,eta,Ntot,rpt,w)

 implicit none

 integer :: j
 integer, intent(in) :: n, Ntot,eta
 real*8 :: pf, intresult,intpf
 real*8, intent(out) :: result
 real*8, intent(in)  :: w(0:Ntot+1), rpt(0:Ntot+1)

 if (n==eta) then
    if (n==0) then
       result=-1d0/(2d0*w(n)**(1.5d0))
    else if (n==Ntot+1) then
       result=1d0/(2d0*w(n)**(1.5d0))
    else
       result=0d0;
    end if
 else
    result=1d0/(sqrt(w(n))*(rpt(n)-rpt(eta)))
    do j=0,Ntot+1
       if ((j .ne. n) .and. (j .ne. eta)) then
          result=result*(rpt(eta)-rpt(j))/(rpt(n)-rpt(j))
       end if
    end do
 end if


 return

end subroutine derLobatto

!########### SUBROUTINE LobattoAbsWeights ######################################
!
!  Calculates weights and abscissa of Gauss-Lobatto quadrature using subroutine 
!  from Manolopoulos paper
!
!  This has been checked for Ntot=8 against the equivalent Mathematica routine
!###############################################################################

subroutine LobattoAbsWeights(rpt,w,Ntotp2,a0)

 implicit none

 integer :: L,k,j,i
 integer, intent(in) :: Ntotp2
 real*8 :: shift,scale,weight,z,p1,p2,p3,pi
 real*8, intent(in) :: a0
 real*8, intent(out):: w(1:Ntotp2),rpt(1:Ntotp2)

 L=(Ntotp2+1)/2
 shift=0.5d0*a0
 scale=0.5d0*a0
 weight=a0/(Ntotp2*(Ntotp2-1))
 pi=dacos(-1d0)
 rpt(1)=0
 w(1)=weight

 do k=2,L
    z=cos(pi*(4d0*k-3d0)/(4d0*Ntotp2-2d0))
    do i=1,7
       p2=0d0;
       p1=1d0;
       do j=1,Ntotp2-1
          p3=p2
          p2=p1
          p1=((2d0*j-1d0)*z*p2-(j-1d0)*p3)/j
       end do
       p2=(Ntotp2-1d0)*(p2-z*p1)/(1d0-z*z)
       p3=(2*z*p2-Ntotp2*(Ntotp2-1)*p1)/(1d0-z*z)
       z=z-p2/p3
    end do
    rpt(k)=shift-scale*z
    rpt(ntotp2+1-k)=shift+scale*z
    w(k)=weight/(p1*p1)
    w(ntotp2+1-k)=w(k)
 end do

 rpt(ntotp2)=a0
 w(ntotp2)=weight

 return

end subroutine LobattoAbsWeights

!########### END ###############################################################

