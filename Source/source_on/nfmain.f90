
!########################################################################
      subroutine nfmain(hr,r1m2t,htheta,r,theta,kz)

!     this routine controls the dvr calculation in the case of
!     symmetrised radau coordinates.
!     written by nic fulton, feb 1993.

      implicit real*8(a-h,o-y),logical(z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /outp/ zpham,zprad,zpvec,zrot,zladd,zembed,zmors2,zplot,&
                    zpmin,zvec,zquad2,zdiag,zlmat,zcut,zall,zlin,&
                    zp1d,zp2d,zr2r1,ztheta,ztran,zmors1,ztwod,zbisc,zperp,&
                    idiag1,idiag2,iout1,iout2,iwave,zpfun,ilev,idip,idipd,&
                    ieigs1,ivecs1,ieigs2,ivecs2,ivint,iband,intvec,iwvpb,iplot

      real*8, dimension(npnt,npnt) :: hr
      real*8, dimension(npnt,npnt) :: r1m2t
      real*8, dimension(nalf,nalf) :: htheta
      real*8, dimension(npnt) :: r
      real*8, dimension(nalf) :: theta
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham2
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig2,eigtmp
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::vecs2d
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eigs2d
      REAL*8, ALLOCATABLE, DIMENSION(:,:) ::ham3
      REAL*8, ALLOCATABLE, DIMENSION(:) ::eig3
      INTEGER, ALLOCATABLE, DIMENSION(:,:) ::iv2
      INTEGER, ALLOCATABLE, DIMENSION(:) ::nv2
      data x4/4.0d0/,x8/8.0d0/,x16/1.6d1/

      ALLOCATE(ham2(max2d,max2d),eig2(max2d),iv2(2,nalf),&
               vecs2d(max2d,max3d),eigs2d(max3d),nv2(nalf))
      if (.not.zcut) ALLOCATE(eigtmp(nalf*max2d))
      if (jrot .ne. 0) then
         term  = dble(jrot * jrot + jrot - kz * kz) / x8
         term2 = dble(jrot * jrot + jrot - 3 * kz * kz) / x4
         if (abs(kz) .eq. 1) then
            term3 = dble(jrot * jrot + jrot) / x16
            if (kmin .ge. 1 .and. zrot) term3 = -term3
         endif
      endif

! no need for the 1d diagonalisations as there is no possiblity of
! truncation as the symmetry would be broken.
      write(6,130)
      call nftim('beginning of 2d loop')
      do 30 igamma = 1,nalf
  
        nv2(igamma) = (npnt*(npnt+1-ipar*2))/2
        nham2 = nv2(igamma)
        call blc2d1(theta(igamma),r,hr,ham2,nham2,&
                          term,term2,term3,kz)
        call diag(ham2,nham2,nham2,eig2)

        if (.not. zcut) then
          call choosr(igamma,nham2,eig2,ham2,iv2,eigs2d,&
                      vecs2d,nv2,eigtmp)

          if(igamma .eq. nalf .and. .not. zpmin)&
              write(6,110) (itmp, iv2(2,itmp),itmp=1,nalf)
        else
           call cut2dr(igamma,nham2,eig2,ham2,iv2,eigs2d,vecs2d)
          if (.not. zpmin) write(6,110) igamma, iv2(2,igamma)
        endif
30    continue
      call testiv(iv2,nbass)

      nham3 = iv2(1,nalf) + iv2(2,nalf) - 1
      if (.not. zpmin) write(6,120) nham3
      call nftim('end of 2d loop')
      write(6,160)

      if (.not.zcut) DEALLOCATE(eigtmp)
      ALLOCATE(ham3(nham3,nham3))

      call bloc3d(htheta,r1m2t,ham3,eigs2d,vecs2d,iv2,nv2,ham2,nham3,r)

      DEALLOCATE(ham2,eig2,eigs2d,nv2)
 
      call nftim('end of 3d ham building')
      write(6,170)
      ALLOCATE(eig3(nham3))

      call diag3d(ham3,nham3,eig3,kz)

      call nftim('end of diagonalising 3d')

      if (ztran) call transr(iv2,vecs2d,ham3,eig3,nham3,nbass)

      DEALLOCATE(iv2,vecs2d,ham3,eig3)
      return
110   format(5x,'for gamma = ',i2,' selected ',i3,' energies.')
120   format(25x,'total = ',i5)
130   format(5x,'starting the 2d calculations.')
160   format(5x,'building the 3d hamiltonian.')
170   format(5x,'diagonalising the 3d hamiltonian.')
      end

!    ***********************************************************************

      subroutine blc2d1(xcos,r,hr,ham2,nham2,term,term2,term3,kz)
      implicit real*8(a-h,o-y),logical(z)
      common /size/ npnt1,npnt2,nalf,nmax1,nmax2,maxleg,nalf2,idvr,&
                    npnt,nlim1,nlim2,neval,ncoord,&
                    jrot,kmin,idia,ipar,&
                    max2d,max3d,max2d2,max3d2,npnta,npntb,npntc,&
                    ndima,ndimb,ndimc,iq,emax1,emax2,&
                    nploti,nplotf,nplot,ithre,npth
      common /split1/ re1,diss1,we1,beta1,ur1,urr1,a1,iu1

      real*8, dimension(npnt,npnt) :: hr
      real*8, dimension(npnt) :: r
      real*8, dimension(nham2,nham2) :: ham2
      data xp5/0.5d0/,x1/1.0d0/
      factr2 = sqrt(xp5)

      ham2 = 0.0d0

!     allow for j > 0 case
      if (jrot .ne. 0) then
        fact =  term + term2 / (x1 - xcos)
        if (kz .eq. 1) fact = fact + term3 * (x1 + xcos)/(x1-xcos)
        ia = 0
        do 15 ibeta=1,npnt
          do 25 ialpha=1,ibeta-ipar
            walpha = xp5 / (r(ialpha)*r(ialpha)*ur1)
            wbeta = xp5 / (r(ibeta)*r(ibeta)*ur1)
            wsum = (walpha + wbeta) * fact

            ham2(ialpha+ia,ialpha+ia) = ham2(ialpha+ia,ialpha+ia) + wsum

 25       continue
          ia=ia+ibeta-ipar
          if (ipar .eq. 0) ham2(ia,ia) = ham2(ia,ia) + wsum
 15     continue
      endif

!      q=0.0d0
      q=1.0d0
      if(ipar .eq. 1) q=-1.0d0
      iap=0
      do 10 ibetap=1,npnt
        ia=0
        do 20 ibeta=1,npnt
          do 30 ialphp=1,ibetap-ipar
            do 40 ialpha=1,ibeta-ipar
              if(ibeta .eq. ibetap)&
                 ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+hr(ialphp,ialpha)
              if(ibeta .eq. ialphp)&
                 ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+q*hr(ibetap,ialpha)
              if(ialpha .eq. ibetap)&
                 ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+q*hr(ialphp,ibeta)
              if(ialpha .eq. ialphp)&
                 ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+hr(ibetap,ibeta)
              if(ialpha .eq. ialphp .and. ibeta .eq. ibetap) then
                call potv(v,r(ibeta),r(ialpha),xcos)
                ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+v
              endif
              if(ialpha .eq. ibetap .and. ibeta .eq. ialphp) then
                call potv(v,r(ibeta),r(ialpha),xcos)
                ham2(ialphp+iap,ialpha+ia)=&
                  ham2(ialphp+iap,ialpha+ia)+q*v
              endif
              if(ialphp .eq. ibetap) ham2(ialphp+iap,ialpha+ia)=&
                ham2(ialphp+iap,ialpha+ia)*factr2
              if(ialpha .eq. ibeta) ham2(ialphp+iap,ialpha+ia)=&
                ham2(ialphp+iap,ialpha+ia)*factr2
40          continue
30        continue
          ia=ia+ibeta-ipar
20      continue
        iap=iap+ibetap-ipar
10    continue
      return
      end

